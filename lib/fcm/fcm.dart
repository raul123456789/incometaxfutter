import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:listview/Bookmark/bookmarkpage.dart';
import 'package:listview/CommonModule/CommonModulePage.dart';
import 'package:listview/Inbox/Inbox.dart';
import 'package:listview/Leaderboard/Leaderboard.dart';
import 'package:listview/askquery/AskQuery.dart';
import 'package:listview/constants/const_module.dart';
import 'package:listview/post_details/PostDetailsStatefull.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/profile/profile.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/Post.dart';
import 'package:listview/Section.dart';
import 'package:listview/profile/profile.dart';
import 'package:listview/model/ITNotification.dart';
import 'package:listview/db/notification_db.dart' as notificationDB;


class FCMHelper {

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
  FirebaseMessaging _firebaseMessaging =FirebaseMessaging();
  BuildContext context;

  FCMHelper(this.context);


  void configureFCM() {

    if(Platform.isIOS) {
        iOS_Permission();
    }

    _firebaseMessaging.getToken().then((v){
      print("FCM------"+v);
    });

    Future.delayed(Duration(seconds: 1),() {

      _firebaseMessaging.configure(

        onMessage: (Map<String, dynamic> message) async {
          Map<String, dynamic> m = message;
          showNotification(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          showNotification(message);
        },
        onResume: (Map<String, dynamic> message) async {
          showNotification(message);
        },
        onBackgroundMessage: myBackgroundMessageHandler,

      );
    });





  }

  void configureLocalNotification() {

    var initializationSettingsAndroid =
    AndroidInitializationSettings("@drawable/launch_background");

    var initializationSettingsIOS =
    IOSInitializationSettings(onDidReceiveLocalNotification:onDidReceiveLocalNotification);

    var initializationSettings =
    InitializationSettings(initializationSettingsAndroid,initializationSettingsIOS);

    flutterLocalNotificationsPlugin.
    initialize(initializationSettings,onSelectNotification:onSelectNotification);

  }


  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];

      showNotification(message);
      print("Hello world ------- ");
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  Future<void> onSelectNotification(String payload) async {
    Map<String, dynamic> message = json.decode(payload);

    /*if (payload != null) {
      debugPrint('-----------------------------------notification payload: ' +
          message["link"]);
    }*/

    Widget destination = await getDestinationWidget(message);

  }

  Future<Widget> getDestinationWidget(Map<String, dynamic> message) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String type = message["type"];
    String title = message["post_title"];
    String body = message["post_msg"];
    String url = message["link"];
    String moduleName = message["module_name"];
    String modItemId = message["item_id"];
    String img = message["img"];

    /*Create a post object*/
    Post post = new Post();
    post.title = body;
    post.moduleName = moduleName;
    post.itemId = modItemId;

    if (url != null && url != null)
      post.links = url;
    else
      post.links = null;
    /*********************/

    Widget dest;

    switch (type) {
      case "1":
         /*
        * Check if we have a moditem id
        * */
        if (modItemId != null && modItemId != ("")) {
          dest = PostDetailsStatefull(post);
        } else if (url != null && url != (""))  {

          if (url.contains("caclubindia.com")) {
            /*Internal link*/
            post.commentAllow = false;
            dest = PostDetailsStatefull(post);
          } else {
            /*
            * * The link is an external link so open external browser
              * */
          }
        } else {

        }
        break;
      case "2":
        break;
      case "3":
      /*open_forum*/
        dest = CommonModulePage("Forum", network.fetchPost("forum"),moduleInfo["1"]["id"]);
        break;
      case "4":
      /*open_news*/
        dest = CommonModulePage("News", network.fetchPost("news"),moduleInfo["3"]["id"]);
        break;
      case "5":
      /*open articles*/
        dest = CommonModulePage("Articles", network.fetchPost("articles"),moduleInfo["2"]["id"]);
        break;
      case "6":
      /*ask_query*/
        dest = AsKQuery();
        break;
      case "7":
      /*open_section*/
        Section section = new Section();
        section.id = modItemId;
        /*in case 7 : modItemId contains the section_id*/

        /*dest=SectionDetails(section);*/
        break;
      case "8":
      /*open_webview*/

        if (!url.contains("www.caclubindia.com")) {
            post.shareEnabled = false;
        }

        dest = PostDetailsStatefull(post);
        break;
      case "9":
      /*open_extbrow*/

        break;
      case "10":
      /*Breaking news*/

      /*Open app*/

        break;
      case "11":
      /*Enable-disable interlinking*/

        break;
      case "12":
        break;
      case "13":
      /*Open PM*/

        dest = Inbox();

        /*Brodcast this now*/

        /******************/

        break;
      case "14":
      /*Open Control center*/

        break;
      case "15":
      /*Open bookmark*/
        dest = BookmarkPage();

        break;
      case "16":
        break;
      case "17":
      /*Open notes*/

        break;
      case "18":
        break;
      case "19":
      /*open leaderboard*/
        dest = Leaderboard();
        break;
      case "20":
      /*share prompt*/

        String strToShare = title + "\n" + url;

        break;
      case "21":
      /*logout*/

        break;
      case "22":
      /*inapp products*/
        break;
      case "23":
        dest = Profile(prefs.getString(MEMBER_ID_STR));
        break;
    }

    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => dest),
    );

    return dest;
  }

  void showNotification(Map<String,dynamic > message) async {

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    SharedPreferences prefs =await SharedPreferences.getInstance();


    bool showNotification = (prefs.getBool("show_notification")==null || prefs.getBool("show_notification"));

    if(showNotification) {

      String type = message["data"]["type"];
      String title = message["data"]["post_title"];
      String body = message["data"]["post_msg"];
      String url = message["data"]["link"];
      String moduleName = message["data"]["module_name"];
      String modItemId = message["data"]["item_id"];
      String img = message["data"]["img"];

      /*Save the notification locally*/
      ITNotification itNotification =
      ITNotification(type, title, body, url, moduleName, modItemId, img);
      notificationDB.insertNotification(itNotification);

      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'default_channel', 'Income Tax Notifications', 'Get the updates instantly with push notifications',
          importance: Importance.Max,
          priority: Priority.High,
          ticker: 'ticker',
          icon: "@drawable/splash_0");
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          0, title, body, platformChannelSpecifics,
          payload: json.encode(message["data"]));
    }
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {

    await showDialog (
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title,style: TextStyle(color: Colors.white),),
        content: Text(body,style: TextStyle(color: Colors.white),),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
             /* Navigator.of(context, rootNavigator: true).pop();
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PostDetailsStatefull(payload),
                ),
              );*/
            },
          )
        ],
      ),
    );
  }


}

