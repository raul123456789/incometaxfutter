import 'package:flutter/material.dart';

class detailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text("List of Courses"),
        backgroundColor: Colors.red,

      ),
      body:ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Container(

              child: FittedBox(

                child: Material(

                  color: Colors.white,
                  elevation: 12,
                  borderRadius: BorderRadius.circular(24.0),
                  shadowColor: Colors.grey,
                  child: Row(

                    children: <Widget>[

                        Container(

                            child: Padding(

                              padding: const EdgeInsets.only(left:70.0,bottom: 50,right: 30,top: 30),
                            //child:courselistdetails

                            ),
                        ),
                        Container(

                          width: 110,
                          height: 50,
                          alignment: Alignment.topRight,
                             child: ClipRRect(

                               borderRadius: new BorderRadius.circular(24.0),
                               child:

                               Image (

                                 width: 60,
                                 height: 90,
                                 fit: BoxFit.fitWidth,
                                 alignment: Alignment.topRight,
                                 image: NetworkImage("https://skillsigma.com/wp-content/uploads/2019/01/C.png"),
                               ),
                      ),
                    )

                  ],),



                ),

              )

            ),

          ), //C padding 1

          Padding(

                padding: const EdgeInsets.all(16.0),
                child: Container(


                  child: new FittedBox(

                    child: Material(

                      color: Colors.white,
                      elevation: 14.0,
                      borderRadius: BorderRadius.circular(24.0),
                      shadowColor: Colors.grey,
                      child: Row(
                        
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          
                          Container(
                            
                            child: Padding(
                              padding: const EdgeInsets.only(left:70.0,bottom: 50,right: 30,top: 30),
                              
                              
                              
                            ),
                            
                            
                          ),
                          
                          Container(

                            width: 110,
                            height: 50,
                            
                            child: ClipRRect(
                              
                              borderRadius: new BorderRadius.circular(24.0),
                              child: Image(

                                width: 120,
                                height: 80,
                                fit: BoxFit.contain,

                                alignment: Alignment.topRight,
                                
                                image: NetworkImage("https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/1200px-ISO_C%2B%2B_Logo.svg.png"),
                                
                              ),
                              
                              
                              
                            ),
                            
                            
                            
                          ),
                          
                          
                          
                        ],
                        
                        
                      ),


                    ),


                  ),


                ),

          ), //C++ padding 2


          Padding(

            padding: const EdgeInsets.all(16.0),
            child: Container(


              child: new FittedBox(

                child: Material(

                  color: Colors.white,
                  elevation: 14.0,
                  borderRadius: BorderRadius.circular(24.0),
                  shadowColor: Colors.grey,
                  child: Row(

                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Container(

                        child: Padding(
                          padding: const EdgeInsets.only(left:70.0,bottom: 50,right: 30,top: 30),



                        ),


                      ),

                      Container(

                        width: 110,
                        height: 50,

                        child: ClipRRect(

                          borderRadius: new BorderRadius.circular(24.0),
                          child: Image(

                            width: 200,
                            height: 100,
                            fit: BoxFit.contain,

                            alignment: Alignment.topRight,

                            image: NetworkImage("https://cdn.freebiesupply.com/logos/thumbs/2x/java-4-logo.png"),

                          ),



                        ),



                      ),



                    ],


                  ),


                ),


              ),


            ),

          ), //java padding

          Padding(

            padding: const EdgeInsets.all(16.0),
            child: Container(


              child: new FittedBox(

                child: Material(

                  color: Colors.white,
                  elevation: 14.0,
                  borderRadius: BorderRadius.circular(24.0),
                  shadowColor: Colors.grey,
                  child: Row(

                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                      Container(

                        child: Padding(
                          padding: const EdgeInsets.only(left:70.0,bottom: 50,right: 30,top: 30),



                        ),


                      ),

                      Container(

                        width: 110,
                        height: 50,

                        child: ClipRRect(

                          borderRadius: new BorderRadius.circular(24.0),
                          child: Image(

                            width: 100,
                            height: 100,
                            fit: BoxFit.contain,

                            alignment: Alignment.topRight,

                            image: NetworkImage("https://i.pinimg.com/736x/4d/13/d5/4d13d55d6d0b38b7a4e85fcf97ff6279.jpg"),

                          ),



                        ),



                      ),



                    ],


                  ),


                ),


              ),


            ),

          ),



        ],




      )

    );

  }
}
