import 'dart:async';
import 'dart:collection';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:listview/CommonModule/SearchResults.dart';
import 'package:listview/Post.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/post_details/PostDetailsStatefull.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/network/network.dart' as network;
//import 'package:listview/constants/bookmarkconst.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:listview/date_util.dart';
import 'package:hexcolor/hexcolor.dart';

class CommonModulePage extends StatefulWidget {
  Future<List<Post>> posts;
  String moduleName;
  String moduleID;
  String catId;
  String locationId;
  String activityName;

  CommonModulePage(this.moduleName, this.posts, this.moduleID,
      {this.catId, this.activityName, this.locationId});

  @override
  _CommonModulePageState createState() => _CommonModulePageState(
      moduleName, posts, activityName, moduleID, catId, locationId);
}

class _CommonModulePageState extends State<CommonModulePage> {
  Future<List<Post>> posts;
  String moduleName;
  String moduleId;
  String catId;
  String locationId;
  bool isSearching = false;

  ScrollController _scrollController = new ScrollController();
  bool loadingMore = false;
  String activityName;

  Widget appBarTitle = new Text("");
  Icon actionIcon = new Icon(Icons.search);

  _CommonModulePageState(this.moduleName, this.posts, this.activityName,
      this.moduleId, this.catId, this.locationId);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (moduleName == "") {
      moduleName = "";
    }

    appBarTitle = new Text((activityName == null) ? moduleName : activityName);

    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.offset) {
        //_getMoreData();
        if (!loadingMore) {
          setState(() {
            posts.then(
                (list) => {list.insert(list.length, null), loadingMore = true});
          });
        }
        HashMap<String, String> params = HashMap();
        posts.then((list) async {
          if (list.length > 2) {
            params[KEY_API_ITEM_ID] = list[list.length - 2].itemId;
            params[KEY_MODULE_ID] = moduleId;
            params["getold"] = "1";

            /* if(catId!=null)
              params["cat_id"] =catId;

            if(locationId!=null)
              params["l_id"] =locationId;*/

            List<Post> tempListMoreData = await network
                .fetchPost(moduleName.toLowerCase(), jsonMap: params);

            setState(() {
              posts.then((oldList) => {
                    if (oldList[oldList.length - 1] == null)
                      {oldList.removeLast()},
                    if (tempListMoreData.length > 0)
                      oldList.addAll(tempListMoreData),
                    loadingMore = false
                  });
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: !isSearching
              ? appBarTitle
              : TextField(
                  decoration: new InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      //prefixIcon: new Icon(Icons.search, color: Colors.white),
                      hintText: "Search " + moduleName,
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white70)),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white70)),
                      hintStyle: new TextStyle(color: Colors.white)),
                  onSubmitted: (text) => performSearch(text),
                  /* onChanged: (text) {
            setState(() => searchStr = text);
          },*/
                  onChanged: (text) {
                    //setState(()=> searchStr=text);
                  },
                ),
          centerTitle: false,
          actions: _buildactions(),
          backgroundColor: Hexcolor('#008B8B'),
        ),
        body: RefreshIndicator(
          onRefresh: _onRefresh,
          child: new Container(
            child: new Center(
              child: FutureBuilder<List<Post>>(
                future: posts,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return questionList(snapshot.data);
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                },
              ),
            ),
          ),
        ));
  }

  ListView questionList(List<Post> data) {
    return ListView.builder(
        controller: _scrollController,
        itemCount: data.length,
        itemBuilder: (context, index) => Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            margin: EdgeInsets.all(0),
            elevation: 2.0,
            child: (index == data.length - 1 && loadingMore)
                ? Align(
                    child: Padding(
                    padding: EdgeInsets.all(20),
                    child: SizedBox(
                        width: 40,
                        height: 40,
                        child: CircularProgressIndicator(
                          strokeWidth: 1,
                        )),
                  ))
                : GestureDetector(
                    onTap: () {
                      if (data[index].url != null &&
                          data[index].url.contains("#ext")) {
                        _launchURL(data[index].url);
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PostDetailsStatefull(data[index])));
                      }
                    },
                    child: Padding(
                      padding: EdgeInsetsDirectional.only(top: 10, bottom: 10),
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.all(8)),
                            Expanded(
                                child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    // Text(data[index].countryName),
                                    Padding(
                                      padding: EdgeInsets.all(5),
                                      /*child: Icon(
                                          Icons.adjust,
                                          size: 10.0,
                                        )*/
                                    ),
                                    //Text(data[index].categoryName),
                                    Expanded(
                                        child: Align(
                                            alignment: Alignment.centerRight,
                                            child: GestureDetector(
                                              onTap: () => {
                                                bookmarkPost(
                                                    context,
                                                    data[
                                                        index]) //bookmark post function call
                                              },
                                              child: Container(),
                                            )))
                                  ],
                                ),
                                Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      height: ((data[index].previewImage !=
                                                      null &&
                                                  data[index]
                                                      .isPreviewImagePresent()) ||
                                              moduleName.toLowerCase() ==
                                                  VALUE_MODULE_EVENTS)
                                          ? 240
                                          : 0,
                                      width: ((data[index].previewImage !=
                                                      null &&
                                                  data[index]
                                                      .isPreviewImagePresent()) ||
                                              moduleName.toLowerCase() ==
                                                  VALUE_MODULE_EVENTS)
                                          ? 370
                                          : 0,
                                      child: Card(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 15, bottom: 15),
                                                    child: (moduleName
                                                                .toLowerCase() !=
                                                            VALUE_MODULE_EVENTS)
                                                        ? (data[index].previewImage !=
                                                                    null &&
                                                                data[index]
                                                                    .isPreviewImagePresent())
                                                            ? ClipRRect(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        10.0),
                                                                child: Image
                                                                    .network(
                                                                  data[index].previewImage ==
                                                                          null
                                                                      ? 'https://picsum.photos/250?image=9'
                                                                      : data[index]
                                                                          .previewImage,
                                                                  fit: BoxFit
                                                                      .cover,
                                                                  height: 150,
                                                                  width: 100,
                                                                ))
                                                            : Container()
                                                        : Row())),
                                          ],
                                        ),
                                      ),
                                    )),
                                Padding(
                                  padding: EdgeInsets.only(top: 10),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    data[index].title,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ),
                                (moduleName.toLowerCase() !=
                                        VALUE_MODULE_EVENTS)
                                    ? Row(
                                        children: <Widget>[
                                          Text(
                                            data[index].authorName,
                                            style: TextStyle(fontSize: 12),
                                          ),
                                          Visibility(
                                              visible:
                                                  (data[index].authorName !=
                                                      ""),
                                              child: Padding(
                                                padding: EdgeInsets.all(5),
                                                /*child: Icon(
                                              Icons.adjust,
                                              size: 5.0,
                                            )*/
                                              )),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Text(
                                              parseDDMMYYYY(
                                                  data[index].publishDate),
                                              style: TextStyle(fontSize: 11),
                                            ),
                                          ),
                                          Spacer(),
                                          data[index].commentCount != "0"
                                              ? Padding(
                                                  padding:
                                                      EdgeInsets.only(right: 2),
                                                  child: Text(
                                                    data[index].commentCount,
                                                    style: TextStyle(
                                                        color: Colors.grey,
                                                        fontSize: 11),
                                                  ),
                                                )
                                              : Opacity(
                                                  opacity: 0.0,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 4),
                                                    child: Text(
                                                      data[index].commentCount,
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12),
                                                    ),
                                                  ),
                                                ),
                                          data[index].commentCount == "0"
                                              ? Opacity(
                                                  opacity: 0.0,
                                                  child: Align(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    child: Icon(
                                                      Icons.message,
                                                      size: 15,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                )
                                              : Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Icons.message,
                                                    size: 15,
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                          Align(
                                            alignment: Alignment.centerRight,
                                            child: popUpMenuButton(data, index),
                                          ),
                                        ],
                                      )
                                    : Padding(
                                        padding: EdgeInsets.all(5),
                                      ),
                              ],
                            )),
                            Padding(padding: EdgeInsets.all(5)),
                          ],
                        ),
                      ),
                    ))));
  }

  performSearch(String text) {
    HashMap<String, String> params = HashMap();
    params["q"] = text;

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchResultsPage(
                "Search", text, network.fetchPost("search", jsonMap: params))));
  }

  void startSearch() {
    setState(() {
      isSearching = true;
    });
  }

  List<Widget> _buildactions() {
    if (isSearching) {
      return [
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            setState(() {
              isSearching = false;
            });
            //_clearsearchQuery();
          },
        )
      ];
    }
    return [
      IconButton(
        color: Colors.white,
        icon: const Icon(Icons.search),
        onPressed: () {
          startSearch();
        },
      ),
      IconButton(
        icon: Icon(Icons.more_vert),
        tooltip: "Menu",
        onPressed: () {
          popUpMenuButton1();
        },
      ),
    ];
  }

  bookmarkPost(BuildContext context, Post post) async {
    setState(() {
      if (post.isBookmarked)
        post.isBookmarked = false;
      else
        post.isBookmarked = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap<String, String>();
    params[KEY_MODULE_NAME] = post.moduleName;
    params[KEY_API_MOD_ITEM_ID_2] = post.modeItemId;
    params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);
    params[KEY_API_TITLE] = post.title;
    params[KEY_API_ITEM_ID] = post.itemId;

    String res = await network.genericNetworkRequest(params, CREATE_BOOKMARK);

    final snackBar = SnackBar(
      content: Text(res),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }

  Widget popUpMenuButton1() {
    return PopupMenuButton<String>(
      icon: Icon(Icons.more_vert),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'MoreApps',
          child: Text('More Apps'),
        ), //PopupMenuItem
        PopupMenuItem<String>(
          value: 'queries_val',
          child: Text('My Queries'),
        ), //PopupMenuItem
        PopupMenuItem<String>(
          value: 'answers_val',
          child: Text('My Answers'),
        ), //PopupMenuItem
      ],
      onSelected: (retVal) {
        if (retVal == 'MoreApps') {
          launchURL();
        } else if (retVal == 'queries_val') {
        } else {}
      },
    );
  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }

  Widget popUpMenuButton(List data, int index) {
    return PopupMenuButton<String>(
      icon: Icon(
        Icons.more_vert,
        color: Colors.grey,
        size: 20,
      ),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'Bookmark',
          child: Text('Bookmark'),
          height: 5,
        ), //PopupMenuItem
        /*PopupMenuItem<String>(
          value: 'queries_val',
          child: Text('My Queries'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'answers_val',
          child: Text('My Answers'),
        ),//PopupMenuItem*/
      ],
      onSelected: (retVal) {
        if (retVal == 'Bookmark') {
          bookmarkPost(context, data[index]);
        } else {}
      },
    );
  }

  Future<Null> _onRefresh() {
    /* showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              new CircularProgressIndicator(),
              Padding(
                  padding: EdgeInsets.only(top: 6),
                  child: new Text("Loading "+moduleName,style: TextStyle(fontWeight: FontWeight.bold),)
              )

            ],
          ),
        );
      },
    );
    new Future.delayed(new Duration(seconds: 1), () {
      Navigator.pop(context); //pop dialog
    });*/
    Completer<Null> completer = new Completer<Null>();
    new Timer(new Duration(seconds: 1), () {
      print("timer complete");
      completer.complete();
    });

    return completer.future;
  }

  launchURL() {
    const url = 'market://search?q=pub:Offline Apps (No Internet required)';
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not open $url';
    }
  }
}
