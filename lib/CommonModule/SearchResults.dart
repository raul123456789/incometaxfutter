import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/Post.dart';
import 'package:listview/date_util.dart';
import 'package:listview/post_details/PostDetailsStatefull.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/network/network.dart' as network;


class SearchResultsPage extends StatelessWidget {

  Future<List<Post>> posts;
  String moduleName;
  String query;
  SearchResultsPage(this.moduleName,this.query,this.posts);
  bool isSearching=false;
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: !isSearching?Text(query):
        TextField(
          cursorColor: Colors.white70,
          decoration: new InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: Colors.white70,),
              //prefixIcon: new Icon(Icons.search, color: Colors.white),
              hintText: "Search...",
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
              hintStyle: new TextStyle(color: Colors.white)),
          onSubmitted: (text) => performSearch(text),
          /* onChanged: (text) {
            setState(() => searchStr = text);
          },*/
          onChanged: (text) {
            //setState(()=> searchStr=text);
          },
        ),
        backgroundColor: Hexcolor('#008B8B'),
      ),
      body: new Container(

        child: new Center(
          child: FutureBuilder<List<Post>>(
            future: posts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return questionList(snapshot.data);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),


        ),
      ),
    );
  }

  //Search Results display
  ListView questionList(List<Post> data) {

    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) => Card(

            margin: EdgeInsets.all(0),
            elevation: 0.0,
            child: GestureDetector(
              onTap: (){
                Navigator.push
                  (context, MaterialPageRoute(builder: (context)=>
                          PostDetailsStatefull(data[index])));
              },
                child: Padding(

                    padding: EdgeInsetsDirectional.only(top: 10,bottom: 10),
                    child: Container(

                      decoration: new BoxDecoration(
                        border: new Border(
                          bottom: BorderSide(
                            color: Colors.grey,
                            width: 0.0,

                          ))),

                      child: Row(
                        children: <Widget>[
                          Padding(padding: EdgeInsets.all(8)),
                          Expanded(
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      //Text(data[index].countryName),
                                      Padding(
                                          padding: EdgeInsets.all(5),
                                          /*child: Icon(
                                            Icons.adjust,
                                            size: 5.0,
                                          )*/),
                                      //Text(data[index].categoryName),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold, fontSize: 18),
                                    ),
                                  ),
                                  (moduleName.toLowerCase() != VALUE_MODULE_EVENTS)
                                      ? Row(
                                    children: <Widget>[
                                      Text(data[index].authorName),
                                      Padding(
                                          padding: EdgeInsets.all(5),
                                         /* child: Icon(
                                            Icons.adjust,
                                            size: 5.0,
                                          )*/),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: Text(
                                          parseDDMMYYYY(data[index].publishDate),
                                          style: TextStyle(fontSize: 12,fontWeight: FontWeight.normal),
                                          softWrap: false,
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      Spacer(),
                                      data[index].commentCount!="0"?
                                      Padding(
                                        padding: EdgeInsets.only(right: 2),
                                        child:  Text(data[index].commentCount,
                                          style: TextStyle(color: Colors.grey,fontSize: 11),),
                                      ):
                                      Opacity(
                                        opacity: 0.0,
                                        child:  Padding (
                                          padding: EdgeInsets.only(right: 4),
                                          child: Text(data[index].commentCount,style:
                                          TextStyle(color: Colors.grey,
                                              fontSize: 12),
                                          ),
                                        ),
                                      ),
                                      data[index].commentCount=="0"?
                                      Opacity(
                                        opacity: 0.0,
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Icon(
                                            Icons.message,
                                            size: 15,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ):
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: Icon(
                                          Icons.message,
                                          size: 15,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: popUpMenuButton(data,index),
                                      ),
                                    ],
                                  )
                                      : Padding(
                                    padding: EdgeInsets.all(5),
                                  ),
                                ],
                              )),
                          Padding(padding: EdgeInsets.all(5)),
                          /*Align(
                              alignment: Alignment.centerRight,
                              child: SizedBox(
                                  height: 100,
                                  width: 100,
                                  child: Column(
                                    children: <Widget>[
                                      Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                right: 15, bottom: 15),
                                            child: (moduleName.toLowerCase() !=
                                                VALUE_MODULE_EVENTS)
                                                ? ClipRRect(
                                                borderRadius:
                                                new BorderRadius.circular(
                                                    5.0),
                                                child: Image.network(
                                                  data[index].previewImage == null
                                                      ? 'https://picsum.photos/250?image=9'
                                                      : data[index].previewImage,
                                                  fit: BoxFit.fitHeight,
                                                ))
                                                : Column(
                                              children: <Widget>[
                                                Container(
                                                    width: 85,
                                                    height: 85,
                                                    decoration: new BoxDecoration(
                                                        border: Border.all(
                                                            width: 0.2),
                                                        borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                5.0))),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .center,
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Text(
                                                          parseDate(data[index]
                                                              .eventDate),
                                                          style: TextStyle(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold),
                                                        ),
                                                        Text(
                                                          parseMonth(data[index]
                                                              .eventDate),
                                                        ),
                                                      ],
                                                    ))
                                              ],
                                            ),
                                          ))
                                    ],
                                  ))),*/
                        ],
                      ),//Row
                    ),





                ),
            ),//GestureDirector
        ) //Card
    );



  }

  String parseDate(String publishDate) {
    if (publishDate != "") {
      DateFormat format = new DateFormat("dd/MM/yyyy");
      print(publishDate);
      print(format.parse(publishDate));

      DateTime dt = format.parse(publishDate);

      return dt.day.toString();
    }
    return "";
  }

  String parseMonth(String publishDate) {
    if (publishDate != "") {
      DateFormat format = new DateFormat("dd/MM/yyyy");
      print(publishDate);
      print(format.parse(publishDate));

      DateTime dt = format.parse(publishDate);

      var month = "jan";

      switch (dt.month.abs()) {
        case 1:
          month = "Jan";
          break;
        case 2:
          month = "Feb";
          break;
        case 3:
          month = "Mar";
          break;
        case 4:
          month = "Apr";
          break;
        case 5:
          month = "May";
          break;
        case 6:
          month = "Jun";
          break;
        case 7:
          month = "Jul";
          break;
        case 8:
          month = "Aug";
          break;
        case 9:
          month = "Sep";
          break;
        case 10:
          month = "Oct";
          break;
        case 11:
          month = "Nov";
          break;
        case 12:
          month = "Dec";
          break;
      }
      return month;
    }
    return "";
  }

  Widget popUpMenuButton(List data,int index) {
    return PopupMenuButton<String>(
      icon: Icon(
        Icons.more_vert,
        color: Colors.grey,
        size: 20,
      ),
      itemBuilder: (BuildContext context)=><PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'Bookmark',
          child: Text('Bookmark'),
          height: 5,
        ),//PopupMenuItem
      ],
      onSelected: (retVal) {
        if(retVal == 'Bookmark') {
            bookmarkPost(context, data[index]);
        }else {

        }
      },
    );
  }

  bookmarkPost(BuildContext context, Post post) async {
      if (post.isBookmarked)
        post.isBookmarked = false;
      else
        post.isBookmarked = true;


    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap<String, String>();
    params[KEY_MODULE_NAME] = post.moduleName;
    params[KEY_API_MOD_ITEM_ID_2] = post.modeItemId;
    params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);
    params[KEY_API_TITLE] = post.title;
    params[KEY_API_ITEM_ID] = post.itemId;


    String res = await network.genericNetworkRequest(params, CREATE_BOOKMARK);

    final snackBar = SnackBar(
      content: Text(res),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }


  performSearch(String text) {
    HashMap<String, String> params = HashMap();
    params["q"] = text;

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchResultsPage(
                "Search", text, network.fetchPost("search", jsonMap: params))));
  }
}
