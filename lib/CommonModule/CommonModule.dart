import 'dart:async';
import 'dart:collection';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:listview/Post.dart';
import 'package:listview/constants/BookmarkConst.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/date_util.dart';
import 'package:listview/post_details/PostDetailsStatefull.dart';
import 'package:requests/requests.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/network/network.dart' as network;
import 'package:intl/intl.dart';
import 'package:listview/Common.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';



class CommonModule extends StatefulWidget {
  Future<List<Post>> posts;
  final String moduleName;
  final String moduleId;

  CommonModule(this.moduleName, this.posts, [this.moduleId]);

  @override
  State<StatefulWidget> createState() {
    return CommonModuleState(moduleName, posts, moduleId);
  }
}

class CommonModuleState extends State<CommonModule> {
  Future<List<Post>> posts;
  Post thisPost;
  String moduleId;
  AnimationController animationController ;

  final String moduleName;
  ScrollController _scrollController = new ScrollController();
  bool loadingMore = false;

  CommonModuleState(this.moduleName, this.posts, this.moduleId);

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.offset) {
        //_getMoreData();

        if (!loadingMore) {
          setState(() {
            posts.then(
                    (list) => {
                            if(list!=null){
                              list.insert(list.length, null), loadingMore = true
                            }
                        }
                      );
          });
        }

        HashMap<String, String> params = HashMap();
        posts.then((list) {
          if (list.length > 2 && list.length!=null) {
            params[KEY_API_ITEM_ID] = list[list.length - 2].itemId;
            if(moduleId!=null)
              params[KEY_MODULE_ID]=moduleId;

              params["getold"] = "1";
          }
        });

        List<Post> tempListMoreData =
        await network.fetchPost(moduleName.toLowerCase(), jsonMap:params, moduleId: moduleId);

        setState(() {
          posts.then((oldList) => {
            if(oldList.length!=null){
              if (oldList[oldList.length - 1] == null) {oldList.removeLast()},
              if (tempListMoreData.length > 0)
                oldList.addAll(tempListMoreData),
                 loadingMore = false
            }

          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh:_onRefresh,
      child: Container(
        child: new Center(
          child: FutureBuilder<List<Post>>(
            future: posts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return getPostList(snapshot.data);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
    /*return new Container(
      child: new Center(
        child: FutureBuilder<List<Post>>(
          future: posts,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
                return getPostList(snapshot.data);
            } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
            }
            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );*/
  }

  ListView getPostList(List<Post> data) {
    if(data!=null) {
      return ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          itemCount: data.length,
          itemBuilder: (context, index) {
                if (data[index] != null && data[index].showAsBigCard)
                  return getSingleListItemWidgetBigCard(data, index);
                else
                  return getSingleListItemWidget(data, index);
            }
          );
      }
  }

  Widget popUpMenuButton(List data,int index) {
    return PopupMenuButton<String>(
      icon: Icon(
          Icons.more_vert,
          color: Colors.grey,
          size: 20,
      ),
      itemBuilder: (BuildContext context)=><PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'Bookmark',
          child: Text('Bookmark'),
          height: 5,
        ),//PopupMenuItem
      ],
      onSelected: (retVal) {
        if(retVal == 'Bookmark') {
              bookmarkPost(context, data[index]);
        }
      },
    );
  }

  bookmarkPost(BuildContext context, Post post) async {
    setState(() {
      if (post.isBookmarked)
        post.isBookmarked = false;
      else
        post.isBookmarked = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap<String, String>();
    params[KEY_MODULE_NAME] = post.moduleName;
    params[KEY_API_MOD_ITEM_ID_2] = post.modeItemId;
    params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);
    params[KEY_API_TITLE] = post.title;
    params[KEY_API_ITEM_ID] = post.itemId;


    String res = await network.genericNetworkRequest(params, CREATE_BOOKMARK);

    final snackBar = SnackBar(
      content: Text(res),
    );

    Scaffold.of(context).showSnackBar(snackBar);
  }

  Widget getSingleListItemWidget(List data, int index) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        margin: EdgeInsets.all(0),
        elevation: 2.0,
        child: (index == data.length - 1 && loadingMore)
            ? Align(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    strokeWidth: 1,
                  )),
            ))
            : GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PostDetailsStatefull(data[index])
                  ));
            },
            child: Padding(
              padding: EdgeInsetsDirectional.only(top: 10, bottom: 10),
              child: Container(
/*                decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(
                          color: Colors.grey,
                          width: 0.2,
                        ))),*/
                child: Row(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.all(8)),
                    Expanded(
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                /*Expanded(
                                    child: Padding(
                                        padding:
                                        EdgeInsets.only(right: 0, bottom: 0),
                                        child: ClipRRect(
                                          borderRadius:
                                          new BorderRadius.circular(5.0),
                                          child: Image.network(
                                            data[index].previewImage == null
                                                ? ''
                                                : data[index].previewImage,
                                            height: 140,
                                            fit: BoxFit.cover,
                                          ),
                                        )))
*/
                              ],
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                data[index].title,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,

                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 13),
                            ),
                            Row(
                              children: <Widget>[
                                Text(data[index].authorName,style: TextStyle(fontSize: 11),),
                                Padding(
                                    padding: EdgeInsets.all(5),
                                    ),
                                Padding(
                                  padding: EdgeInsets.only(top: 10, bottom: 10),
                                  child: Text(
                                    parseDDMMYYYY(data[index].publishDate),
                                    style: TextStyle(fontSize: 12,fontWeight: FontWeight.normal,color: Colors.grey),
                                    softWrap: false,
                                    overflow: TextOverflow.fade,
                                  ),
                                ),
                                Spacer(),
                                data[index].commentCount!="0"?
                                Padding(
                                  padding: EdgeInsets.only(right: 2),
                                  child:  Text(data[index].commentCount,
                                    style: TextStyle(color: Colors.grey,fontSize: 11),),
                                ):
                                Opacity(
                                  opacity: 0.0,
                                  child:  Padding (
                                    padding: EdgeInsets.only(right: 4),
                                    child: Text(data[index].commentCount,style:
                                    TextStyle(color: Colors.grey,
                                        fontSize: 12),
                                    ),
                                  ),
                                ),
                                data[index].commentCount=="0"?
                                Opacity(
                                  opacity: 0.0,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      Icons.message,
                                      size: 15,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ):
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(
                                    Icons.message,
                                    size: 15,
                                    color: Colors.grey,
                                  ),
                                ),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: popUpMenuButton(data,index),
                                  ),
                              ],
                            ),
                          ],
                        )),
                    Padding(padding: EdgeInsets.all(5)),

                    Padding(
                      padding: EdgeInsets.only(right: 15),
                    )
                  ],
                ),
              ),
            )));
  }

  /*Big card view*/
  Widget getSingleListItemWidgetBigCard(List data, int index) {
    return Container(
        margin: EdgeInsets.all(0),
        child: (index == data.length - 1 && loadingMore)
            ? Align(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: SizedBox(
                  width: 40,
                  height: 40,
                  child: CircularProgressIndicator(
                    strokeWidth: 1,
                  )),
            ))
            : GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PostDetailsStatefull(data[index])));
            },
            child: Padding(
                padding: EdgeInsetsDirectional.only(
                    top: 15, bottom: 15, start: 15, end: 15),
                child: Container(
                  /*  decoration: new BoxDecoration(
                    border: new Border(
                        bottom: BorderSide(
                          color: Colors.grey,
                          width: 0.2,
                        ))),*/
                  child: Card(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                  child: Padding(
                                      padding:
                                      EdgeInsets.only(right: 0, bottom: 0),
                                      child: ClipRRect(
                                        borderRadius:
                                        new BorderRadius.circular(5.0),
                                        child: Image.network(
                                          data[index].previewImage == null
                                              ? ''
                                              : data[index].previewImage,
                                          height: 140,
                                          fit: BoxFit.cover,
                                        ),
                                      )))
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Padding(padding: EdgeInsets.all(8)),
                              Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.all(5),
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          data[index].title,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 17),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Text(data[index].authorName,style: TextStyle(fontSize: 12),),
                                          Padding(
                                              padding: EdgeInsets.all(5),
                                             /* child: Icon(
                                                Icons.adjust,
                                                size: 5.0,
                                              )*/
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 10, bottom: 10),
                                            child: Text(
                                              parseDDMMYYYY(
                                                  data[index].publishDate),
                                              style: TextStyle(fontSize: 12,fontWeight: FontWeight.normal,color: Colors.grey),
                                              softWrap: false,
                                              overflow: TextOverflow.fade,
                                            ),
                                          ),
                                          Spacer(),
                                        ],
                                      ),
                                    ],
                                  )),
                              Padding(
                                padding: EdgeInsets.only(right: 15),
                              ),
                              data[index].commentCount!="0"?
                              Padding(
                                padding: EdgeInsets.only(right: 2),
                                child:  Text(data[index].commentCount,
                                  style: TextStyle(color: Colors.grey,fontSize: 11),),
                              ):
                              Opacity(
                                opacity: 0.0,
                                child:  Padding (
                                  padding: EdgeInsets.only(right: 4),
                                  child: Text(data[index].commentCount,style:
                                  TextStyle(color: Colors.grey,
                                      fontSize: 12),
                                  ),
                                ),
                              ),
                              data[index].commentCount=="0"?
                              Opacity(
                                opacity: 0.0,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(
                                    Icons.message,
                                    size: 15,
                                    color: Colors.grey,
                                  ),
                                ),
                              ):
                              Align(
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  Icons.message,
                                  size: 15,
                                  color: Colors.grey,
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: popUpMenuButton(data,index),
                              ),
                            ],
                          ),
                        ],
                      )),
                ))));
  }

  Future<Null> _onRefresh() {

    Completer<Null> completer = new Completer<Null>();
    new Timer(new Duration(seconds: 1), () {
      print("timer complete");
      completer.complete();
    });

    return completer.future;

  }

}
