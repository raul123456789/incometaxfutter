import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/constants/constants_dropdown.dart';
import 'package:listview/network/network.dart' as network;


class AsKQuery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final apptitle = "Ask Query";

    return Scaffold(
      appBar: AppBar(
        title: Text(apptitle),
        backgroundColor: Hexcolor('#008B8B'),
      ),
      body: AskQueryForm(),
    );
  }
}

class AskQueryForm extends StatefulWidget {
  @override
  _AskQueryFormState createState() => _AskQueryFormState();
}

class _AskQueryFormState extends State<AskQueryForm> {

  final _formKey = new GlobalKey<FormState>();

  bool _showLoaderAskQuery = false;

  final controllerTitle = TextEditingController();
  final controllerQuery = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Form(

      key: _formKey,
      child: SingleChildScrollView(

        child: Padding(
            padding: EdgeInsets.all(16),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                TextFormField(
                  decoration: new InputDecoration(
                    hintText: 'Enter a Query title',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 0.2,
                      ),
                    ),
                  ),
                  controller: controllerTitle,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLength: null,
                  maxLines: 22,
                  decoration: new InputDecoration(
                    hintText: 'Enter your query',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 0.2,
                      ),
                    ),
                  ),
                  controller: controllerQuery,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),
                new Text(
                  '                        Powered by CAClubIndia',
                  style: new TextStyle(
                      fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.black),
                ),
                (!_showLoaderAskQuery)?
                    Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Align(
                        alignment: Alignment.center,
                        child: GestureDetector(
                            onTap: () async {
                              if (_formKey.currentState.validate()) {
                                // If the form is valid, display a Snackbar.
                                submitQuery();
                              }
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(0)),
                                  color: Hexcolor('#008B8B'),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 20,
                                      bottom: 20,
                                      left: 138,
                                      right: 138),
                                  child: Text(
                                    "SUBMIT",
                                    style: TextStyle(color: Colors.white,
                                        fontWeight:FontWeight.bold),
                                  ),
                                ))))) 
                          :Center(

                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ))
                          )
              ],
            ),
        ),
      ),
    );
  }

  submitQuery() async {

      setState(() {
        _showLoaderAskQuery=true;
      });

      HashMap<String, String> params = new HashMap<String, String>();
      params[QUERY_TITLE]=controllerTitle.text;
      params[QUERY_BODY]=controllerQuery.text;

      String response = await network.genericNetworkRequest(params, POST_IN_FORUM);

      var resArr = response.split("#");

      setState(() {
        _showLoaderAskQuery=false;
      });

      if(resArr[0].toLowerCase()=="true"){

        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Query Posted Sucessfully")));

      }else{
        Scaffold.of(context).showSnackBar(SnackBar(content: Text("Failed to Post your Query")));

      }





  }
}

