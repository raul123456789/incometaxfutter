import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/act/SectionList.dart';
import 'package:listview/act/SectionListPage.dart';
import 'package:listview/CommonModule/CommonModule.dart';
import 'package:listview/constants/const_module.dart';
import 'package:listview/db/local_db.dart';
import 'package:listview/network/network.dart' as network;

class BookmarkPage extends StatelessWidget {
   String id="";

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      body: DefaultTabController(
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Hexcolor('#008B8B'),
              title: Text("Bookmarks/Notes"),

              bottom: TabBar(
                labelColor: Colors.white,
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  Tab(text: "BOOKMARKS"),
                  Tab(text: "NOTES"),
                  Tab(text: "     OTHER \nBOOKMARKS")
                ],
              ),

            ),//Appbar

            body: TabBarView(children: <Widget>[
              //new CommonModule("Bookmark", network.fetchPost("",jsonMap:{"bookmarkonly":"2","user_id":"3"})),
              new SectionListPage(getSectionsBookmarked(id),
              ),
              new CommonModule("Notes", network.fetchPost("",jsonMap:{"notesonly":"2","user_id":"3"})),
              new CommonModule("Other Bookmarks", network.fetchPost("",jsonMap:{"bookmarkonly":"1","user_id":"2"})),
            ]),
          )
      ),
    );
  }
}