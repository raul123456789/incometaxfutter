import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/User.dart';
import 'package:listview/followers/followers_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/network/network.dart' as network;


class Followers extends StatelessWidget {

  String memberId;
  int initialIndex;
  Followers(this.memberId,this.initialIndex);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: DefaultTabController(

          initialIndex: initialIndex,
          length: 2,
          child: Scaffold(

            appBar: AppBar(
              title: Text(""),
              bottom: TabBar(tabs: <Widget>[

                Tab(text: "Followers"),
                Tab(text: "Following"),
              ],),
            ),
            body: TabBarView(children: <Widget>[
              FollowersList("Followers", getFollowers()),
              FollowersList("Followings", getFollowings()),

            ]),
          )),

    );
  }

  Future<List<User>> getFollowers() async {

    SharedPreferences pref = await SharedPreferences.getInstance();

    Map<String, String> params = Map();
    params[MEMBER_ID_STR] = "2";
    params[FRIEND_ID_STR] = memberId;
    params[KEY_API_FOLLOW_DATA_TYPE] = "followers";


    String res = await network.genericNetworkRequest(params, FOLLOW_DATA);

    List<User> followings = network.parseFollowerFollowing(res);

    return followings;

  }

  Future<List<User>> getFollowings() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    Map<String,String> params= new Map();
    params[MEMBER_ID_STR]= "2";
    params[FRIEND_ID_STR] =memberId;
    params[KEY_API_FOLLOW_DATA_TYPE]="following";

    String res=await network.genericNetworkRequest(params, FOLLOW_DATA);

    List<User> followings=network.parseFollowerFollowing(res);


    return followings;
  }



}
