import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/User.dart';
import 'package:listview/profile/profile.dart';


class FollowersList extends StatelessWidget {

  Future<List<User>> followers;
  String title;
  ScrollController _scrollController=new ScrollController();

  FollowersList(this.title,this.followers);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Center(
        child: FutureBuilder<List<User>>(
            future: followers,
            builder: (context,snapshot){
              if(snapshot.hasData){
                getUserList(snapshot.data,context);
              }else if(snapshot.hasError){
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();
            }),
      ),
    );
  }

  Widget getUserList(List<User> data, BuildContext context) {

    return ListView.builder(
        itemCount: data.length,
        controller: _scrollController,
        itemBuilder: (context,index)=>getSingleListItem(data[index],context));

  }

  getSingleListItem(User user, BuildContext context) {

    return ListTile(
        title: Text(user.name),
        subtitle: Text(user.occupation),
        leading: ClipOval(
          child: Image.network(
            CCI_BASE_URL_PROFILE_IMAGES +
                "my_avatars/" +
                user.memberId+
                ".jpg",
            height: 50,
            width: 50,
            fit: BoxFit.cover,
          ),
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Profile(user.memberId)));
        }
    );

  }


}
