import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/Inbox/MessageList.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/model/Conversation.dart';
import 'package:listview/network/network.dart' as network;
import 'package:shared_preferences/shared_preferences.dart';


class Inbox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Inbox"),
          backgroundColor: Hexcolor('#008B8B')
      ),
      body: new Container(
        child: new Center(
          child: FutureBuilder<List<Conversation>>(
            future: getConversations(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if(snapshot.data.length==0)
                  return Text("No messages found");
                else
                  return displayConversations(snapshot.data);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }

  Future<List<Conversation>> getConversations() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    HashMap<String, String> params = new HashMap();
    params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);

    Future<String> res=network.genericNetworkRequest(params, PERSONAL_MESSAGE_OF_A_USER);

    List<Conversation> conversations=network.parseConversation(await res);

    return conversations;

  }


  Widget displayConversations(List<Conversation> data) {

    return ListView.builder(

        itemCount: data.length,
        itemBuilder:(context,index)=>Card(

         elevation: 0.0,
          child: ListTile(

             title: new Text(data[index].SenderName),
             subtitle: new Text(data[index].LastMsgBody),
             leading: ClipOval(
               child: Image.network(
                 CCI_BASE_URL_PROFILE_IMAGES +
                     "my_avatars/" +
                     data[index].SenderId+
                     ".jpg",
                 height: 50,
                 width: 50,
                 fit: BoxFit.cover,
               ),
             ),
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MessageList(data[index])));
            },




          ),
        )
    );




  }
}
