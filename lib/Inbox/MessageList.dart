import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/model/Conversation.dart';
import 'package:listview/model/Message.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/network/network.dart' as network;



class MessageList extends StatefulWidget {

  Conversation _conversation;

  MessageList(this._conversation);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MessageListState(_conversation);
  }
}

class _MessageListState extends State<MessageList>  with TickerProviderStateMixin{

  Conversation _conv;

  String memberId;

  _MessageListState(this._conv);

  TextEditingController commentTextController;

  Future<List<Message>> msgList;
  ScrollController _scrollController = new ScrollController();

  bool loadingMore = false;

  @override
  void initState() {
    // TODO: implement initState
    commentTextController=TextEditingController();
    msgList = getMessages();
    super.initState();

    _scrollController.addListener(() async {
      if (_scrollController.position.maxScrollExtent ==
          _scrollController.offset) {
        //_getMoreData();

        if (!loadingMore) {
          setState(() {
            msgList.then(
                    (list) => {list.insert(list.length, null), loadingMore = true});
          });
        }

        List<Message> tempListMoreData = await getMessages();

        setState(() {
          msgList.then((oldList) => {
            if (oldList[oldList.length - 1] == null) {oldList.removeLast()},
            if (tempListMoreData.length > 0)
              oldList.addAll(tempListMoreData),
            loadingMore = false
          });
        });
      }
    });

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_conv.SenderName),
      ),
      body: Stack(
        children: <Widget>[
            Container(

                margin: EdgeInsets.only(bottom: 60),
                child: new Center(

                  child: FutureBuilder<List<Message>>(
                      future: msgList,
                      builder: (context,snapshot) {

                        if(snapshot.hasData){
                          return displayMessages(snapshot.data);
                        }else if (snapshot.hasError){
                            return Text("${snapshot.error}");

                          }

                        return CircularProgressIndicator();
                      }
                  ),
                ),
            ),

            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                decoration: new BoxDecoration(
                    border:
                    Border(top: BorderSide(color: Colors.black12, width: 0.5))),
                child: Row(
                  children: <Widget>[
                    Flexible(
                        child: TextField(
                          controller: commentTextController,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(5),
                              border: InputBorder.none,
                              hintText: 'Enter your message'),
                      )),
                  IconButton(
                    icon: const Icon(Icons.send),
                    color: Colors.blue,
                    tooltip: 'Submit',
                    onPressed: () {
                      sendMsg();
                    },
                  ),
                ],
              ),
            ),)




        ],
      ),

    );
  }

  Future<List<Message>> getMessages() async {

          SharedPreferences prefs = await SharedPreferences.getInstance();
          memberId = prefs.getString(MEMBER_ID_STR);

          HashMap<String, String> params = new HashMap();
          params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);
          params[KEY_API_PM_SENDER] = _conv.SenderId;

          msgList.then((list){
            if(list.length>2 && loadingMore)
            {
              params[KEY_API_PM_ID]=list[list.length-2].PMId;
            }
          });

          Future<String> res = network.genericNetworkRequest(
              params, PERSONAL_MESSAGES_FROM_A_SPECIFIC_USER);

          List<Message> messages = network.parseMessagesOfAConversation(await res);

    return messages;


  }

  Widget displayMessages(List<Message> data) {

      return ListView.builder(

          controller: _scrollController,
          reverse: true,
          itemCount : data.length,
          itemBuilder: (context, index) => (data[index] == null)
              ? Align(
              alignment: Alignment.centerRight,
              child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ClipRRect(
                            borderRadius: new BorderRadius.circular(5.0),
                            child: Container(
                              color: Colors.black12,
                              padding: EdgeInsets.all(16),
                              child: SizedBox(
                                  width: 20.0,
                                  height: 20.0,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 1,
                                  )),
                            )),
                        Padding(
                          padding: EdgeInsets.all(5),
                        ),
                        ClipOval(
                          child: Image.network(
                            CCI_BASE_URL_PROFILE_IMAGES +
                                "my_avatars/" +
                                memberId +
                                ".jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        )
                      ],
                    ),
                  )))
              : Align(
              alignment: (memberId == data[index].SenderId)
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Wrap(
                    children: <Widget>[
                      Container(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              (memberId != data[index].SenderId)
                                  ? ClipOval(
                                child: Image.network(
                                  CCI_BASE_URL_PROFILE_IMAGES +
                                      "my_avatars/" +
                                      data[index].SenderId +
                                      ".jpg",
                                  height: 50,
                                  width: 50,
                                  fit: BoxFit.cover,
                                ),
                              )
                                  : Container(),
                              Padding(
                                padding: EdgeInsets.all(5),
                              ),
                              ClipRRect(
                                  borderRadius: new BorderRadius.circular(5.0),
                                  child: Container(
                                      color: (memberId == data[index].SenderId)
                                          ? Colors.black12
                                          : Colors.grey,
                                      padding: EdgeInsets.all(16),
                                      child: Column(
                                        crossAxisAlignment:
                                        (memberId == data[index].SenderId)
                                            ? CrossAxisAlignment.end
                                            : CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            data[index].PMBody,
                                          )
                                        ],
                                      ))),
                              Padding(
                                padding: EdgeInsets.all(5),
                              ),
                              (memberId == data[index].SenderId)
                                  ? ClipOval(
                                child: Image.network(
                                  CCI_BASE_URL_PROFILE_IMAGES +
                                      "my_avatars/" +
                                      memberId +
                                      ".jpg",
                                  height: 50,
                                  width: 50,
                                  fit: BoxFit.cover,
                                ),
                              )
                                  : Container(),
                            ],
                          ))
                    ],
                  ))));

  }

  void sendMsg() async {

    setState(() {
      msgList.then((list) {
        list.insert(0, null);
      });
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> parameters = new HashMap();
    parameters[KEY_API_PM_TO] = _conv.SenderId;
    parameters[KEY_API_PM_FROM] = prefs.getString(MEMBER_ID_STR);
    parameters[KEY_API_PM_BODY] = commentTextController.text;

    String res =
    await network.genericNetworkRequest(parameters, SEND_PERSONAL_MESSAGES);

    Future<List<Message>> newList = getMessages();
    msgList = newList;

    setState(() {});
  }


}

