class Post {

  String _title,_name,_info;

  Post(this._title,this._name,this._info);

  get info => _info;
  get name => _name;
  get title => _title;

}