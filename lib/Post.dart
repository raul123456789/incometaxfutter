import 'package:listview/constants/const_api.dart';
import 'package:listview/network/ApiUrl.dart' as urls;
import 'package:listview/ApiUrl.dart' as urls;

class Post {

  String title = "";
  String links;
  String authorName = "";
  String previewImage;
  String previewImageUrl;
  String countryName="";
  String categoryName;
  String countryId;
  String moduleName = "";
  String publishDate;
  String itemId;
  String categoryId;
  String memberId;
  String modeItemId;
  String avatarUrl;
  String eventDate;
  String url;
  bool commentAllow = true;
  String commentCount;
  bool shareEnabled;
  bool isBookmarked = false;
  bool showAsBigCard = false;

  String getPreviewImageUrl() {
      return CCI_BASE_URL_IMAGES;
  }

  String getPostUrl() {
      return urls.CCI_BASE_URL_HTTPS+"webapp/"+this.moduleName+"/details.asp?mod_id="+this.modeItemId;
  }

  String getShareUrl() {
      return urls.CCI_BASE_URL_HTTP+"share_files/details.asp?mod_id="+this.modeItemId;
  }

  bool isPreviewImagePresent() {
      return !(previewImage==CCI_BASE_URL_IMAGES);
  }


}