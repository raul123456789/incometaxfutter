import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

String parseDate(String publishDate) {
  if (publishDate != null) {
    DateFormat format = new DateFormat("dd/MM/yyyy");
    print(publishDate);
    print(format.parse(publishDate));

    DateTime dt = format.parse(publishDate);

    return dt.day.toString();
  }

  return "";
}

String parseDDMMYYYY(String publishDate) {

  var currentDate = new DateTime.now();
  String mnth;

  if (publishDate != "") {
    DateFormat format = new DateFormat("dd/MM/yyyy");
    print(publishDate);
    print(format.parse(publishDate));

    DateTime dt = format.parse(publishDate);
    print(dt.month);

    // var dte = dt.month.toString();
    var duration = currentDate.difference(dt).inDays;
    var timehr = currentDate.difference(dt).inHours;
    var mint= currentDate.difference(dt).inMinutes;

    if(timehr < 24) {
      return timehr.toString()+" hrs ago";
    } else if(duration > 6) {
      mnth=parseMonth(publishDate.toString());
      return mnth.toString()+"  "+dt.day.toString();
    }else if(duration == 1) {
      return "Yesterday";
    }else if(mint < 60) {
      return mint.toString()+" min ago";
    } else {
        return duration.toString() + " days ago";
    }
    //return "Posted on "+dt.day.toString()+"/"+dt.month.toString()+"/"+dt.year.toString();
  }
  return "";

}

String parseTime(String publishDate) {

  var currentTime = new DateTime.now();

            /*Fluttertoast.showToast(
                msg: publishDate.toString(),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 3,
                textColor: Colors.white,
                fontSize: 14.0
            );*/

  if(publishDate!=null) {

    DateFormat format = new DateFormat("hh/MM/ss");
    print(publishDate);
    print(format.parse(publishDate));
    DateTime dt = format.parse(publishDate);

    /*Fluttertoast.showToast(
                msg: dt.hour.toString(),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 3,
                textColor: Colors.white,
                fontSize: 14.0
            );*/

    var time = ('${currentTime.hour-dt.hour}');

    return time.toString()+"hr. ago";

  }

  return "";

}



String parseMonth(String publishDate) {
  if (publishDate != "") {
    DateFormat format = new DateFormat("dd/MM/yyyy");
    print(publishDate);
    print(format.parse(publishDate));

    DateTime dt = format.parse(publishDate);

    var month = "jan";

    switch (dt.month.abs()) {
      case 1:
        month = "Jan";
        break;
      case 2:
        month = "Feb";
        break;
      case 3:
        month = "Mar";
        break;
      case 4:
        month = "Apr";
        break;
      case 5:
        month = "May";
        break;
      case 6:
        month = "Jun";
        break;
      case 7:
        month = "Jul";
        break;
      case 8:
        month = "Aug";
        break;
      case 9:
        month = "Sep";
        break;
      case 10:
        month = "Oct";
        break;
      case 11:
        month = "Nov";
        break;
      case 12:
        month = "Dec";
        break;
    }
    return month;
  }
  return "";
}





