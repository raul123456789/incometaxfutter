import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:listview/Chapters/Chapter.dart';
import 'package:listview/db/local_db.dart';

class ChapterList extends StatefulWidget {

  Future<List<Chapter>> chapters;

  ChapterList(this.chapters);

  @override
  _ChapterListState createState() => _ChapterListState(chapters);
}

class _ChapterListState extends State<ChapterList> {

  static final String TITLE="Chapters";

  Future<List<Chapter>> chapters;

  List<Chapter> chaptersFiltered;

  _ChapterListState(this.chapters);

  Chapter thisChapter;

  int index;

  Widget appBarTitle = new Text(TITLE);
  Icon actionIcon = new Icon(Icons.search);

  String searchStr="";

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: new Container(
          child: new Center(
            child: (searchStr.isEmpty)?FutureBuilder<List<Chapter>>(
              future: chapters,
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  return displayChapters(snapshot.data);
                }else if(snapshot.error){
                  return Text("${snapshot.error}");
                }
                return CircularProgressIndicator();
              },
            ):displayChapters(chaptersFiltered),
          ),
        ),
    );
  }

  ListView displayChapters(List<Chapter> data) {

    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) => Card(
            margin: EdgeInsets.all(0),
            elevation: 0.0,
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border(
                      bottom: BorderSide(
                        color: Colors.grey,
                        width: 0.2,
                      ))
              ),
              child: ListTile(
                title: new Text(
                  data[index].title.toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,),
                ),

                subtitle: new Text(data[index].summery,
                  style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.normal),),
                contentPadding: EdgeInsets.only(bottom: 9,top: 6,left: 12,right: 5),
                onTap: () {
                  if(Platform.isAndroid) {
                    /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));*/
                  }
                  else if(Platform.isIOS)
                  {
                    /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));*/
                  }
                },
              ),
            )
        ));



  }
}




