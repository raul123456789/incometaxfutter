class Chapter {

  String _id;
  String _title="";
  String _summery="";
  String _details="";
  String _actId="";

  Chapter([this._id,this._title,this._summery,this._details,this._actId]);

  Map<String, dynamic> toMap() {
    return {
      'id' : _id,
      'title': _title,
      'summery':_summery,
      'details':_details,
      'act_id':_actId,
    };
  }


  String get id => _id;

  set id(String value) {
    _id = value;
  }

  String get details {

    _details=_details.replaceAll("@#","'");
    _details=_details.replaceAll("*^","\"");

    int lastDigit=int.parse(_id[_id.length-1]);
    int newRefDigit=(lastDigit+1)>9?0:(lastDigit+1);

    String firstReplaceCombo=getSymbolToConvert(lastDigit)+getSymbolToConvert(newRefDigit);
    String firstSymbol=getSymbolToConvert(lastDigit);
    String secondSymbol=getSymbolToConvert(newRefDigit);

    _details=_details.replaceAll(firstReplaceCombo, "/");
    _details=_details.replaceAll(firstSymbol, "<");
    _details=_details.replaceAll(secondSymbol, ">");

    /*
    * Add uri protocol to the interlinking links ; otherwise 'navigationDelegate' doesn't get called
    * */
    _details=_details.replaceAll("href=\"chapter#", "href=\"http://chapter#");

    return _details;
  }

  String getSymbolToConvert(int mynumber) {
    String convertnumtosymbol="";

    if (mynumber==0)
      convertnumtosymbol="~";
    else if (mynumber==1 )
      convertnumtosymbol="\$";
    else if (mynumber==2 )
      convertnumtosymbol="^";
    else if (mynumber==3 )
      convertnumtosymbol="|";
    else if (mynumber==4 )
      convertnumtosymbol="`";
    else if (mynumber==5)
      convertnumtosymbol="}";
    else if (mynumber==6)
      convertnumtosymbol="{";
    else if (mynumber==7 )
      convertnumtosymbol="(";
    else if (mynumber==8)
      convertnumtosymbol=")";
    else if (mynumber==9 )
      convertnumtosymbol="_";

    return convertnumtosymbol;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get summery => _summery;

  set summery(String value) {
    _summery = value;
  }

  String get actId => _actId;

  set actId(String value) {
    _actId = value;
  }


}