import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/profile/profile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:listview/model/Slider.dart' as sl;
import 'package:listview/constants/web.dart';



class LeaderboardScore extends StatefulWidget {


  bool prev=false;
  Future<List> scores;

  LeaderboardScore(this.scores);

 @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LeaderboardScoreState(scores);
  }
}

class LeaderboardScoreState extends State<LeaderboardScore> {

  Future<List> scores;
  bool prev=false;
  String moduleId;

  ScrollController _scrollController = new ScrollController();
  bool loadingMore=false;

  LeaderboardScoreState(this.scores);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

   _scrollController.addListener(()async {

      if(_scrollController.position.maxScrollExtent == _scrollController.offset){

        if (!loadingMore) {
          setState(() {
            scores.then(
                    (list) => {list.insert(list.length, null), loadingMore = true});
          });
        }
        HashMap<String, String> params = HashMap();
        scores.then((list) {
          if (list.length > 2) {
            params[KEY_API_ITEM_ID] = list[list.length - 2].itemId;

            if(moduleId!=null)
              params[KEY_MODULE_ID]=moduleId;

            params["getold"] = "1";
          }
        });




      }




    });


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:
        new Container(
          child: new Center(
            child: FutureBuilder<List>(
              future: scores,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return scoreList(snapshot.data);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              },
            ),
          ),
        )
    );
  }

  Widget scoreList(List data) {

    return ListView.builder(

        itemCount: data.length,
        itemBuilder: (context,index)=>Card(

         elevation: 0,
          child:
          (index==0)?
          Container(
              height: 150,
              child: createSlider(data[0]))
              :
          ListTile(
            title: new Text(data[index].name,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.blue),),
            subtitle: new Text(data[index].profession,style: TextStyle(fontWeight:FontWeight.bold,color: Colors.black),),
            trailing: Container(
                  height: 20.0,
                  width: 100.0,
                      decoration: new BoxDecoration(
                        color: Colors.blue,
                        shape: BoxShape.rectangle,
                        border: new Border.all(
                          color: Colors.blue,
                          width: 1.0,
                        )
                      ),
                  child: Text(data[index].score+" points",
                          style: TextStyle(fontSize: 11,color:
                                    Colors.white,fontWeight: FontWeight.bold)
                                      ,textAlign: TextAlign.center,),
            ),
            leading: ClipOval(
              child: Image.network(
                CCI_BASE_URL_PROFILE_IMAGES +
                    "my_avatars/" +
                    data[index].memberId+
                    ".jpg",
                height: 50,
                width: 50,
                fit: BoxFit.cover,
              ),
            ),
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Profile(data[index].memberId)));
            },
          ),
        ));

  }

  Widget createSlider(List<sl.Slider> slider) {

    return ListView.builder(

        scrollDirection: (slider.length>1)?Axis.horizontal:Axis.vertical,
        itemCount: slider.length,
        itemBuilder: (context,index)=>Container(
          height: 150,
          child: GestureDetector(
            child:
            Image.network(
              slider[index].imgURL,
              fit: BoxFit.fill,
            ),
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) =>Web(slider[index].url,"Leaderboard")));

            },

          ),
        ),
    );


  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }


}
