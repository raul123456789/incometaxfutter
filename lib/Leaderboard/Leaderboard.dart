import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/Leaderboard/LeaderboardScore.dart';
import 'package:listview/constants/constants.dart';


class Leaderboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: DefaultTabController(
           length: 2,
           child: Scaffold(
             appBar: AppBar(
               backgroundColor: Hexcolor('#008B8B'),
               title: Text("Leaderboard"),
               bottom: TabBar(
                 labelColor: Colors.white,
                 indicatorColor: Hexcolor('#008B8B'),
                 tabs: <Widget>[
                   Tab(text: "This week"),
                   Tab(text: "All time"),
                 ],
               ),
             ),//Appbar
             body: TabBarView(children: <Widget>[
                 new LeaderboardScore(getScores(false)),
                 new LeaderboardScore(getScores(true)),
             ]),
           )
       ),
    );
  }

  Future<List> getScores(bool prev) async {

    HashMap<String,String> map = HashMap();
    map[KEY_MODULE_NAME]=VALUE_MODULE_LEADERBOARD;
    map[FRIEND_ID_STR]=MEMBER_ID_STR;

    if(prev)
      map[KEY_PREV]="1";

    String response = await network.genericNetworkRequest(map, GET_LEADERBOARD);

    List scores=await network.parseScore(response);

    return scores;

  }





}
