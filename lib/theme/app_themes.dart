import 'package:flutter/material.dart';

enum AppTheme {
  Light,
  Dark,
}

final ThemeData base = ThemeData.light();
final ThemeData baseDark = ThemeData.dark();
final appThemeData = {
  AppTheme.Light: base.copyWith(
    primaryColor: Colors.black,
    primaryColorDark: Colors.black,
    accentColor: Colors.black,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: Colors.black,
      textTheme: ButtonTextTheme.normal,
    ),
    cardColor: Colors.white,
    textTheme: _buildTextTheme(base.textTheme),
    tabBarTheme: _buildTabarTheme(base.tabBarTheme),
    iconTheme: _buildIconTheme(base.iconTheme),
    floatingActionButtonTheme:
    _buildFABThemeDark(base.floatingActionButtonTheme),
  ),
  AppTheme.Dark: baseDark.copyWith(
    primaryColor: Colors.black,
    primaryColorDark: Colors.black,
    accentColor: Colors.black,
    buttonTheme: baseDark.buttonTheme.copyWith(
      buttonColor: Colors.black,
      textTheme: ButtonTextTheme.normal,
    ),
    cardColor: baseDark.cardColor,
    textTheme: _buildTextThemeDark(baseDark.textTheme),
    tabBarTheme: _buildTabarThemeDark(baseDark.tabBarTheme),
    iconTheme: _buildIconThemeDark(baseDark.iconTheme),
    floatingActionButtonTheme:
    _buildFABThemeDark(baseDark.floatingActionButtonTheme),
  )
};

_buildFABThemeDark(FloatingActionButtonThemeData base) {
  return base.copyWith(
      backgroundColor: Colors.black, foregroundColor: Colors.white);
}

TextTheme _buildTextTheme(TextTheme base) {
  return base
      .copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,

    ),
    title: base.title.copyWith(fontSize: 18.0),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  )
      .apply(
    fontFamily: 'Hind',
  );
}

TextTheme _buildTextThemeDark(TextTheme base) {
  return base
      .copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(fontSize: 18.0),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14.0,
    ),
  )
      .apply(
    fontFamily: 'Hind',
  );
}

_buildTabarTheme(TabBarTheme base) {
  return base.copyWith(
    labelColor: Colors.black54,
  );
}

_buildIconTheme(IconThemeData base) {
  return base.copyWith(color: Colors.black54);
}

_buildTabarThemeDark(TabBarTheme base) {
  return base.copyWith(
    labelColor: Colors.white,
  );
}

_buildIconThemeDark(IconThemeData base) {
  return base.copyWith(color: Colors.white70);
}
