import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:listview/theme/app_themes.dart';
import 'package:listview/theme/theme_event.dart';
import 'package:listview/theme/theme_state.dart';


class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {

  bool initialDark=false;
  ThemeBloc(this.initialDark);

  @override
  // TODO: implement initialState

  ThemeState get initialState =>
      ThemeState(themeData:appThemeData[(initialDark)?AppTheme.Dark:AppTheme.Light]);

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    if (event is ThemeChanged) {
      yield ThemeState(themeData: appThemeData[event.theme]);
    }
  }







}