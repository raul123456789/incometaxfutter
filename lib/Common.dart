import 'package:flutter/material.dart';
import 'package:listview/posts.dart';
import 'package:xml/xml.dart' as xml;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomepage(),
    );
  }
}

class MyHomepage extends StatelessWidget {

  Future<List<Post>> getPostFromXML(BuildContext context) async {

    String xmlString = await DefaultAssetBundle.of(context).loadString("assets/articles.xml");
    var raw  = xml.parse(xmlString);
    var elements = raw.findAllElements("r");

    return elements.map((element){

      return Post(element.findElements("t").first.text
          ,element.findElements("n").first.text
          ,element.findElements("i").first.text);
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: Text("Data parser"),
      ),*/
      body: Container(
        child: FutureBuilder(
          future: getPostFromXML(context),
          builder: (context,data){
            if(data.hasData){
              List<Post> posts = data.data;

              return ListView.builder(

                  itemCount: posts.length,
                  itemBuilder: (context, index){
                    return ListTile(
                      title:
                      Text(posts[index].title, style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),),
                      subtitle: Text(posts[index].name),
                    );
                  }
              );
            }
            else{
              return Center(child: CircularProgressIndicator(),);
            }
          },

        ),

      ),

    );
  }
}
