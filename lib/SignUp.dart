import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:crypto/crypto.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/network/network.dart' as network;
import 'package:shared_preferences/shared_preferences.dart';
import 'loginemail.dart';


class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: ProfileUpdateForum(),


    );
  }
}

//form widget
class ProfileUpdateForum extends StatefulWidget {
  @override
  _ProfileUpdateForumState createState() => _ProfileUpdateForumState();
}

class _ProfileUpdateForumState extends State<ProfileUpdateForum> {

  final _formKey = GlobalKey<FormState>();

//  var prefs = SharedPreferences.getInstance();

  SharedPreferences prefs;

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  // your list of DropDownMenuItem
  List<DropdownMenuItem<String>> menuItems = List();

  String fcmToken = "";

  String memberId = "";

  /*bool showSpecifyCountry = false;
  bool showSpecifyQualification = false;*/
  bool agreeTOS = false;
  bool showLoader = false;

  var controllerName = new TextEditingController();
  var controllerEmail = new TextEditingController();
  var controllerPass = new TextEditingController();

  /* var controllerOtherCountry = new TextEditingController();
  var controllerOtherQuali = new TextEditingController();*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    configureFCM();
  }


  @override
  Widget build(BuildContext context) {
    final mq = MediaQueryData.fromWindow(window);

    return Container(
      color: Colors.black,
      child: SingleChildScrollView(
        child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(height: mq.size.height),
            child: Container(
                height: double.infinity,
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Create a new account",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 40),
                        ),
                        TextFormField(
                          style: TextStyle(color: Colors.white),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Color(0xff2A2A2A),
                            hintText: 'Enter name',
                            hintStyle: TextStyle(color: Colors.white70),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide:
                              BorderSide(color: Colors.grey, width: 0.2),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 0.2,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter your name';
                            }
                            return null;
                          },
                          controller: controllerName,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        TextFormField(
                          style: TextStyle(color: Colors.white),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Color(0xff2A2A2A),
                            hintText: 'Enter email',
                            hintStyle: TextStyle(color: Colors.white70),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide:
                              BorderSide(color: Colors.grey, width: 0.2),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 0.2,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter email';
                            }
                            return null;
                          },
                          controller: controllerEmail,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        TextFormField(
                          obscureText: true,
                          style: TextStyle(color: Colors.white),
                          decoration: new InputDecoration(
                            filled: true,
                            fillColor: Color(0xff2A2A2A),
                            hintText: 'Enter password',
                            hintStyle: TextStyle(color: Colors.white70),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide:
                              BorderSide(color: Colors.grey, width: 0.2),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                color: Colors.grey,
                                width: 0.2,
                              ),
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a password';
                            }
                            return null;
                          },
                          controller: controllerPass,
                        ),
                        /*
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                          ),
                          Row(
                            children: <Widget>[
                              Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor: Colors.grey),
                                  child: Checkbox(value: agreeTOS,
                                    activeColor: Colors.grey,
                                    onChanged: (v) => tosChange(v),
                                  )),
                              Flexible(child: Text("I agree to the Terms of Service and Privacy Policy", style: TextStyle(color:Colors.white),),)

                            ],
                          ),*/
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        (!showLoader)
                            ? GestureDetector(
                            onTap: () => attemptSignUp(),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(40),
                                child: Container(
                                    height: 50,
                                    width: double.infinity,
                                    color: Colors.white,
                                    child: new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        new Container(
                                            padding: EdgeInsets.only(
                                                left: 10.0, right: 10.0),
                                            child: new Text(
                                              "Sign up",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.black,
                                              ),
                                            )),
                                      ],
                                    ))))
                            : Center(
                            child: CircularProgressIndicator(
                              valueColor:
                              new AlwaysStoppedAnimation(Colors.white),
                            )),
                      ],
                    ),
                  ),
                )) //Container
        ), //ConstrainedBox
      ), //SingleChildScrollView
    );
  }


  void configureFCM() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.getToken().then((token) {
      //print("========================="+token);

      fcmToken = token;
    });
  }

  attemptSignUp() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        showLoader = true;
      });
    }

    HashMap<String, String> userinfo = new HashMap<String, String>();
    userinfo[UPDATE_VIA] = "13";
    userinfo[NAME_STR] = controllerName.text;
    userinfo[USERNAME_STR] = controllerEmail.text;
    userinfo[PASS_STR] = controllerPass.text;

    try {
      String response = await network.genericNetworkRequestBeforeLogin(
          userinfo, LOGIN_USER_NEW);

      List<String> resArr = response.split('#');

      if (resArr[0].toUpperCase() == "true") {
        final SharedPreferences prefs = await SharedPreferences.getInstance();

        setState(() {
          showLoader = false;
        });

        var snackBar = SnackBar(content: Text(resArr[1]),
          duration: Duration(minutes: 1),
          action: SnackBarAction(
            textColor: Colors.white,
            label: 'Login',
            onPressed: () {
              // Some code to undo the change.
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginEmail()));
            },
          ),);

        Scaffold.of(context).showSnackBar(snackBar);
      } else {
        final snackBar = SnackBar(content: Text(resArr[1]));
        Scaffold.of(context).showSnackBar(snackBar);

        setState(() {
          showLoader = false;
        });
      }
    } on TimeoutException catch (_) {
      final snackBar = SnackBar(
          content: Text("Failed to reach the server; Please try again"));
      Scaffold.of(context).showSnackBar(snackBar);

      setState(() {
        showLoader = false;
      });
    } on SocketException catch (_) {
      final snackBar = SnackBar(
          content: Text("Could not connect to the server; Please try again"));
      Scaffold.of(context).showSnackBar(snackBar);

      setState(() {
        showLoader = false;
      });
    } on Exception catch (_) {
      final snackBar =
      SnackBar(content: Text("Some error occurred; Please try again"));
      Scaffold.of(context).showSnackBar(snackBar);

      setState(() {
        showLoader = false;
      });
    }

  tosChange(bool value) {
      setState(() {
        agreeTOS = value;
      });
    }
  }
}




