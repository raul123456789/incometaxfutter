
import 'dart:collection';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/Inbox/Inbox.dart';
import 'package:listview/Inbox/MessageList.dart';
import 'package:listview/Leaderboard/Leaderboard.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/feed.dart';
import 'package:listview/followers/followers.dart';
import 'package:listview/model/Conversation.dart';
import 'package:listview/settings/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../User.dart';
import 'package:listview/model/Score.dart';
import 'package:listview/Leaderboard/LeaderboardScore.dart';
import 'package:listview/network/network.dart' as network;

class Profile extends StatefulWidget {

  String _memberIdReceived;

  Profile(this._memberIdReceived);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfilePageState(_memberIdReceived);
  }
}

class ProfilePageState extends State<Profile> with TickerProviderStateMixin {

  Future<User> userFuture;
  Future<List> scores;
  User user;
  int index=0;
  String _memberId;
  String memberName = "";
  bool isNotOwnProfile = true;
  bool showFollowLoader = false;
  bool loadImage = true;
  String urlProfilePic;
  File file;

  ProfilePageState(this._memberId);

  CachedNetworkImage _image;
  //String name="";

  bool _loadingDP=true;
  bool _dpUpdatedAlready = false;


  var items = ["Inbox","Settings","Feed"];
  var itemIcons = [Icons.message,Icons.thumb_up,Icons.event_note];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userFuture  = getUserInfo();
    scores=getScores(true);
    //checkIfNotOwnProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
          backgroundColor: Hexcolor('#008B8B')
      ),
      body: Container(
        child: FutureBuilder<User>(
            future: userFuture,
            builder: (context,snapshot) {
              if (snapshot.hasData) {
                return createProfileUI(snapshot.data);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              // By default, show a loading spinner.
                  return Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator()
                  );
            }
            ),
      ),
      bottomNavigationBar: createBottomBar(),
    );
  }

  Widget createProfileUI(User user) {
    bool isFolowing = (user.isBeingFollowedByMe == 1) ? true : false;
    return SingleChildScrollView(
      child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              /*Top row : Profile Pic, User Name, Follower Following*/
              /*Padding(
                  padding: EdgeInsets.only(top: 0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Leaderboard()));
                    },
                    child: Container(
                        height: 30.0,
                        width: 650.0,
                        decoration: new BoxDecoration(
                            color: Colors.blue,
                            shape: BoxShape.rectangle,
                            border: new Border.all(
                              color: Colors.blue,
                              width: 2.0,
                            )
                        ),
                        child: Center(
                          child:
                          Text("Weekly Leaderboard Points :",
                            style: TextStyle(fontSize: 11,color: Colors.white,fontWeight: FontWeight.bold)
                            ,textAlign: TextAlign.center,
                          ),
                        )
                    ),
                  )
              ),*/
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(left: 98,top: 15),
                          child: Stack(
                            children: <Widget>[
                              Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(top:30),
                                    child: ClipOval(
                                      child:Container(
                                        height: 150,
                                        width: 150,
                                        child: (_image != null) ? _image : Container(width: 150,height: 150, color: Colors.black12,
                                          child: Text(getFirstLetter(user.name.trim()),style: TextStyle(fontSize: 20,color: Colors.red,fontWeight: FontWeight.bold),),
                                        ),),
                                    ),
                                  ),

                                  (_loadingDP)
                                      ? Align(
                                  )
                                      : Container(),
                                ],
                              ),
                              if (!isNotOwnProfile)
                                Positioned(
                                    bottom: 0,
                                    child: GestureDetector(
                                        onTap: () => _changeDPAlert(),
                                        child: ClipOval(
                                            child: Container(
                                                color: Colors.black,
                                                child: Padding(
                                                  padding: EdgeInsets.all(5),
                                                  child: Icon(Icons.camera_alt,
                                                      size: 26.0,
                                                      color: Colors.white),
                                                )))))
                            ],
                          )),
                      Padding(
                          padding: EdgeInsets.only(top: 80,left: 110),
                          child: Text(
                               user.name,
                            style: TextStyle(
                                fontSize: 18,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold
                            ),
                          )
                      )
                    ],
                  ),
                  /*Expanded(child: Container(child:),)*/
                ],
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 0)));
                      },
                      child: Container(
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 70,top: 50),
                              ),
                              Text("Followers :",
                                style: TextStyle(color: Colors.grey),),
                              Text(
                                (user.followCount == "")? "0" : user.followCount,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold),
                              ),

                            ],
                          )
                      )
                  ),
                  /*Padding(
                    padding: EdgeInsets.only(left: 1),
                  ),*/
                  Padding(
                    padding: EdgeInsets.only(left: 6),
                    child: Text(" | "),
                  ),

                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 1)));
                      },
                      child: Container(
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 15,top: 70),
                              ),
                              Text("Followings :",style: TextStyle(color: Colors.grey)),
                              Text(
                                (user.followingCount == "")
                                    ? "0"
                                    :    user.followingCount,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          )
                      )
                  ),

                  /*GestureDetector(
                      onTap: () {
                       *//* Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 1)));*//*
                      },
                      child: Container(
                          child: new Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(left: 15,top: 100),
                              ),
                              Text("Points :",
                                  style: TextStyle(color: Colors.grey)),
                              Text(
                                (user.totalpoints == "")
                                    ? "0"
                                    :    user.totalpoints,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 15,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          ),
                      )
                  ),*/
                  Padding(
                    padding: EdgeInsets.all(5),
                  ),
                  isNotOwnProfile
                      ? Stack(
                    children: <Widget>[
                      (isFolowing
                          ? Align(
                          alignment: Alignment.center,
                          child: GestureDetector(
                              onTap: () => unFollowUser(),
                              child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(30)),
                                    color: Colors.black,
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: 10,
                                        bottom: 10,
                                        left: 20,
                                        right: 20),
                                    child: Text(
                                      "Unfollow",
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  ))))
                          : Align(

                      )),
                      showFollowLoader
                          ? Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                            child: CircularProgressIndicator(
                                valueColor:
                                new AlwaysStoppedAnimation<
                                    Color>(Colors.blue)),
                            width: 30.0,
                            height: 30.0,
                          ))
                          : Container()
                    ],
                  )
                      : Container(),
                ],
              ),

              Padding(
                padding: EdgeInsets.only(top: 148),
                child: Container(
                  color: Colors.black12,
                  height: 48,
                  width: 700,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: <Widget>[
                      GestureDetector(
                          onTap: () {
                            /* Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 0)));*/
                          },
                          child: Container(

                              child: Row(

                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 22,top: 350),
                                  ),
                                  Text("Points : ",
                                    style: TextStyle(color: Colors.black,fontSize: 11),),
                                  Text(
                                    (user.totalpoints == null)? "" : user.totalpoints,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 11,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                              )
                          )
                      ),
                      /*Padding(
                    padding: EdgeInsets.only(left: 1),
                  ),*/


                      GestureDetector(
                          onTap: () {
                            /*Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 1)));*/
                          },
                          child: Container(
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 51,top: 300),
                                  ),
                                  Text("Likes :",style: TextStyle(color: Colors.black,fontSize: 11)),
                                  Text(
                                    (user.thankscount == null)? "" : user.thankscount,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 11,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                              ))
                      ),

                      GestureDetector(
                          onTap: () {
                            /*Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    Followers(user.memberId, 1)));*/
                          },
                          child: Container(
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(left: 52,top: 300),
                                  ),
                                  Text("Visits : ",style: TextStyle(color: Colors.black,fontSize: 11)),
                                  Text(
                                    (user.viewsCount == null)? "" : user.viewsCount,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 11,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                              ))
                      ),
                    ],
                  ),

                ),

              ),

              Padding(
                padding: EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    /*About user*/
                    /*Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "ABOUT",
                          style: TextStyle(
                            fontSize: 15,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        Text(user.bio)
                      ],
                    ),*/
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),

                  ],
                ),
              ),
            ],
          )),);
  }


  Widget createBottomBar() {

    return BottomNavigationBar(

        currentIndex: 0,
        onTap: (int index){
          setState(() {
            this.index=index;
          });
          _navigateToScreens(index, items);
        },
        items: createButtons(items,itemIcons)
    );
  }
  Future<List> getScores(bool prev) async {

    HashMap<String,String> map = HashMap();
    map[KEY_MODULE_NAME]=VALUE_MODULE_LEADERBOARD;
    map[FRIEND_ID_STR]=MEMBER_ID_STR;

    if(prev)
      map[KEY_PREV]="1";

    String response = await network.genericNetworkRequest(map, GET_LEADERBOARD);

    List scores=await network.parseScore1(response);

    return scores;

  }


  createButtons(List<String> items, List<IconData> itemIcons) {

    List<BottomNavigationBarItem> buttons = [];

    var count=0;
    items.forEach((name) {
      buttons.add(BottomNavigationBarItem (
        icon: new Icon(itemIcons[count],color: Colors.black,),
        title: new Text(name,style: TextStyle(color: Colors.black),),
      ));
      count++;
    });

    return buttons;

  }


  void _navigateToScreens(int index,List<String> items) {

    switch(items[index]) {

      case "Inbox":

        if(isNotOwnProfile){

          Navigator.push(context, MaterialPageRoute(builder: (context)=> Inbox()));

        }else{

          Conversation conversation = new Conversation();
          conversation.SenderId = _memberId;
          conversation.SenderName = memberName;
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MessageList(conversation)));

        }
        break;

      case "Settings":

        if(isNotOwnProfile) {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => Settings()));
        }

        break;

      case "Feed":

      Navigator.push(context,
          MaterialPageRoute(builder: (context) => Feed()));
        break;

    }

  }

  Future<User> getUserInfo() async {

    SharedPreferences pref = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap<String, String>();
    params[MEMBER_ID_STR] = pref.getString(MEMBER_ID_STR);
    params[FRIEND_ID_STR] = _memberId;

    //make api request
    String response = await network.genericNetworkRequest(params, USER_PROFILE_API);

    //parse user information
    User user = network.parseUserInfo(response);

    isNotOwnProfile = "2"!= user.memberId;
    memberName=user.name;

    setState(() {
      if(!isNotOwnProfile && user!= null) {
        items=["Inbox","Settings","Feed"];
        itemIcons=[Icons.inbox,Icons.settings,Icons.message];
      }
    });

    if(!_dpUpdatedAlready) {
      setState(() {
        this.user=user;
      });

      _image = CachedNetworkImage(
        width: 150,
        height: 150,
        imageUrl: user.avatarUrl+ "?d=" + DateTime.now().millisecondsSinceEpoch.toString(),
        placeholder: (context, url) => new CircularProgressIndicator(),
        errorWidget: (context, url, error) => Container(width: 150,height: 150, color: Colors.black12,
          child: Center( child:Text(getFirstLetter(user.name.trim()),style: TextStyle(fontSize: 35,color: Colors.black,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)),
        ),
      );

    }
    return user;
  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }

  getFirstLetter(String name) {
    return name.substring(0,1);
  }

  _changeDPAlert() {}

  Future<String> unFollowUser() async {

    setState(() {
      showFollowLoader = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap();
    params[MEMBER_ID_STR] = "2";
    params[FRIEND_ID_STR] = _memberId;

    /*make the api req*/
    String response =
    await network.genericNetworkRequest(params, UNFOLLOW_USER);

    setState(() {
      showFollowLoader = false;
      user.isBeingFollowedByMe = 0;
      user.followCount = (int.parse(user.followCount) - 1).toString();
    });

  }

  Future<String> followUser() async {

    setState(() {
      showFollowLoader = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();

    HashMap<String, String> params = new HashMap();
    params[MEMBER_ID_STR] = "2";
    params[FRIEND_ID_STR] = _memberId;

    // make the api req
    String response = await network.genericNetworkRequest(params, FOLLOW);

    setState(() {
      showFollowLoader = false;
      user.isBeingFollowedByMe = 1;
      user.followCount = (int.parse(user.followCount) + 1).toString();
    });















  }

}

