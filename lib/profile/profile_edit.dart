import 'dart:collection';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/User.dart';
import 'package:listview/constants/constants_dropdown.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/constants/constants.dart';
import 'package:listview/profile/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileEdit extends StatelessWidget {


  User user;


  ProfileEdit(this.user);

  @override
  Widget build(BuildContext context) {
    final appTitle = 'Edit Profile';

    return Scaffold(
      body: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
            backgroundColor: Hexcolor('#008B8B')
        ),
        body: ProfileUpdateForm(user),
      ),
    );
  }
}

// Create a Form widget.
class ProfileUpdateForm extends StatefulWidget {

  User user;


  ProfileUpdateForm(this.user);

  @override
  ProfileUpdateFormState createState() {
    return ProfileUpdateFormState(user);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class ProfileUpdateFormState extends State<ProfileUpdateForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.


  User user;

  ProfileUpdateFormState(this.user);

  final _formKey = GlobalKey<FormState>();

//  var prefs = SharedPreferences.getInstance();

  SharedPreferences prefs;

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  // your list of DropDownMenuItem
  List<DropdownMenuItem<String>> menuItems = List();

  // loop in the map and getting all the keys

  final KEY_OTHER_COUNTRY = "7";
  final KEY_OTHER_QUALI = "16";

  String dropdownValueCountry = '1';
  String dropdownValueQuali = '1';
  String mobileNumber = "";
  String name = "";
  String otherCountry = "";
  String otherQualification = "";
  String fcmToken = "";
  String website = "";
  String linkedin = "";
  String twitter = "";
  String youtube = "";
  String memberId = "";
  String about="";
  String occupation="";

  bool showSpecifyCountry = false;
  bool showSpecifyQualification = false;
  bool _showLoaderSubmit = false;

  var controllerMobile = new TextEditingController();
  var controllerName = new TextEditingController();
  var controllerAbout = new TextEditingController();
  var controllerCity = new TextEditingController();
  var controllerOtherCountry = new TextEditingController();
  var controllerOtherQuali = new TextEditingController();

  Future<Null> getSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
//    mobileNumber = prefs.getString(MOBILE);
    memberId = prefs.getString(MEMBER_ID_STR);
    fcmToken=prefs.getString(GCM_REG_ID_STR);


  }

  @override
  void initState() {
    super.initState();

    getSharedPrefs();

//    configureFCM();

    /*this.name=user.name;
    this.dropdownValueCountry = (user.countryId == null) ? "1" : user.countryId;
    this.dropdownValueQuali = (user.qualiId == null) ? "1" : user.qualiId;
//    this.mobileNumber = user.mo;
    this.otherCountry = user.otherCountry;
    this.otherQualification = user.otherQualification;
    this.about=user.bio;
    *//*this.website=user.website;
    this.linkedin=user.linkedIn;
    this.twitter=user.twitter;
    this.youtube=user.youtube;*//*
    this.occupation=user.occupation;
    this.mobileNumber=user.mobile;

    controllerOtherCountry.text = otherCountry;
    controllerOtherQuali.text = otherQualification;

    controllerName.text=name;
    controllerAbout.text=about;
    controllerWebsite.text=website;
    controllerLinkedin.text=linkedin;
    controllerTwitter.text=twitter;
    controllerYoutube.text=youtube;
    controllerOccupation.text=occupation;
    controllerMobile.text=mobileNumber;*/

  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(

        child: Padding(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(padding: EdgeInsets.only(top: 10),),
                Container(
                  decoration: new BoxDecoration(
                    border: Border.all(width: 0.2,color: Colors.grey),borderRadius:BorderRadius.all(Radius.circular(10)), ),
                  width:  double.infinity,
                  height: 65,
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: dropdownValueQuali,
                        items: qualification.entries
                            .map<DropdownMenuItem<String>>(
                                (MapEntry<String, String> e) => DropdownMenuItem<String>(
                              value: e.key,
                              child: Text(e.value),
                            ))
                            .toList(),
                        onChanged: (String newKey) {
                          setState(() {
                            if (newKey == KEY_OTHER_QUALI)
                              showSpecifyQualification = true;
                            else
                              showSpecifyQualification = false;

                            dropdownValueQuali = newKey;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                Visibility(
                  child: Padding(padding: EdgeInsets.only(top: 10),),
                  visible: showSpecifyQualification,
                ),
                Visibility(
                  child: TextFormField(
                    decoration:
                    new InputDecoration(hintText: 'Specify qualification',
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(color: Colors.grey, width: 0.2),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        borderSide: BorderSide(color: Colors.grey, width: 0.2,),
                      ),
                    ),
                    controller: controllerOtherQuali,
                  ),
                  visible: showSpecifyQualification,

                ),
                Padding(padding: EdgeInsets.only(top: 10),),
                Container(
                  decoration: new BoxDecoration(
                    border: Border.all(width: 0.2,color: Colors.grey),borderRadius:BorderRadius.all(Radius.circular(10)), ),
                  width:  double.infinity,
                  height: 65,
                  child: DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        value: dropdownValueCountry,
                        items:  country.entries
                            .map<DropdownMenuItem<String>>(
                                (MapEntry<String, String> e) => DropdownMenuItem<String>(
                              value: e.key,
                              child: Text(e.value),
                            ))
                            .toList(),
                        onChanged: (String newKey) {
                          setState(() {
                            if (newKey == KEY_OTHER_COUNTRY)
                              showSpecifyCountry = true;
                            else
                              showSpecifyCountry = false;

                            dropdownValueCountry = newKey;
                          });
                        },
                      ),
                    ),
                  ),
                ),

                Visibility(
                  child: Padding(padding: EdgeInsets.only(top: 10),),
                  visible: showSpecifyCountry,
                ),
                Visibility(
                  visible: showSpecifyCountry,
                  child: TextFormField(
                    decoration: new InputDecoration(hintText: 'Specify country'),
                    controller: controllerOtherCountry,
                  ),
                ),
                Padding(padding: EdgeInsets.only(top: 10),),
                TextFormField(
                  decoration: new InputDecoration(hintText: 'Mobile number',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2,),
                    ),
                  ),
                  controller: controllerMobile,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Enter some text';
                    }
                    return null;
                  },
                ),
                Padding(padding: EdgeInsets.only(top: 10),),
                TextFormField(
                  decoration: new InputDecoration(hintText: 'City',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(color: Colors.grey, width: 0.2,),
                    ),
                  ),
                  controller: controllerCity,

                ),

                Padding(padding: EdgeInsets.only(top: 10),),
                (!_showLoaderSubmit)?
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Align(
                        alignment: Alignment.center,
                        child: GestureDetector(
                            onTap: () async {
                              if (_formKey.currentState.validate()) {

                                // If the form is valid, display a Snackbar.
                                submitForm();

                              }
                            },
                            child: Container(
                                width: 150,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(30)),
                                  color: Colors.grey[350],
                                ),
                                child: /*(!_showLoaderSubmit)?*/
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: 10,
                                      bottom: 10,
                                      left: 20,
                                      right: 20),
                                  child:
                                  Center (
                                      child: Text(
                                        "Continue",
                                        style: TextStyle(
                                            color: Colors.black),
                                      ),
                                  )
                                ))))
                ):Center(
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          width: 30,
                          height: 30,
                          child: CircularProgressIndicator(),
                        ))),

              ],
            ),
          ),
        ));
  }

  void configureFCM() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    _firebaseMessaging.getToken().then((token) {
      //print("========================="+token);

      fcmToken = token;
    });
  }

  Future submitForm() async {

    setState(() {
      _showLoaderSubmit=true;
    });

    HashMap<String, String> userInfo =
    new HashMap<String, String>();
    userInfo[MOBILE] = controllerMobile.text;
    userInfo[OTHER_COUNTRY] = controllerOtherCountry.text;
    userInfo[OTHER_QUALIFICATION] = controllerOtherQuali.text;
    userInfo[COUNTRY_ID] = dropdownValueCountry;
    userInfo[QUALIFICATION_ID] = dropdownValueQuali;
    userInfo[FCM_TOKEN] = fcmToken;
    userInfo[MEMBER_ID_STR] = memberId;
    userInfo["about_me"]=controllerAbout.text;
   /* userInfo["linkedin_profile"]=controllerLinkedin.text;
    userInfo["twitter_profile"]=controllerTwitter.text;
    userInfo["youtube_profile"]=controllerYoutube.text;*/
    userInfo["website"]=controllerCity.text;

    userInfo.forEach((k,v)=>{
      print("++++++++++++++++++$k    =   $v")
    });
    String response = await network.uploadProfileInfo(userInfo);

    var resArr = response.split("#");


    setState(() {
      _showLoaderSubmit=false;
    });

    if (resArr[0].toLowerCase() == "true") {

      //  prefs.setString(GCM_REG_ID_STR, resArr[2]);

      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(resArr[1])));

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Profile(memberId)));

    } else {

      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(resArr[1])));
    }
  }

}