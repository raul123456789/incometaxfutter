final city = {
  "1": "Mumbai",
  "2": "Delhi",
  "3": "Bangalore",
  "4": "Hyderabad",
  "5": "Chennai",
  "6": "Kolkata",
  "7": "Pune",
  "8": "Ahmedabad",
  "9": "Jaipur",
  "10": "Thane",
  "11": "Gurgaon",
  "12": "Indore",
  "13": "Surat",
  "14": "Ghaziabad",
  "15": "Chandigarh",
  "16": "Dubai",
  "17": "Coimbatore",
  "18": "Faridabad",
  "19": "Lucknow",
  "20": "Vadodara",
  "21": "Nagpur",
  "22": "Noida",
  "23": "Ludhiana",
  "24": "Vijayawada",
  "25": "Bhubaneshwar",
  "26": "Dispur",
  "27": "Guwahati",
  "28": "Jalandhar",
  "29": "Jamshedpur",
  "30": "Jorhat",
  "31": "Others"
};

final qualification = {
  "1": "CA",
  "2": "ACCA",
  "3": "CPA",
  "4": "CIMA",
  "5": "IMA",
  "6": "CA Pakistan",
  "7": "CA Sri Lanka",
  "8": "Accountant",
  "9": "CS",
  "10": "CMA",
  "11": "Finance Professional",
  "12": "Tax practitioner",
  "13": "Lawyer",
  "14": "Student",
  "15": "Entrepreneur",
  "16": "Others"
};

final country = {
  "1": "Baharain",
  "2": "KSA",
  "3": "Oman",
  "4": "Qatar",
  "13":"Kuwait",
  "5": "UAE ",
  "6": "GCC",
  "8": "India",
  "9": "Pakistan",
  "10": "Nepal",
  "11": "Sri Lanka",
  "12": "USA",
  "14": "UK",
  "15": "Europe",
  "7": "Others"
};

final category={
  "-1":"Select Category",
  "1":"VAT",
  "2":"Customs",
  "3":"Excise",
  "4":"Career",
  "5":"Others"
};

final function={
  "1":"VAT",
  "2":"Accounting",
  "3":"Customs",
  "4":"Excise",
  "5":"Audit",
  "6":"Consulting",
  "7":"Others",
  "8":"Internship"
};
