import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:listview/constants/const_api.dart';

class Web extends StatelessWidget {

  String url = "";
  String title="";
  double progress = 0;


  Web(this.url,this.title);

  WebViewController _webViewController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
            backgroundColor: Hexcolor('#008B8B')
        ),
        body: WebView(
            initialUrl: url,
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController controller)=> {
              controller.loadUrl(url),
            },
            onPageFinished: (url) {

              //DynamicTheme.of(context).setBrightness(Theme.of(context).brightness == Brightness.dark? Brightness.light: Brightness.dark);
              if(Theme.of(context).brightness == Brightness.dark) {
                      _webViewController.evaluateJavascript(
                          "document.body.style.setProperty(\"color\", \"#E0E0E0\")");
                      _webViewController.evaluateJavascript(
                          "document.body.style.setProperty(\"background-color\", \"#424242\")");
                      _webViewController.evaluateJavascript(
                          "document.getElementsByTagName(\"h1\")[0].style.setProperty(\"color\", \"#E0E0E0\");");
              }
            },
            navigationDelegate: (NavigationRequest request) {
              print("----------"+request.url);
              if(request.url.contains("www.caclubindia.com/profile.asp"))
              {

                var userId;
                var uri = Uri.parse(request.url);
                uri.queryParameters.forEach((k, v) {

                  print('key: $k - value: $v');
                  if(k=="member_id") {
                    userId=v;
                  }

                });

                return NavigationDecision.prevent;
              }

              return NavigationDecision.navigate;
            }
        )
    );
  }
}