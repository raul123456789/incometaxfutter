var moduleInfo={
  "1":{"id":"1","name":"forum","name_f":"Q&A","folder":"discussion"},

  "2":{"id":"2","name":"articles","name_f":"Articles","folder":"articles"},

  "3":{"id":"3","name":"news","name_f":"News","folder":"news"},

  "4":{"id":"4","name":"share_files","name_f":"Resources","folder":"resources"},

  "5":{"id":"5","name":"clarification","name_f":"Public Clarifications","folder":"clarifications"},

  "6":{"id":"6","name":"events","name_f":"Events","folder":"events"},

  "7":{"id":"7","name":"decisions","name_f":"Cabinet/FTA Decisions","folder":"decisions"},

  "8":{"id":"8","name":"forms","name_f":"Forms/Letters","folder":"forms"},

  "9":{"id":"9","name":"guides","name_f":"Guides","folder":"guides"},

  "10":{"id":"10","name":"government_resources","name_f":"Government Resources","folder":"govt_resources"},

  "11":{"id":"11","name":"acts","name_f":"Acts","folder":"law"},

  "12":{"id":"12","name":"editorial","name_f":"Editorial","folder":"editorial"},

  "13":{"id":"13","name":"bookmarkonly","name_f":"Bookmarks","folder":"bookmark"},
};