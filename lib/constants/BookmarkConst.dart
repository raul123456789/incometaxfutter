class Bookmarkconst {

  static const String Subscribe = 'Subscribe';
  static const String Settings = 'Settings';
  static const String Stop = 'Stop';

  static const List<String> choice = <String> [
    Subscribe,
    Settings,
    Stop
  ];

}