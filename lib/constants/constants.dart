const String APP_NAME="IT Act No.2 and Ordinance";
const String APP_NAME1="All Subjects";
const String IS_LOGGED_IN="is_logged_in";
const String EMAIL_STR="email";
const String PASS_STR="password";
const String USERNAME_STR="username";
const String NAME_STR="name";
const String MEMBER_ID_STR="member_id";
const String FRIEND_ID_STR="friend_id";
const String AVATAR_URL="avatar_url";
const String IS_PROFILE_INCOMPLETE="is_profile_complete";
const String CITY_ID="city_id";
const String COUNTRY_ID="country";
const String CAT_ID="cat_id";
const String QUERY_TITLE="title";
const String UPDATE_VIA="update_via";
const String QUERY_BODY="body";
const String KEY_API_ITEM_ID="item_id";
const String KEY_MODULE_ID="module_id";
const String VALUE_MODULE_LEADERBOARD = "leaderboard";
const String VALUE_MODULE_SHARE_FILES="share_files";
const String OTHER_COUNTRY="other_country";
const String OTHER_QUALIFICATION="other_qualification";
const String OTHER_CITY="other_city";

const String QUALIFICATION_ID="qualification_id";
const String MOBILE="mobile_no";
const String DOB="dob";
const String FCM_TOKEN="token";
const String AFTER_LOGIN="after_login";
const String GCM_REG_ID_STR="reg_id";
const String KEY_MODULE_NAME="module_name";
const String KEY_API_MSG_BODY="msg_body";
const String VALUE_MODULE_EVENTS = "events";
const String KEY_PREV="previous";


const String Bookmark = "Bookmark";
const String KEY_API_MOD_ITEM_ID_2="moditemid";
const String KEY_API_TITLE="title";

const String KEY_API_FOLLOW_DATA_TYPE = "showtype";

//pmsg
const String KEY_API_PM_SENDER="pm_member";
const String KEY_API_PM_ID="pm_id";
const String KEY_API_PM_TO="pm_to";
const String KEY_API_PM_FROM="pm_from";
const String KEY_API_PM_BODY="pm_body";


