const String MODULE_FEED_MASTER="https://www.caclubindia.com/api/flutter/master_feed.asp";
const String LOGIN_SOCIAL_NEW="https://www.caclubindia.com/api/flutter/login_social_dual.asp";
const String UPDATE_SOCIAL_PROFILE_NEW="https://www.caclubindia.com/api/flutter/social_profile_update_dual.asp";
const String CCI_BASE_URL_HTTP="https://www.caclubindia.com/";

const String CCI_BASE_URL_IMAGES="https://www.caclubindia.com/img/preview/";

const String CCI_BASE_URL_PROFILE_IMAGES="https://www.caclubindia.com/img/avatars/";

const String FORGOT_PASSWORD="https://www.caclubindia.com/user_forgotpassword.asp";

const String LOGIN_USER_NEW="https://www.caclubindia.com/api/flutter/login_native_dual.asp";

const String POST_COMMENT_COMMON="https://www.caclubindia.com/api/flutter/comment_common.asp";

const String CONTACT_US="https://www.caclubindia.com/api/flutter/android_feedback.asp";
const String POST_COMMENT_FORUM="https://www.caclubindia.com/api/flutter/forum_message_reply.asp";

const String CCI_API_REDIRECT_POST="https://www.caclubindia.com/api/flutter123/redirectget.asp";

const String CREATE_BOOKMARK = "https://www.caclubindia.com/api/flutter/bookmark_this_api.asp";
const String PERSONAL_MESSAGE_OF_A_USER="https://www.caclubindia.com/api/flutter/pm_sender_list.asp";
const String PERSONAL_MESSAGES_FROM_A_SPECIFIC_USER="https://www.caclubindia.com/api/flutter/pm_msg_list.asp";
const String SEND_PERSONAL_MESSAGES="https://www.caclubindia.com/api/flutter/pm_send.asp";
const String FOLLOW_DATA = "https://www.caclubindia.com/api/flutter/follow_data.asp";
const String UNFOLLOW_USER ="https://www.caclubindia.com/api/flutter/follow_unfollow.asp" ;
const String FOLLOW ="https://www.caclubindia.com/api/flutter/follow_now.asp";
const String PRIVACY_POLICY="https://www.caclubindia.com/privacy_policy.asp";
const String TERMS_OF_USE="https://www.caclubindia.com/terms_of_use.asp";
const String USER_PROFILE_API="https://www.caclubindia.com/api/flutter/user_details.asp";

const String GET_LEADERBOARD="https://www.caclubindia.com/api/flutter/get_leaderboard.asp";

const String GET_LEADERBOARD_POINTS="https://www.caclubindia.com/api/common/get_lb_points.asp";

const String POST_IN_FORUM="https://www.caclubindia.com/api/flutter/forum_post.asp";
const String USER_PROFILE="https://www.caclubindia.com/profile.asp";
const String MODULE_BOOKMARKS="https://www.caclubindia.com/api/flutter/bookmark_get_data.asp";
const String GET_COMMENTS="https://www.caclubindia.com/api/flutter/get_comment.asp";
const String OPEN_SOURCE_CREDITS="https://www.caclubindia.com/api/flutter/open_source.asp";
