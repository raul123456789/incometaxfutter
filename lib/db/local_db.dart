import 'package:flutter/material.dart';
import 'package:listview/Chapters/Chapter.dart';
import 'package:listview/db/personal_db.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:listview/Section.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:listview/Utility.dart';


final String DB_NAME = "act";
final String TABLE_EXTRA = "personal";
final String KEY_ID = "id";
final String KEY_SECTION_TITLE = "section_title";
final String KEY_CHAPTER_TITLE = "chapter_title";
final String KEY_BOOKMARK = "bookmark";
final String KEY_IN_SYNC = "sync";
final String KEY_TIMESTAMP = "time";
final String KEY_SECTION_ID = "section_id";
final String KEY_ACT_ID = "act_id";
final String KEY_CHAPTER = "chapter";
final String KEY_CHAPTER_ID="chapter_id";
final String KEY_SECTION_NOTE = "note";


Future<List<Section>> getSectionsBookmarked(actId) async {
  // Get a reference to the database.
  final Database db = await getDB();

  /*Fluttertoast.showToast(
      msg: actId,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 3,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 14.0
  );*/

  // Query the table for all The Dogs.

  final List<Map<String, dynamic>> maps = await db.query(DB_NAME,where: 'SECTION_CODE = ?',whereArgs: [actId]);

  // Convert the List<Map<String, dynamic> into a List<Dog>.
  return List.generate(maps.length, (i) {
    return Section(maps[i]['ID'].toString(), maps[i]['TITLE'],  maps[i]['SUMMERY'], maps[i]['DESCRIPTION']);
  });
}


Future<List<Section>> getSections(String actId) async {
  // Get a reference to the database.

  var actId = int.parse(getActCode());

    final Database db = await getDB();
    /*Fluttertoast.showToast(
                msg: actId,
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 3,
                textColor: Colors.white,
                fontSize: 14.0
            );*/
    final List<Map<String, dynamic>> maps = await db.query(
        'act', where: 'SECTION_CODE = ?', whereArgs: [actId]);

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return Section(
          maps[i]['ID'].toString(), maps[i]['TITLE'], maps[i]['SUMMERY'],
          maps[i]['DESCRIPTION']);
    });
    // Query the table for all The Dogs.


}

Future<List<Chapter>> getChapters(String actId) async {

  var actId = int.parse(getActCode());

  if(actId !=null) {

    final Database db = await getDB1();

    final List<Map<String, dynamic>> maps = await db.query(
        'act', where: 'SECTION_CODE = ?', whereArgs: [actId]);

    return List.generate(maps.length, (i) {
      return Chapter(
          maps[i]['ID'].toString(), maps[i]['TITLE'], maps[i]['SUMMERY']);
    });

  }

}


Future<Database> getDB() async {

  //db in path
  var databasePath = await getDatabasesPath();
  var path = join(databasePath,"act");

  //if db exist
  var exist = await databaseExists(path);

  if(!exist){

    print("Creating New Copy from asset");
    // Make sure the parent directory exists
    try{
        await Directory(dirname(path)).create(recursive: true);
    }catch(_){
          print("Error");
    }
    //Copy from asset
    ByteData data = await rootBundle.load(join("assets","act.db"));
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes,data.lengthInBytes);

    //flush the bytes
    await File(path).writeAsBytes(bytes,flush: true);

  }
  else{
    print("Opening Existing Databse");
  }

  var db =await openDatabase(path, readOnly: false);

  var sql = "CREATE TABLE "+TABLE_EXTRA+
      "("
            +KEY_ID+"INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            +KEY_SECTION_ID+ "TEXT "+
            KEY_SECTION_TITLE+" TEXT"
            +KEY_BOOKMARK+"INTEGER,"
            +KEY_SECTION_NOTE+" TEXT"+""
            + KEY_TIMESTAMP + " TEXT,"
            + KEY_ACT_ID + " TEXT,"
            + KEY_IN_SYNC + " TEXT,"
            + KEY_CHAPTER + " TEXT" +
      ")";

  db.execute(sql);
  return db;
}

Future<Database> getDB1() async {

  var databasePath = await getDatabasesPath();
  var path = join(databasePath,"act");

  var exist = await databaseExists(path);

  if(!exist){
    print("Creating New Copy from asset");
    // Make sure the parent directory exists
    try{

      await Directory(dirname(path)).create(recursive: true);

    }catch(_){
      print("Error");
    }
    //Copy from asset
    ByteData data = await rootBundle.load(join("assets","act.db"));
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes,data.lengthInBytes);

    //flush the bytes
    await File(path).writeAsBytes(bytes,flush: true);
  }
  else{
    print("Opening Existing Databse");
  }

  var db = await openDatabase(path,readOnly: false);

  var sql = "CREATE TABLE"+TABLE_EXTRA+
      "("
          +KEY_ID+"INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          +KEY_CHAPTER_ID+" TEXT "
          +KEY_CHAPTER_TITLE+" TEXT "
          +KEY_TIMESTAMP+" TEXT "
          +KEY_ACT_ID+" TEXT "
          +KEY_IN_SYNC+" TEXT "
      ")";
      /*var sql = "CREATE TABLE "+TABLE_EXTRA+
          "("
          +KEY_ID+"INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
          +KEY_SECTION_ID+
          "TEXT "
            +KEY_SECTION_TITLE+" TEXT"+KEY_BOOKMARK+"INTEGER,"+KEY_SECTION_NOTE+" TEXT"+"" + KEY_TIMESTAMP + " TEXT,"
          + KEY_ACT_ID + " TEXT,"
          + KEY_IN_SYNC + " TEXT,"
          + KEY_CHAPTER + " TEXT" +
      ")";*/
  db.execute(sql);

  return db;
}


Future<int> addBookmark(Section section) async {

  //reference to the database
  final Database db = await getDB();

  Map<String, dynamic> row = {

    KEY_SECTION_TITLE: section.title,
    KEY_BOOKMARK:1,
    KEY_ACT_ID: section.actId,
    KEY_IN_SYNC:0,
    KEY_SECTION_ID:section.id,
    KEY_CHAPTER:section.chapter,
    KEY_TIMESTAMP:new DateTime.now().millisecond

  };

  try{

    int id = await db.insert
      (TABLE_EXTRA,
      row,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    return id;

  }catch(e){
    print(e);
    return -1;
  }

}

Future<List<Section>> getBookmarkedSections() async {
  // Get a reference to the database.
  final Database db = await getDB();
/*
  List<Map> result = await db.rawQuery('SELECT * FROM my_table WHERE name=?', ['Mary']);
*/
  List<Map<String, dynamic>> maps = await db.rawQuery
    ('SELECT act.id,act.title,act.summery,act.description,act.section_code,act.chapter_no '
      'FROM act,personal WHERE act.id=personal.section_id and personal.bookmark=?', ['1']);
  // Query the table for all The Dogs.
//  final List<Map<String, dynamic>> maps = await db.query(TABLE_EXTRA);
  // Convert the List<Map<String, dynamic> into a List<Dog>.

  List<Section> sections= List.generate(maps.length, (i) {
    Section section=Section();
    section.id=maps[i]["ID"].toString();
    section.title=maps[i]["TITLE"];
    section.actId=maps[i]["SECTION_CODE"];
    section.summery=maps[i]["SUMMERY"];
    section.chapter=maps[i]["CHAPTER_NO"];
    section.details=maps[i]["DESCRIPTION"];
    return section;
  });
  return sections;
}

Future<Section> getASection(int id) async {

  final Database db = await getDB();
  int i=0;
  List<Map> maps = await db.rawQuery('SELECT * FROM act WHERE id=?', [id]);

  Section section=Section();
  section.id=maps[i]["ID"].toString();
  section.title=maps[i]["TITLE"];
  section.actId=maps[i]["SECTION_CODE"];
  section.summery=maps[i]["SUMMERY"];
  section.chapter=maps[i]["CHAPTER_NO"];
  section.details=maps[i]["DESCRIPTION"];

  return section;

}