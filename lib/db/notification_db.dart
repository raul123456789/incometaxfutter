import 'package:listview/model/ITNotification.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';


openDbNotifications() async {

  final database = openDatabase(

      join(await getDatabasesPath(), 'notifications_database.db'),

    onCreate :(db, version) {

        return db.execute(
            "CREATE TABLE notifications(id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "type TEXT, title TEXT,body TEXT,url TEXT,"
                "module_name TEXT,mod_item_id TEXT,img TEXT )"
        );
    },
    version: 1,
  );
  return database;
}

Future<void> insertNotification(ITNotification itNotification) async{

  //Database reference
  final Database db = await openDbNotifications();

  try{
    await db.insert("notification",
      itNotification.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,);

  }catch(e){
    print(e);
  }

}

