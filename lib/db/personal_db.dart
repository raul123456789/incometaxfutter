import 'package:listview/Section.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


final String DB_NAME = "act";
final String TABLE_EXTRA = "personal";
final String KEY_ID = "id";
final String KEY_SECTION_TITLE = "section_title";
final String KEY_SECTION_NOTE = "note";
final String KEY_BOOKMARK = "bookmark";
final String KEY_IN_SYNC = "sync";
final String KEY_TIMESTAMP = "time";
final String KEY_SECTION_ID = "section_id";
final String KEY_ACT_ID = "act_id";
final String KEY_CHAPTER = "chapter";


Future<int> addBookmark(Section section) async {

  // Get a reference to the database.
  final Database db = await openDb();

  Map<String,dynamic> row = {
    KEY_SECTION_TITLE: section.title,
    KEY_BOOKMARK: 1,
    KEY_ACT_ID:section.actId,
    KEY_IN_SYNC:0,
    KEY_SECTION_ID:section.id,
    KEY_CHAPTER:section.chapter,
    KEY_TIMESTAMP:new DateTime.now().millisecond
  };

  try {
    int id=await db.insert(
      TABLE_EXTRA,
      row,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    return id;
  } catch (e) {
    print(e);
    return -1;
  }

}

openDb() async {
  final database = openDatabase(
    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    join(await getDatabasesPath(), DB_NAME),
    // When the database is first created, create a table to store dogs.
    onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE "+TABLE_EXTRA+
            "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_SECTION_ID + " TEXT, "
            + KEY_SECTION_TITLE + " TEXT,"
            + KEY_BOOKMARK + " INTEGER,"
            + KEY_SECTION_NOTE + " TEXT,"
            + KEY_TIMESTAMP + " TEXT,"
            + KEY_ACT_ID + " TEXT,"
            + KEY_IN_SYNC + " TEXT,"
            + KEY_CHAPTER + " TEXT" +
            ")",
      );
    },
    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    version: 1,
    readOnly: false,
  );

  return database;
}



