import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:listview/constants/constants.dart';
import 'package:url_launcher/url_launcher.dart';


class OnlineLearning extends StatefulWidget {
  @override
  _OnlineLearningState createState() => _OnlineLearningState();
}

class _OnlineLearningState extends State<OnlineLearning> {

  final appBarTitle = "All Subjects";
  Widget appBarTitle1 = new Text(APP_NAME1);
  Icon actionIcon = new Icon(Icons.search);
  Icon callIcon = new Icon(Icons.phone);

  @override
  Widget build(BuildContext context) {

    Widget image_slider = Container(

        height: 195,
        width: 195,
        child: Carousel(
            boxFit: BoxFit.fill,
            images: [
                AssetImage('assets/one.jpg'),
                AssetImage('assets/course.jpg'),


            ],
            autoplay: false,
            indicatorBgPadding: 1.0,
            dotColor: Colors.white,
            dotSize: 3.0,
            dotBgColor: Colors.white,
        ),//carousel
    );//container

    return Scaffold(
      appBar: AppBar(

          title: appBarTitle1,
          backgroundColor: Colors.cyan,
          actions: <Widget>[

            new IconButton(

                icon: actionIcon,

                onPressed: (){

                  setState(() {

                    if(this.actionIcon.icon == Icons.search) {

                      this.actionIcon = new Icon(Icons.close);
                      this.appBarTitle1 = new TextField(
                        textInputAction: TextInputAction.search,
                        style: new TextStyle(
                          color: Colors.white,
                        ),
                        decoration: new InputDecoration(
                            prefixIcon: new Icon(Icons.search, color: Colors.white),
                            hintText: "Search Courses",
                            hintStyle: new TextStyle(color: Colors.white)),
                        onSubmitted: (text) => performSearch(text),
                      );
                    } else {
                      this.actionIcon = new Icon(Icons.search);
                      this.appBarTitle1 = new Text(APP_NAME);
                    }
                  });
                }
            ),
            new IconButton(
                color: Colors.white,
                icon: callIcon,
                onPressed: () {
                      launchCaller(8880320003);
                }
            )
          ],
      ),
      body: new Container(
          child: ListView(
            children: <Widget>[
              image_slider,
              Container(
                  color: Colors.white,
                  height: 110,
                  padding: EdgeInsets.only(top: 5,bottom: 5),
                  child: new Text(
                          '  CA',
                        style: new TextStyle(
                        fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.black),
                        ),
              ),//1st container
              Padding(
                padding: EdgeInsets.only(top: 5,bottom: 5),
              ),
              Container(
                color: Colors.white,
                height: 110,
                padding: EdgeInsets.only(top: 5,bottom: 5),
                child: new Text(
                  '  CS',
                  style: new TextStyle(
                      fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.black),
                ),
              ),///2nd container
              Padding(
                padding: EdgeInsets.only(top: 5,bottom: 5),
              ),
              Container(
                color: Colors.white,
                height: 110,
                padding: EdgeInsets.only(top: 5,bottom: 5),
                child: new Text(
                  '  CMA',
                  style: new TextStyle(
                      fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.black),
                ),
              ),///3rd container
              Padding(
                padding: EdgeInsets.only(top: 5,bottom: 5),
              ),
              Container(
                color: Colors.white,
                height: 110,
                padding: EdgeInsets.only(top: 5,bottom: 5),
                child: new Text(
                  '  Professional',
                  style: new TextStyle(
                      fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.black),
                ),


              ),///2nd container
            ],
          )//ListView
      )
    );
  }

  void launchCaller(int number) async {

    var url="tel:${number.toString()}";

    if(await canLaunch(url)){
      await launch(url);
    }else{
      throw 'Could not place call';
    }
  }

  performSearch(String text) {}
}
