import 'dart:io';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/Section.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:listview/db/personal_db.dart' as personalDB;
import 'package:listview/act/SectionDetails.dart';
import 'package:listview/db/local_db.dart';
import 'package:url_launcher/url_launcher.dart';

class ITAct extends StatefulWidget {

  Future<List<Section>> sections;

  ITAct(this.sections);

  @override
  _ITActState createState() => _ITActState(sections);
}

class _ITActState extends State<ITAct> {

  static final String TITLE = " ";
  Future<List<Section>> sections;

  List<Section> sectionsFiltered;

  _ITActState(this.sections);

  Section thisSection;

  int index;

  Widget appBarTitle = new Text(TITLE);
  Icon actionIcon = new Icon(Icons.search);
  bool isSearching=false;
  String searchStr="";

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar
        (
          title: !isSearching?appBarTitle:
          TextField(
            cursorColor: Colors.white70,
            decoration: new InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.white70,),
                //prefixIcon: new Icon(Icons.search, color: Colors.white),
                hintText: "Search...",
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
                hintStyle: new TextStyle(color: Colors.white)),
            onSubmitted: (text) => performSearch(text),
            /* onChanged: (text) {
            setState(() => searchStr = text);
          },*/
            onChanged: (text) {
              //setState(()=> searchStr=text);
            },
          ),
          actions: buildactions(),
         /* <Widget>[
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(Icons.close);
                  this.appBarTitle = new TextField(
                    textInputAction: TextInputAction.search,
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Search...",
                        hintStyle: new TextStyle(color: Colors.white)),
                    onSubmitted: (text) => performSearch(text),
                    onChanged: (text) => performSearch(text),

                  );
                } else {
                  performSearch(null);
                  this.actionIcon = new Icon(Icons.search);
                  //this.appBarTitle = new Text(TITLE);
                }
              });
            },
          ),
            popUpMenuButton(),
        ],*/
        backgroundColor: Hexcolor('#008B8B'),
      ),
        body: new Container(
          child: new Center(
            child: (searchStr.isEmpty)?
            FutureBuilder<List<Section>>(
              future: sections,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return displaySections(snapshot.data);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              },
            ):displaySections(sectionsFiltered),
          ),
        ));
  }

  ListView displaySections(List<Section> data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) => Card(
            margin: EdgeInsets.all(0),
            elevation: 0.0,
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border(
                      bottom: BorderSide(
                        color: Colors.grey,
                        width: 0.2,
                      ))
              ),
              child: ListTile(
                title: new Text(
                  data[index].title.toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,),
                ),
                trailing: IconButton (
                  icon: Icon(data[index].isBookmarked
                      ? Icons.star
                      : Icons.star_border,
                  ),
                  onPressed: () {
                    addBookmark(data[index]);
                  },
                  alignment: Alignment.centerRight,
                ),
                subtitle: new Text(data[index].summery,
                  style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.normal),),
                contentPadding: EdgeInsets.only(bottom: 9,top: 6,left: 12,right: 5),
                onTap: () {
                  if(Platform.isAndroid)
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));
                  }
                  else if(Platform.isIOS)
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));
                  }
                },
              ),
            )
        ));
  }

  void addBookmark(final Section section) async {
    setState(() {
      if(section.isBookmarked) {
        thisSection.isBookmarked = true;
      }
      else {
        thisSection.isBookmarked = false;
      }
    });

    Future<int> response = personalDB.addBookmark(section);

    response.then((id) => {
      if (id > 0){
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Bookmark added")
      }
      else {
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Failed to add bookmark")
      }
    });
  }

  List<Widget> buildactions() {
    if(isSearching) {
      return [
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            setState(() {
              isSearching=false;
            });
            //_clearsearchQuery();

          },
        )
      ];
    }
    return [
      IconButton(
        color: Colors.white,
        icon: const Icon(Icons.search),
        onPressed: (){
          startSearch();
        },
      ),
      popUpMenuButton()

    ];

  }


  performSearch(String text) async {

    if(text==null && searchStr.isNotEmpty) {
      searchStr = "";
    }
    else {
      searchStr = text;
    }
    sectionsFiltered = List();

    sections.then((sectionList) =>
    {
      sectionList.forEach((section) {
        if (section.title.toLowerCase().contains(text.toLowerCase()) ||
            section.summery.toLowerCase().contains(text.toLowerCase()) ||
            section.details.toLowerCase().contains(text.toLowerCase())) {
          sectionsFiltered.add(section);
        }
      })
    });



    setState(() {

    });
  }

  Widget popUpMenuButton() {
    return PopupMenuButton<String>(
      icon: Icon(Icons.more_vert),
      itemBuilder: (BuildContext context)=><PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'MoreApps',
          child: Text('More Apps'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'queries_val',
          child: Text('My Queries'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'answers_val',
          child: Text('My Answers'),
        ),//PopupMenuItem
      ],
      onSelected: (retVal){
        if(retVal == 'MoreApps'){
          launchURL();
        }else if(retVal == 'queries_val'){

        }else {

        }
      },
    );
  }

  launchURL() {

    const url = 'market://search?q=pub:Offline Apps (No Internet required)';
    if(canLaunch(url) != null){
      launch(url);
    }else {
      throw 'Could not open $url';
    }
  }

  void startSearch() {
    setState(() {
        isSearching=true;
    });
  }


}