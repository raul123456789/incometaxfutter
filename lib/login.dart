import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:listview/main.dart';
import 'package:http/http.dart'as http;
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'dart:convert';
import 'dart:math';
import 'package:listview/network/network.dart' as network;
import 'package:listview/profileupdate/profile_update.dart';
import 'loginemail.dart';
import 'networkcheck.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/constants/constants.dart';
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

class login extends StatefulWidget {
  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<login> {

  bool loggedIn=false;
  //String memberId="3023253";

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GoogleSignInAccount googleAccount;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  static const _kDuration = const Duration(milliseconds: 300);

  static const _kCurve = Curves.ease;
  final _kArrowColor = Colors.black.withOpacity(0.8);
  final _controller = new PageController();
  NetworkCheck networkCheck = new NetworkCheck();

  bool showLoader = false;

   void signOutGoogle() async{

    /*SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();*/

    await googleSignIn.signOut().whenComplete((){

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return login();
          },
        ),
      );

    }
    ) ;
    print("User Sign Out");

  }

  /*signInButton() async{

    await SignInWithGoogle().whenComplete((){
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
              return Homepage();
          },
        ),
      );



    });


  }*/



  @override
  Widget build(BuildContext context) {

    final List<Widget> _pages = <Widget>[
      new ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20),
                ),
                Image.asset(
                  'assets/splash_0.png',
                  width: 280,
                  height: 280,
                ),

                /*Padding(
                  padding: EdgeInsets.only(left: 30,right: 30),
                  child: Text(
                    "Share Learn Network",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                  ),),*/

                Padding(
                  padding: EdgeInsets.only(left: 30, right: 30,bottom: 30),
                  child: Text(
                    "Welcome to the Income Tax Act and Query App",
                    style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          )),
      new ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(20),
                  ),
                  Image.asset(
                    'assets/splash_2.png',
                    width: 280,
                    height: 280,
                  ),
                  /*Text(
                    "Q&A Forum",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),*/
                  Padding(
                    padding: EdgeInsets.only(left: 30, right: 30,bottom: 30),
                    child: Text(
                      "Discuss your Queries with fellow Income Tax \n Law experts in India",
                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ))),
      new ConstrainedBox(
          constraints: const BoxConstraints.expand(),
          child: Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(20),
                  ),
                  Image.asset(
                    'assets/splash_1.png',
                    width: 280,
                    height: 280,
                  ),
                  /*Text(
                    "Read.Write.Share",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),*/
                  Padding(
                    padding: EdgeInsets.only(left: 30, right: 30,bottom: 30),
                    child: Text(
                      "Read Exclusive Articles from India's top Income  \n  Tax professionals and Chartered Accountants",
                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ))),
    ];


    return Scaffold(

      body: Container(
        child: Stack(
          children: <Widget>[
            new PageView.builder(
              itemCount: 3,
              physics: new AlwaysScrollableScrollPhysics(),
              controller: _controller,
              itemBuilder: (BuildContext context, int index) {
                return _pages[index];
              },
            ),
            new Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: new Container(
                  color: Colors.white /*grey[800].withOpacity(0.5)*/,
                  padding: const EdgeInsets.all(20.0),
                  child: new Column(
                    children: <Widget>[
                      Padding (
                          padding: EdgeInsets.only(top: 10),
                          child: (!showLoader)
                              ? Column(children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: GestureDetector(
                                        onTap: () => signInWithFacebook(),
                                        child: ClipRRect(
                                            borderRadius:
                                            BorderRadius.circular(40),
                                            child: Container(
                                                color: Colors.black12,
                                                child: new Row(
                                                  mainAxisSize:
                                                  MainAxisSize.min,
                                                  children: <Widget>[
                                                    new Image.asset(
                                                      'assets/fb_logo.png',
                                                      height: 50.0,
                                                    ),
                                                    new Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            left:
                                                            10.0,
                                                            right:
                                                            10.0),
                                                        child: new Text(
                                                          "Facebook",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black87,
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold),
                                                        )),
                                                  ],
                                                ))))),
                                Padding(
                                  padding: EdgeInsets.all(4),
                                ),
                                Expanded(
                                    child: GestureDetector(
                                        onTap: () => signInWithGoogle(),
                                        child: ClipRRect(
                                            borderRadius:
                                            BorderRadius.circular(40),
                                            child: Container(
                                                color: Colors.black12,
                                                child: new Row(
                                                  mainAxisSize:
                                                  MainAxisSize.min,
                                                  children: <Widget>[
                                                    new Image.asset(
                                                      'assets/google_new.png',
                                                      height: 50.0,
                                                    ),
                                                    new Container(
                                                        padding: EdgeInsets
                                                            .only(
                                                            left:
                                                            10.0,
                                                            right:
                                                            10.0),
                                                        child: new Text(
                                                          "Google",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .black87,
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold),
                                                        )),
                                                  ],
                                                ))))
                                    )
                                ],
                            ),
                            GestureDetector(
                              child: Padding(
                                padding: EdgeInsets.all(20),
                                child: Text(
                                  "Login with Email",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            LoginEmail()));
                              },
                            )
                          ])
                              : Center(child: CircularProgressIndicator())),
                      Container(
                        height: 55,
                        child: DotsIndicator(
                          controller: _controller,
                          itemCount: _pages.length,
                          onPageSelected: (int page) {
                            _controller.animateToPage(
                              page,
                              duration: _kDuration,
                              curve: _kCurve,
                            );
                          },
                        ),//Dots Indicator
                      ),//container
                    ],//widget
                  )),//Positioned>Container
            ),//positioned
          ],//widget
        ),//stack
      ),//body container
    ); //scaffhold
  } //build

  Future<Null> initUser() async {
    googleAccount = await getSignedInAccount(googleSignIn);
    if(googleAccount == null) {

    }else {
      await signInWithGoogle();
    }

  }

  /*Future<String> SignInWithGoogle() async {

            final FirebaseAuth _auth = FirebaseAuth.instance;
            final GoogleSignIn googleSignIn = new GoogleSignIn();

            final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
            final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;


            final AuthCredential credential = GoogleAuthProvider.getCredential(accessToken: googleSignInAuthentication.accessToken,
                idToken: googleSignInAuthentication.idToken);

            final AuthResult authResult = await _auth.signInWithCredential(credential);
            final FirebaseUser user =authResult.user;

            assert(!user.isAnonymous);
            assert(await user.getIdToken() != null);

            final FirebaseUser currentUser = await _auth.currentUser();
            assert(user.uid == currentUser.uid);
            assert(user.displayName==currentUser.displayName);
            assert(user.email==currentUser.email);

            *//*setState(() {
              String displayName=user.displayName.toString();
              String email=user.email.toString();
              String photoUrl = user.photoUrl;
            });*//*


            return 'signInWithGoogle succeeded: $user';

  }*/

  Future<Null> signInWithGoogle() async {

    networkCheck.checkInternet((bool connected) async {

      if(connected) {
        setState(() {
          showLoader = true;
        });


        if (googleAccount == null) {
          //start sign in
          try {
            googleAccount = await googleSignIn.signIn();

            if (googleAccount == null) {
              setState(() {
                showLoader = false;
              });
            }
          } catch (error) {
            setState(() {
              showLoader = false;
            });
          }
        }

        HashMap<String, String> userInfo = new HashMap<String, String>();
        userInfo["provider"] = "google";
        userInfo["femail"] = googleAccount.email;
        userInfo["fbid"] = googleAccount.id;
        userInfo["fbfullname"] = googleAccount.displayName;
        userInfo["profilepic"] =
        (googleAccount.photoUrl == null) ? "" : googleAccount
            .photoUrl + "?sz=110";

        attemptSocialLogin(userInfo);
      }
      else{
        final snackbar = SnackBar(content: Text("Please check your internet connection"));
        _scaffoldKey.currentState.showSnackBar(snackbar);
      }

    });

  }



  Future<GoogleSignInAccount> getSignedInAccount(GoogleSignIn googleSignIn) async {

    GoogleSignInAccount account = googleSignIn.currentUser;
    if(account == null) {
      account = await googleSignIn.signInSilently();
    }
    return account;
  }

  attemptSocialLogin(HashMap<String, String> userInfo) async {


    try {

      String response = await network.attemptSocialLogin(userInfo);

      List<String> resArr = response.split("#");


      if(resArr[0].toLowerCase() == "true") {

        final prefs = await SharedPreferences.getInstance();//

        prefs.setBool(IS_LOGGED_IN.toString(), true);
        prefs.setString(MEMBER_ID_STR.toString(), resArr[1]);
        prefs.setString(NAME_STR, resArr[2]);
        prefs.setString(EMAIL_STR.toString(), resArr[3]);
        prefs.setString(AVATAR_URL, resArr[4]);
        prefs.setBool(IS_PROFILE_INCOMPLETE, resArr[6] == 1);
        prefs.setString(CITY_ID, resArr[7]);
        prefs.setString(QUALIFICATION_ID, resArr[8]);
        prefs.setString(MOBILE, resArr[9]);
        prefs.setString(DOB, resArr[10]);
        //prefs.setString(COUNTRY_ID, resArr[12]);
        /*bool loggedIn = prefs.getBool(IS_LOGGED_IN);

        Fluttertoast.showToast (
              msg: loggedIn.toString(),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 3,
              backgroundColor: Colors.black,
              textColor: Colors.white,
              fontSize: 14.0
          );*/

        String memberId=prefs.getString(MEMBER_ID_STR);

        setState(() {
            showLoader=false;
        });

        //Navigator.push(context, MaterialPageRoute(builder: (context) => Homepage(memberId)));
        //Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileUpdate()));
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ProfileUpdate()));

      } else {
        setState(() {
          showLoader=false;
        });
      }

    } on TimeoutException catch (_) {

      final snackbar = SnackBar(content: Text("Failed to reach the server; Please try again"));
      Scaffold.of(context).showSnackBar(snackbar);

      setState(() {
        showLoader=false;
      });
    }on SocketException catch(_){

      final snackbar = SnackBar(content: Text("Could not reach the server; Please try again"));
      Scaffold.of(context).showSnackBar(snackbar);

      setState(() {
        showLoader=false;
      });
    } on Exception catch(_){
      final snackbar = SnackBar(content: Text("Some error occurred; Please try again"));
      Scaffold.of(context).showSnackBar(snackbar);

      setState(() {
        showLoader=false;
      });
    }
  }

  void loginWithapple() async {

    final AuthorizationResult result = await AppleSignIn.performRequests([AppleIdRequest(requestedScopes: [Scope.email,Scope.fullName])]);

    switch(result.status){

      case AuthorizationStatus.authorized:

        HashMap<String,String> parameters = new HashMap();
        parameters["provider"] = "apple";
        parameters["femail"] = result.credential.email;
        parameters["fbid"] = result.credential.user;
        parameters["fbfullname"] = result.credential.fullName.givenName;

        attemptSocialLogin(parameters);

        break;

      case AuthorizationStatus.cancelled:
        print("Sign in failed: ${result.error.localizedDescription}");
        setState(() {
          // errorMessage = "Sign in failed 😿";
        });
        break;
      case AuthorizationStatus.error:
        print('User cancelled');
        break;
    }


  }


  signInWithFacebook() async {



    networkCheck.checkInternet((bool connected) async {

      if(connected) {

        setState(() {
          showLoader = true;
        });

        final facebookLoginPL = FacebookLogin();
        final result = await facebookLoginPL.logIn(['email']);

        switch (result.status) {
          case FacebookLoginStatus.loggedIn:
            final token = result.accessToken.token;
            final graphResponse = await http.get(
                'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
            final profile = jsonDecode(graphResponse.body);


            /*{name: Gitanjal Bhattacharjya, first_name: Gitanjal, last_name: Bhattacharjya, email: gitudrebel92@gmail.com, id: 10219205252417128}*/
            HashMap<String, String> parameters = new HashMap();
            parameters["provider"] = "facebook";
            parameters["femail"] =
            profile["email"]; //response.getJSONObject().get("email"));
            parameters["fbid"] = profile["id"];
            parameters["fbfullname"] = profile["name"];
            parameters["profilepic"] = "https://graph.facebook.com/" +
                profile["id"] +
                "/picture??width=110&height=110";

            attemptSocialLogin(parameters);

            break;
          case FacebookLoginStatus.cancelledByUser:
            setState(() {
              showLoader = false;
            });
            break;
          case FacebookLoginStatus.error:
            setState(() {
              showLoader = false;
            });
            break;
        }
      }

      else {
        final snackBar = SnackBar(
            content: Text("Please check your internet connection"));
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }

    });
    /*final facebookLoginPL = FacebookLogin();
    final result = await facebookLoginPL.logIn(['email']);


    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
        final profile = jsonDecode(graphResponse.body);


        HashMap<String,String> parameters = new HashMap();
        parameters["providers"] = "facebook";
        parameters["femail"] = profile["email"];
        parameters["fbid"] = profile["id"];
        parameters["fbfullname"] = profile["name"];
        parameters["profilepic"] = "https://graph.facebook.com/" + profile["id"] + "/picture??width=110&height=110";


      break;
      case FacebookLoginStatus.cancelledByUser:
        // TODO: Handle this case.
        break;
      case FacebookLoginStatus.error:
        // TODO: Handle this case.
        break;
    }

    return 'signInWithFacebook succeeded: $result';
*/
  }









}


class DotsIndicator extends AnimatedWidget {



  DotsIndicator({

    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color:Colors.grey,

    }) : super(listenable: controller);

  final PageController controller; // page controller which is represented by Dots Indicator

  final int itemCount; //no of items by page controller

  final ValueChanged<int> onPageSelected; //dot is tapped
  final Color color;

  static const double _kDotSize = 6.0; //size of the dots

  static const double _kMaxZoom = 2.0; // increase in size of the dots

  static const double _kDotSpacing = 20.0; // spacing between the dots

  Widget _buildDot(int index) {

    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return new Container (
      width: _kDotSpacing,
      child: new Center(

        child: new Material(
          color: color,
          type: MaterialType.circle,
          child: new Container (
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),

            ),
          ),
        ),

      ),



    );

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Row(

      mainAxisAlignment: MainAxisAlignment.center,
      children :new List<Widget>.generate(itemCount, _buildDot),

    );
  }



}



