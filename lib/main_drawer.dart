import 'package:flutter/material.dart';
import 'details_screen.dart';
import 'package:listview/login.dart';
import 'package:listview/main.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/settings/settings.dart';
import 'package:share/share.dart';


class MainDrawer extends StatelessWidget {

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  Future<String> signInWithGoogle() async {

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken);

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user =authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return 'signInWithGoogle succeeded: $user';

  }

  BuildContext get context => null;

  void signOutGoogle() async {

      SharedPreferences sharedPref = await SharedPreferences.getInstance();
      sharedPref.clear();


      googleSignIn.signOut().whenComplete(( ) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => login()),
          ModalRoute.withName('/login'),
        );
      });
  }

  @override
  Widget build(BuildContext context) {

    var headerChild = new GestureDetector(

      onTap: (){

      },



    );

    return Drawer(

          child: Column(
            children: <Widget>[

                  /*Container(

                    color: Colors.black,
                    height: 20,
                    padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    child: new Row(

                            children: <Widget>[

                              ClipOval(
                                  //child: Image.network("https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg",height: 70,width: 70, fit: BoxFit.fill,),

                              ),
                              Padding(
                                padding: EdgeInsets.all(5),

                              ),
                              FutureBuilder<String>(
                                future: getName(),

                                builder: (context,snapshot){

                                  return Text(snapshot.data,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 15),);

                                },

                              )


                            ],


                        ),



                    ),*/


                /*Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  color: Theme.of(context).accentColor,
                  child: Center(
                    child: Column(
                      children: <Widget>[

                        Container(
                        width: 20,
                        height: 20,
                        margin: EdgeInsets.only(
                          top: 30,
                          bottom: 10
                        ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: NetworkImage('https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg'),
                              fit: BoxFit.fill
                            ), //Decoration image
                          ), //Box Decoration
                        ), //Container
                        Text(
                          'Pradip Kumar',
                           style: TextStyle(
                             color: Colors.white,
                           ),
                        ),
                        Text('pradip@gmail.com',
                            style: TextStyle(
                              color: Colors.white
                            ),
                        ),
                      ], //widget
                    ), //column
                  ), //center
                ), //container*/
              ListTile(
                leading: Icon(Icons.person),
                title: Text("News"),
                onTap: (){

                },
              ),//Listtile
              ListTile(
                leading: Icon(Icons.attach_file),
                title: Text("Files"),
                onTap: (){},
              ),
              ListTile(
                leading: Icon(Icons.book),
                title: Text("IT ACT 2018"),
                onTap: (){},
              ),//Listtile
              ListTile(
                leading: Icon(Icons.desktop_windows),
                title: Text("Online Learning"),
                onTap: () {

                }
              ),
              ListTile(
                  leading: Icon(Icons.star),
                  title: Text("Bookmark/Notes"),
                  onTap: () {

                  }
              ),
              ListTile(
                  leading: Icon(Icons.chrome_reader_mode),
                  title: Text("ICDS"),
                  onTap: () {

                  }
              ),
              ListTile(
                  leading: Icon(Icons.star_half),
                  title: Text("Leaderboard"),
                  onTap: () {

                  }
              ),
              ListTile(
                  leading: Icon(Icons.attach_money),
                  title: Text("Remove Ads"),
                  onTap: () {

                  }
              ),
              ListTile(
                  leading: Icon(Icons.settings),
                  title: Text("Settings"),
                  onTap: () {

                    Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));

                  }
              ),
              ListTile(
                  leading: Icon(Icons.share),
                  title: Text("Share app"),
                  onTap: () {

                    Share.share("Share With");

                  }
              ),
              ListTile(
                  leading: Icon(Icons.featured_play_list),
                  title: Text("Other Apps"),
                  onTap: () {

                  }
              ),
              ListTile(
                  leading: Icon(Icons.share),
                  title: Text("Share app"),
                  onTap: () {

                    Share.share("Share With");

                  }
              ),
              ListTile(
                  leading: Icon(Icons.featured_play_list),
                  title: Text("Other Apps"),
                  onTap: () {

                  }
              ),

            ], //widget
          ) //column
    ); //drawer
  }

  Future<String> getName() async {





  }//build


}//MainDrawer


