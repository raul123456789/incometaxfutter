import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:hexcolor/hexcolor.dart';



class Personalize extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PersonalizeState();
  }

}

class _PersonalizeState extends State<Personalize> {


  int selectedRadio;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedRadio=0;
  }

  setSelectedRadio(int val){
    setState(() {
        selectedRadio=val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("Personalise"),
          backgroundColor: Hexcolor('#008B8B')

      ),

      body: new Container(
              padding: EdgeInsets.all(8.0),
              child: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      'Select the first tab you want to see in the main screen',
                      style: new TextStyle(
                          fontSize: 12.0, fontWeight: FontWeight.bold,color: Colors.black),
                    ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[

                            new Radio(
                            value: 1,
                              groupValue: selectedRadio,
                              activeColor: Colors.green,
                              onChanged: (val){
                                setSelectedRadio(val);
                                showSnackBar(context);
                                //print("Radio Sections");
                              },
                            ),
                            new Text(
                              'Sections',
                              style: new TextStyle(fontSize: 16.0),
                            ),
                          new Padding(
                            padding: new EdgeInsets.all(25.0)
                          ),
                            new Radio(
                              value: 2,
                              groupValue: selectedRadio,
                              activeColor: Colors.green,
                              onChanged: (val){
                                setSelectedRadio(val);
                                showSnackBar(context);
                                //print("Radio Chapters");
                              },
                            ),
                            new Text(
                              'Chapters',
                              style: new TextStyle(
                                fontSize: 16.0,
                                ),
                              ),
                          ]
                        ),//Row
                      ],
                    ) //Row
              ),
            );



  }

  void showSnackBar(BuildContext context) {
    final snackBar = SnackBar(
        content: Text('Restart app to Reflect changes'),
        duration: Duration(seconds: 8),
        action: SnackBarAction(
        label: "RESTART",
        onPressed: (){
            Phoenix.rebirth(context);
        },
      ),
    );
    scaffoldKey.currentState.showSnackBar(snackBar);
  }


}
