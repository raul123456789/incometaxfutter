import 'dart:async';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:listview/Section.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:listview/db/personal_db.dart' as personalDB;
import 'package:listview/act/SectionDetails.dart';
import 'package:listview/db/local_db.dart';

class SectionListPage extends StatefulWidget {

  Future<List<Section>> sections;

  SectionListPage(this.sections);

  @override
  _SectionListPageState createState() => _SectionListPageState(sections);
}

class _SectionListPageState extends State<SectionListPage> {

  static final String TITLE = "Sections";
  Future<List<Section>> sections;

  List<Section> sectionsFiltered;

  _SectionListPageState(this.sections);

  Section thisSection;

  int index;

  Widget appBarTitle = new Text(TITLE);
  Icon actionIcon = new Icon(Icons.search);

  String searchStr="";

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      /*appBar: AppBar(title: appBarTitle, actions: <Widget>[
          new IconButton(
            icon: actionIcon,
            onPressed: () {
              setState(() {
                if (this.actionIcon.icon == Icons.search) {
                  this.actionIcon = new Icon(Icons.close);
                  this.appBarTitle = new TextField(
                    textInputAction: TextInputAction.search,
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Search...",
                        hintStyle: new TextStyle(color: Colors.white)),
                    onSubmitted: (text) => performSearch(text),
                    onChanged: (text) => performSearch(text),

                  );
                } else {
                  performSearch(null);
                  this.actionIcon = new Icon(Icons.search);
                  this.appBarTitle = new Text(TITLE);
                }
              });
            },
          ),
        ]),*/
      body:RefreshIndicator(
        onRefresh: _onRefresh,
        child: Container(
          child: new Center(
            child: (searchStr.isEmpty)?
            FutureBuilder<List<Section>>(
              future: sections,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return displaySections(snapshot.data);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              },
            ):displaySections(sectionsFiltered),
          ),
        ),
      ),

    );
  }

  ListView displaySections(List<Section> data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) => Card(
            margin: EdgeInsets.all(0),
            elevation: 0.0,
            child: Container(
              decoration: new BoxDecoration(
                  border: new Border(
                      bottom: BorderSide(
                        color: Colors.grey,
                        width: 0.2,
                      ))
              ),
              child: ListTile(
                title: new Text(
                  data[index].title.toUpperCase(),
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue,),
                ),
                /* trailing: IconButton (
                  icon: Icon(data[index].isBookmarked
                      ? Icons.star
                      : Icons.star_border,
                  ),
                  onPressed: () {
                          addBookmark(thisSection);
                    },
                  alignment: Alignment.centerRight,
                ),*/
                subtitle: new Text(data[index].summery,
                  style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.normal),),
                contentPadding: EdgeInsets.only(bottom: 9,top: 6,left: 12,right: 5),
                onTap: () {
                  if(Platform.isAndroid)
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));
                  }
                  else if(Platform.isIOS)
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SectionDetails(data,index)));
                  }
                },
              ),
            )
        ));
  }

  void addBookmark(final Section section) async {
    setState(() {
      if (!thisSection.isBookmarked) {
        if (thisSection == section)
          thisSection.isBookmarked = true;
        else
          thisSection.isBookmarked = false;
      }
    });

    Future<int> response = personalDB.addBookmark(section);

    response.then((id) => {
      if (id > 0){
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Bookmark added"),
        Fluttertoast.showToast(
            msg: "BookMark Added",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            textColor: Colors.white,
            fontSize: 14.0
        ),
      }
      else {
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Failed to add bookmark"),
        Fluttertoast.showToast(
            msg: "Failed to add bookmark",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            textColor: Colors.white,
            fontSize: 14.0
        ),
      }
    });

  }

  performSearch(String text) async {

    if(text==null && searchStr.isNotEmpty) {
      searchStr = "";
    }
    else {
      searchStr = text;
    }
    sectionsFiltered = List();

    sections.then((sectionList) =>
    {
      sectionList.forEach((section) {
        if (section.title.toLowerCase().contains(text.toLowerCase()) ||
            section.summery.toLowerCase().contains(text.toLowerCase()) ||
            section.details.toLowerCase().contains(text.toLowerCase())) {
          sectionsFiltered.add(section);
        }
      })
    });
    setState(() {

    });
  }


  Future<Null> _onRefresh() {

    Completer<Null> completer = new Completer<Null>();
    new Timer(new Duration(seconds: 1), () {
      print("timer complete");
      completer.complete();
    });

    return completer.future;



  }
}