import 'package:flutter/material.dart';
import 'package:listview/Section.dart';
import 'dart:io' show Platform;
import 'package:listview/act/SectionDetails.dart';
import 'package:listview/db/local_db.dart';
import 'package:listview/act/SectionDetailsIOS.dart';


class SectionList extends StatelessWidget {

  Future<List<Section>> sectionsList;

  SectionList(this.sectionsList);


  @override
  Widget build(BuildContext context) {
    return Container(

      child: new Center(

        child: FutureBuilder<List<Section>>(

          future: sectionsList,
          builder: (context, snapshot) {
             if(snapshot.hasData){
               return displaySections(snapshot.data);
             }else if(snapshot.error){
                  Text("${snapshot.error}");
             }
                return CircularProgressIndicator();
          }
        ),
      ),

    );
  }

  ListView displaySections(List<Section> data) {

    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) =>
          Card(
            margin: EdgeInsets.all(0),
            elevation: 2,
            child: Container(
              decoration: new BoxDecoration(
                border: new Border(
                  bottom: BorderSide(
                    color: Colors.grey,
                    width: 0.5,
                  )
                )
              ),
              child: ListTile(
                  title: new Text(data[index].title,
                  style: TextStyle(fontWeight: FontWeight.bold),),
                subtitle: new Text((data[index].summery==null)?"":data[index].summery),
                onTap: (){
                    if(Platform.isAndroid){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> SectionDetails(data, index)));
                    }
                    else if(Platform.isIOS){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SectionDetailsIOS(data, index)));
                    }
                  },
              ),//ListTile

            ),
          )//card
        );
  }
}
