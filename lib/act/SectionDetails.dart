import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/CommonModule/SearchResults.dart';
import 'package:listview/db/local_db.dart';
import 'package:listview/db/personal_db.dart' as personalDB;
import 'package:listview/Section.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:listview/network/network.dart' as network;
import 'package:substring_highlight/substring_highlight.dart';


class SectionDetails extends StatefulWidget {
  List<Section> sections;
  int index;

  SectionDetails(this.sections, this.index);

  @override
  State<StatefulWidget> createState() {
    return SectionDetailsState(sections, index);
  }
}

class SectionDetailsState extends State<SectionDetails> {

  static final String TITLE = "Sections";
  List<Section> sections;

  int index;

  int indexOfThisSection;

  List<Section> sectionsFiltered;

  Section s;

  SectionDetailsState(this.sections, this.index);

  var controllerNote = new TextEditingController();

  PageController controller;
  String searchStr="";
  Widget appBarTitle = new Text(TITLE);
  bool isSearching=false;
  final scaffoldKey = GlobalKey<ScaffoldState>();



  Section thisSection;
  Icon actionIcon = new Icon(Icons.search);
  Icon actionShare = new Icon(Icons.share);
  Icon actionNotes = new Icon(Icons.assignment);

  @override
  void initState() {
    controller = PageController(initialPage: index);
    thisSection = sections[index];
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controllerNote.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: !isSearching?Text(sections[index].title,style: TextStyle(fontSize: 15)):
        TextField(
            cursorColor: Colors.white70,
            decoration: new InputDecoration(
            prefixIcon: Icon(

                Icons.search,
                color: Colors.white70,
            ),
            //prefixIcon: new Icon(Icons.search, color: Colors.white),
            hintText: "Search in "+sections[index].title,
                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
            hintStyle: new TextStyle(color: Colors.white)),
          onSubmitted: (text) => performSearch1(text),
         /* onChanged: (text) {
            setState(() => searchStr = text);
          },*/
          onChanged: (text) {
              setState(()=> searchStr=text);
          },
          ),
          actions:_buildactions(),
         /* <Widget>[
              Row(
                children: <Widget>[
                  new IconButton(
                    icon: actionIcon,
                    onPressed: () {
                      setState(() {
                        this.isSearching=!this.isSearching;

                        if (this.actionIcon.icon == Icons.search) {
                        *//*  this.actionIcon = new Icon(Icons.close);
                          this.appBarTitle = new TextField(
                            textInputAction: TextInputAction.search,
                            style: new TextStyle(
                              color: Colors.white,
                            ),
                            decoration: new InputDecoration(
                                prefixIcon: new Icon(Icons.search, color: Colors.white),
                                hintText: "Search...",
                                hintStyle: new TextStyle(color: Colors.white)),
                            onSubmitted: (text) => performSearch(text),
                            onChanged: (text) => performSearch(text),

                          );*//*
                          _buildactions();
                        } else {
                          *//*this.actionIcon = new Icon(Icons.search);
                          this.appBarTitle = new Text(TITLE);*//*
                        }
                      });
                    },
                  ),
                  IconButton(
                    icon: actionShare,
                    tooltip: "Share",
                    onPressed: (){
                      Share.share("Share");
                    },
                  ),
                  IconButton(
                    icon: actionNotes,
                    tooltip: "Notes",
                    onPressed: (){
                      settingBottomsheet(context);
                    },
                  ),
                ],
              ),
        ],*/
          backgroundColor: Hexcolor('#008B8B'),
      ),
      body: PageView(
        children: showHTML(),
        controller: controller,
        onPageChanged: (num) {
//            controller.jumpToPage(num);
          setState(() {
            index = num;
            thisSection = sections[index];
          });
        },
        physics: ClampingScrollPhysics(),
      ),
    );
  }

  List<Widget> showHTML() {
    WebViewController _controller;
    List<Widget> pages = new List();

    String html;

    sections.forEach((Section s) => {
      indexOfThisSection = sections.indexOf(s),
      html = "<html><body>" + s.details + "</body></html>",
      pages.add(
          SingleChildScrollView (
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(15),
                  child: Html(
                    data: html,
                    onLinkTap: (url) {

                      if (url.contains("?id=")) {
                        List splitLink = url.split("?");
                        Section section;
                        indexOfThisSection;

                        try {
                                getASection(int.parse(splitLink[1])).then((s) => {
                                  section = s,
                                  indexOfThisSection = sections.indexOf(section),
                                 _ackAlert(context, section),
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SectionDetails(
                                              sections, indexOfThisSection)))
                                });


                        } catch (e) {
                           print(e);
                        }
                        return NavigationDecision.prevent;
                      }

                    },

                  )
              ),
              (indexOfThisSection + 1 < sections.length)
                  ? getNextButtonWidget(
                  context, sections, indexOfThisSection + 1, controller)
                  : Container()
            ],
          )))
    });
    return pages;
  }

  List<Widget> getPages() {
    WebViewController _controller;
    List<Widget> pages = new List();

    String html;
    sections.forEach((Section s) => {
      html = "<html><body>" + s.details + "</body></html>",
      pages.add(Column(
        children: <Widget>[
          WebView(
              initialUrl: Uri.dataFromString(html,
                  mimeType: 'text/html',
                  encoding: Encoding.getByName('utf-8'))
                  .toString(),
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _controller.loadUrl(Uri.dataFromString(s.details,
                    mimeType: 'text/html',
                    encoding: Encoding.getByName('utf-8'))
                    .toString());
              },
              navigationDelegate: (NavigationRequest request) {
                if (request.url.contains("section/#")) {
                  List splitLink = request.url.split("#");
                  Section section;
                  int indexOfThisSection;
                  try {
                        getASection(int.parse(splitLink[1])).then((s) => {
                          section = s,
                      //_ackAlert(context, section),
                      indexOfThisSection = sections.indexOf(section),

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SectionDetails(
                                  sections, indexOfThisSection)))
                    });
                  } catch (e) {
                    print(e);
                  }
                  return NavigationDecision.prevent;
                }
                return NavigationDecision.prevent;
              }),
        ],
      ))
    });

    return pages;
  }

  performSearch(String text) async {

    if(text==null && searchStr.isNotEmpty) {
          searchStr = "";
    }
    else{
        searchStr=text;
    }
   /* Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 3,
        textColor: Colors.white,
        fontSize: 14.0
    );*/
    Directionality(
        child: SubstringHighlight(
            textStyle: TextStyle(                       // non-highlight style
                  color: Colors.red,
            ),
            textStyleHighlight: TextStyle(              // highlight style
                color: Colors.yellow,
            ),
            text: showHTML().toString(),     // search result string from database or something
            term: text,       // user typed "et"
            ),
          textDirection: TextDirection.ltr
    );
    setState(() {

    });

  }

  List<Widget> _buildactions() {

    if(isSearching) {
      return [
        IconButton(
            icon: const Icon(Icons.clear),
          onPressed: () {
                if(controllerNote == null || controllerNote.text.isEmpty) {
                  setState(() {
                      isSearching=false;
                  });
                  return;
                    //this.actionIcon = new Icon(Icons.search);
                  return;
                }
              clearsearchQuery();
          },
        )

      ];
    }
    else{


    }
    return [
      IconButton(
        color: Colors.white,
        icon: const Icon(Icons.search),
        onPressed: (){
          startSearch();
        },
      ),
      IconButton(
        icon: actionShare,
        tooltip: "Share",
        onPressed: (){
          Share.share("Checkout "+sections[index].title+
                           "in Income Tax Act"+"\n http://play.google.com/store/apps/details?id="
                                                                +"com.offlineappsindia.incometaxacts");
        },
      ),
      /*IconButton(
        icon: Icon((thisSection.isBookmarked)
            ? Icons.star
            : Icons.star_border),
        tooltip: "Bookmark",
        onPressed: (){
            addBookmark(thisSection);
        },
      ),*/
      IconButton(
        icon: actionNotes,
        tooltip: "Notes",
        onPressed: () {
            settingBottomsheet(context);
        },
      ),
    ];
  }

  performSearch1(String text) async {
    if(text==null && searchStr.isNotEmpty) {
      searchStr = "";
    }
    else {
      searchStr=text;
    }
    var text1=s.details;
    TextStyle posRes = TextStyle(color: Colors.black, backgroundColor: Colors.yellow),
        negRes = TextStyle(color: Colors.black, backgroundColor: Colors.white);

     if(searchStr==null || searchStr == "") {
        return TextSpan(text: searchStr,style: negRes);
     }

     var refinedMatch=text1;
    Fluttertoast.showToast (
      msg: s.details,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 3,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 14.0
    );
     var refinedSearch = searchStr.toLowerCase();

     if(refinedMatch.contains(refinedSearch)) {
        if(refinedMatch.substring(0,refinedSearch.length)==refinedSearch) {
              return TextSpan(
                style: posRes,
                text: text1.substring(0,refinedSearch.length),
                children: [
                    performSearch1(text1.substring(refinedSearch.length,))
                ]

              );

        } else if(refinedSearch.length == refinedSearch.length) {

            return TextSpan(text: text1,style: posRes);

        } else {
            return TextSpan(
              style:negRes,
              text: text.substring(0,refinedMatch.indexOf(refinedSearch),),
              children: [
                      performSearch1(text1.substring(refinedMatch.indexOf(refinedSearch)
                  ),
                ),
              ]
            );
        }

     }else if (!refinedMatch.contains(refinedSearch)) {
       return TextSpan(text: text1, style: negRes);
     }
    return TextSpan(
      text: text1.substring(0, refinedMatch.indexOf(refinedSearch)),
      style: negRes,
      children: [
        performSearch1(text1.substring(refinedMatch.indexOf(refinedSearch)))
      ],
    );
  }

  void addBookmark(final Section section) async {
    setState(() {
      if (!thisSection.isBookmarked) {
        if (thisSection == section)
          thisSection.isBookmarked = true;
        else
          thisSection.isBookmarked = false;
      }
    });

    Future<int> response = personalDB.addBookmark(thisSection);

    response.then((id) => {
      print(id),
      if (id < 0) {
        showMsg(),
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Bookmark added")
      }
      else {
        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Failed to add bookmark")
      }
    });
  }

  Future<void> _ackAlert(BuildContext context, Section section) {
    WebViewController _controller;
    return showDialog<void> (
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(section.title),
          content: WebView(
            initialUrl: "",
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller = webViewController;
              _controller.loadUrl(Uri.dataFromString(section.details,
                  mimeType: 'text/html',
                  encoding: Encoding.getByName('utf-8'))
                  .toString());
            },
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('close'),
              onPressed: () {
                    Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  void showMsg() {


    Fluttertoast.showToast (
      msg: "BookMark Added",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 3,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 14.0
  );


  }

  void settingBottomsheet(BuildContext context) async {

    SharedPreferences pref = await SharedPreferences.getInstance();
    var text2;
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        backgroundColor: Colors.white,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: const EdgeInsets.symmetric(horizontal:18 ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 15,bottom: 15),
                    child: Text("     Add Note",style: TextStyle(fontWeight: FontWeight.bold),),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 190),
                    child: GestureDetector(
                      onTap: (){
                          Navigator.pop(context);
                      },
                          child: Text("Close",style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: TextFormField(
                  style: TextStyle(color: Colors.black),
                  keyboardType: TextInputType.text,
                  maxLines: 8,
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Enter Notes',
                    hintStyle: TextStyle(color: Colors.grey),
                    focusedBorder: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.all(Radius.circular(10)),
                      borderSide:
                      BorderSide(color: Colors.grey, width: 0.2),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius:
                      BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 0.8,
                      ),
                    ),
                  ),

                  onTap: () {
                      controllerNote.text;
                  },
                  autofocus: true,
                  controller: controllerNote,
                  onSaved: (String value) {
                        controllerNote.text=value;
                  },
                ),


              ),
              SizedBox(
                height: 8.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: RaisedButton(
                            color: Colors.blue,
                            textColor: Colors.white,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text(
                                'Save',
                                style: TextStyle(fontSize: 13)
                            ),
                          ),
                    )


                  ),
                 /* Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text("Close"),
                  )*/
                ],
              ),

              SizedBox(height: 10),
            ],
          ),
        ));

  }

  void clearsearchQuery() {
    setState(() {
      controllerNote.clear();
    });

  }

  void startSearch() {
    setState(() {
        isSearching=true;
    });
  }

}

getNextButtonWidget(
    BuildContext context, sections, int index, PageController controller) {
  Section section = sections[index];
  return Padding(
      padding: EdgeInsets.all(15),
      child: GestureDetector(
        onTap: () {
/*          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SectionDetails(sections, index+1)));*/

          controller.jumpToPage(index);
        },
        child: Container(
          child: Card(
            child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(section.title,style: TextStyle(fontSize: 18),),
                    Text(section.summery),
                    Padding(padding: EdgeInsets.all(5),),
                    Container(
                      decoration: new BoxDecoration(
                          border: new Border(
                              top: BorderSide(
                                color: Colors.grey,
                                width: 0.2,
                              ))),
                      height: 30,
                      child: Padding(padding: EdgeInsets.all(5),
                          child:Text("Click to Read",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold),)),
                    )
                  ],
                )),
          ),
        ),
      ));
}
