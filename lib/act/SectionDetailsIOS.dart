import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:listview/db/local_db.dart';
import 'package:listview/Section.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:listview/db/personal_db.dart' as personalDB;

class SectionDetailsIOS extends StatefulWidget {
  List<Section> sections;
  int index;

  SectionDetailsIOS(this.sections, this.index);

  @override
  State<StatefulWidget> createState() {
    return SectionDetailsIOSState(sections, index);
  }
}

class SectionDetailsIOSState extends State<SectionDetailsIOS> {
  List<Section> sections;
  int index;

  SectionDetailsIOSState(this.sections, this.index);

  PageController controller;

  Section thisSection;

  @override
  void initState() {
    controller = PageController(initialPage: index);

    thisSection = sections[index];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("Section IOS"),
          actions: <Widget>[
            IconButton(
              icon: Icon((thisSection.isBookmarked)
                  ? Icons.bookmark
                  : Icons.bookmark_border),
              tooltip: 'Bookmark',
              onPressed: () {
                addBookmark(thisSection);
              },
            ),
          ],
        ),
        body: showHTML(sections[index])
    );
  }

  Widget showHTML(Section s)
  {
    String html;
    html="<html><body>"+s.details+"</body></html>";

    return Html(
      data: html,
    );
  }

  Widget getPages(Section s) {
    WebViewController _controller;


    String html;
    html="<html><body>"+s.details+"</body></html>";
    var sanitizer = const HtmlEscape();
    WebView wv=WebView(
        initialUrl: Uri.dataFromString(sanitizer.convert(html),
            mimeType: 'text/html',
            encoding: Encoding.getByName('utf-8'))
            .toString(),
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller = webViewController;
          _controller.loadUrl(Uri.dataFromString(s.details,
              mimeType: 'text/html',
              encoding: Encoding.getByName('utf-8'))
              .toString());
        },
        navigationDelegate: (NavigationRequest request) {
          if (request.url.contains("section/#")) {
            List splitLink = request.url.split("#");
            Section section;
            int indexOfThisSection;
            try {


              getASection(int.parse(splitLink[1])).then((s) => {
                section = s,

                //_ackAlert(context, section),

                indexOfThisSection = sections.indexOf(section),

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SectionDetailsIOS(sections, indexOfThisSection)))

              });
            } catch (e) {
              print(e);
            }
            return NavigationDecision.prevent;
          }
          return NavigationDecision.prevent;
        });


    return wv;
  }

  void addBookmark(final Section section) async {
    setState(() {
      if (!thisSection.isBookmarked) {
        if (thisSection == section)
          thisSection.isBookmarked = true;
        else
          thisSection.isBookmarked = false;
      }
    });

    Future<int> response = personalDB.addBookmark(section);

    response.then((id) => {
      if (id > 0)
        {print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Bookmark added")}
      else
        {print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Failed to add bookmark")}
    });
  }

  Future<void> _ackAlert(BuildContext context, Section section) {
    WebViewController _controller;
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(section.title),
          content: WebView(
            initialUrl: "",
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller = webViewController;
              _controller.loadUrl(Uri.dataFromString(section.details,
                  mimeType: 'text/html',
                  encoding: Encoding.getByName('utf-8'))
                  .toString());
            },
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
