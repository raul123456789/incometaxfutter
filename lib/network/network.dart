import 'package:http/http.dart' as http;
import 'package:listview/model/Conversation.dart';
import 'package:listview/model/Message.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml/xml.dart' as xml;
import 'package:listview/constants/constants.dart';
import 'dart:collection';
import 'dart:async';
import 'dart:io';
import 'package:listview/constants/const_api.dart';
import 'package:listview/Post.dart';
import 'package:listview/User.dart';
import 'package:listview/model/Slider.dart';
import 'package:listview/model/Score.dart';


Map<String, String> headers = {
  "Content-type": "application/x-www-form-urlencoded"
};


//Retrieving the list of post

Future<List<Post>> fetchPost(String moduleName,
    {Map<String, String> jsonMap, String moduleId, String categoryId,String locationId}) async {

  if (jsonMap == null) {
      jsonMap = Map<String, String>();
  }

  SharedPreferences prefs = await SharedPreferences.getInstance();
  jsonMap["update_via"] = "13";
  jsonMap[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);//"2";                 //prefs.getString(MEMBER_ID_STR);
  jsonMap["gcm_id"] = "836";//prefs.getString(GCM_REG_ID_STR);
  jsonMap["module_name"] = moduleName;

  if(moduleId!=null) {
      jsonMap["module_id"] = moduleId;
  }
  /*if(categoryId!=null && !jsonMap.containsKey("cat_id"))
    jsonMap["cat_id"] = categoryId;

  if(locationId!=null && !jsonMap.containsKey("l_id"))
    jsonMap["l_id"] = locationId;*/

  List<Post> posts = [];


  try {

    final responseXml = await http.post(MODULE_FEED_MASTER, headers:headers, body: jsonMap);
    //print(responseXml);
    var document = xml.parse(responseXml.body);
    var nodes = document.findAllElements("r").toList();

    for (var node in nodes) {
        posts.add(createPost(node));
    }

  } on TimeoutException catch(e)
  {
    throw new Exception("Failed to load content");
  }
  on SocketException catch(e)
  {
    throw new Exception("Connection error: please check internet connection");
  }
  on Exception catch(e) {

    throw new Exception("Some error occurred");
  }

  return posts;

}

Future<String> genericNetworkRequestBeforeLogin(
    Map<String, String> params, String url) async {

  try {
    final responseStr = await http.post(url, headers: headers, body: params);
    return responseStr.body;
  }catch(e) {
    throw e;
  }

}

Future<String> genericNetworkRequest(Map<String, String> params, String url) async {

  SharedPreferences prefs = await SharedPreferences.getInstance();
  params["update_via"] = "13";
  params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);//"2";//prefs.getString(MEMBER_ID_STR);

  try{

    final responseStr = await http.post(url,headers: headers,body: params);

    return responseStr.body;

  } on TimeoutException catch(_) {

    throw new Exception("Failed to load content");

  } on SocketException catch(_) {

    throw new Exception("Connection Error : Please Check your internet connection");
  } on Exception catch(e) {
    throw new Exception("Some Error Occured");
  }


}

Future<String> genericNetworkRequestGET(Map<String, String> parameters, String url) async {

  Map<String, String> params = addCommonParams(parameters);

  SharedPreferences prefs = await SharedPreferences.getInstance();
  params[MEMBER_ID_STR] = prefs.getString(MEMBER_ID_STR);//"2";//prefs.getString(MEMBER_ID_STR);

  final responseStr = await http.get(url);
  return responseStr.body;

}

Map<String, String> addCommonParams(Map<String, String> parameters) {

  parameters["update_via"] = "13";

  if(Platform.isAndroid) {

    parameters["os"] = "android";

  }else if(Platform.isIOS){

    parameters["os"] = "ios";

  }

  return parameters;
}

Future<String> attemptSocialLogin(HashMap<String, String> params) async {


  Map<String, String> finalParams = addCommonParams(params);

  final responseStr = await http.post(LOGIN_SOCIAL_NEW,headers: headers,body: finalParams);

  return responseStr.body;

}

Future<String> uploadProfileInfo(Map<String, String> params) async {

  Map<String, String> finalParams = addCommonParams(params);

  try {

    final responseStr = await http.post(UPDATE_SOCIAL_PROFILE_NEW,headers: headers,body: finalParams);
    return responseStr.body;

  }on TimeoutException catch(e)
  {
    throw new Exception("Failed to load content");
  }
  on SocketException catch(e)
  {
    throw new Exception("Connection error: please check internet connection");
  }
  on Exception catch(e)
  {
    throw new Exception("Some error occurred");
  }


}

//Parse post data from xml

Post createPost(xml.XmlElement element) {
  /*title*/
  var titleEle = element.findAllElements("t").elementAt(0);
  var title = titleEle.text;

  /*author*/
  var authorEle = element.findAllElements("n").elementAt(0);
  var author = authorEle.text;

  /*Post info*/
  var postInfoEle = element.findAllElements("i").elementAt(0);
  var postInfo = postInfoEle.text;

  /*Preview Image*/
  var previewImageEle = element.findAllElements("pp").elementAt(0);
  var previewimage = CCI_BASE_URL_IMAGES+previewImageEle.text;

  /*URL*/
  var url;
  if(element.findAllElements("url").length>0) {
        var urlEle = element.findAllElements("url").elementAt(0);
        url = urlEle.text;
  }

  var eventDate;
  if (element.findAllElements("edate").length > 0) {
    var eventDateEle = element.findAllElements("edate").elementAt(0);
    eventDate = eventDateEle.text;
  }

  /*Category name*/
  /*if (element.findAllElements("bookmark").length > 0) {
    var bookmarkedEle = element.findAllElements("bookmark").elementAt(0);
    isBookmarked = bookmarkedEle.text;
  }*/

  /*Show as big card*/
  var showAsBigCard = false;
  if (element.findAllElements("card").length > 0) {
        var showAsBigCardEle = element.findAllElements("card").elementAt(0);
        String s = showAsBigCardEle.text;
        showAsBigCard = (showAsBigCardEle.text == "1") ? true : false;
  }

  var post = Post();
  post.title = title;
  post.authorName = author;
  post.previewImage=previewimage;
  post.eventDate = (eventDate == null) ? " " : eventDate;
  post.showAsBigCard = showAsBigCard;
  post.url=url;

  /*post info*/
  var postInfoArr = postInfo.split("#");
  post.moduleName = postInfoArr[0];
  post.publishDate = postInfoArr[1];
  post.itemId = postInfoArr[2];
  post.memberId = postInfoArr[3];
  post.modeItemId = postInfoArr[4];
  post.categoryId = postInfoArr[5];
  post.avatarUrl = postInfoArr[7];
  post.commentCount=postInfoArr[8];


  /*if (isBookmarked == null || isBookmarked == "" || isBookmarked == 0)
    post.isBookmarked = false;
  else
    post.isBookmarked = true;*/

  return post;

}

/*Parse user info from the xml*/

User parseUserInfo(String response) {

  var document = xml.parse(response);
  var nodes = document.findAllElements("r").toList();

  xml.XmlElement element = nodes[0];

  /*title*/
  var titleEle = element.findAllElements("n").elementAt(0);
  var title = titleEle.text;

  var bioEle = element.findAllElements("b").elementAt(0);
  var bio = bioEle.text;

  var idEle = element.findAllElements("i").elementAt(0);
  var id = idEle.text;

  /*Occupation*/
  var occEle = element.findAllElements("o").elementAt(0);
  var occ = occEle.text;

  /*Mobile*/
 /* var mobileEle = element.findAllElements("mob").elementAt(0);
  var mobile = mobileEle.text;*/

  var arrSplittedInfo = id.split("#");
  String memberId = arrSplittedInfo[0];
  String avatarUrl = (arrSplittedInfo[6] == "1")
      ? CCI_BASE_URL_PROFILE_IMAGES + "my_avatars/" + memberId + ".jpg"
      : ""; /*ApiUrls.CCI_BASE_URL_PROFILE_IMAGES + "my_avatars/" + user.getmMemberId() + ".jpg");*/
  String totalpoints = arrSplittedInfo[4];
  String viewsCount=arrSplittedInfo[2];
  String thanksCount=arrSplittedInfo[1];
  int isBeingFollowedByMe = int.parse(arrSplittedInfo[8]);
  String followCount = arrSplittedInfo[9];
  String follwingCount = arrSplittedInfo[10];

  User user = User();
  user.name = title;
  user.bio = bio;
  user.memberId = memberId;
  user.avatarUrl = avatarUrl;
  user.totalpoints=(totalpoints!=null && totalpoints!="")?totalpoints:"0";
  user.viewsCount=(viewsCount!=null && viewsCount!="")?viewsCount:"0";
  user.thankscount=(thanksCount!=null && thanksCount!="")?thanksCount:"0";
  user.isBeingFollowedByMe = isBeingFollowedByMe;
  user.followCount = (followCount!=null && followCount!="")?followCount:"0";
  user.followingCount = (follwingCount!=null && follwingCount!="")?follwingCount:"0";
  //user.mobile=mobile;
  user.occupation=occ;

  return user;
}

List<User> parseFollowerFollowing(String res) {

  List<User> userlist = List();

  var document = xml.parse(res);
  var nodes = document.findAllElements("r").toList();

  for(var element in nodes) {

    User user = User();

    /*member id*/
    var memberIdEle = element.findAllElements("mi").elementAt(0);
    var memberId = memberIdEle.text;

    /*name*/
    var nameEle = element.findAllElements("name").elementAt(0);
    var name = nameEle.text;

    /*Occupation*/
    var occEle = element.findAllElements("occ").elementAt(0);
    var occ = occEle.text;

    /*Follow record id*/
   /* var followRecordEle = element.findAllElements("id").elementAt(0);
    var followRecord = followRecordEle.text;*/

    user.memberId = memberId;
    user.name = name;
    user.occupation = occ;
    //user.followRecordId = followRecord;

    userlist.add(user);

  }

  return userlist;

}

List<Slider> parseSliders(String res) {
  List<Slider> sliderList = List();

  var document = xml.parse(res);
  var sliderNode = document.findAllElements("sl").toList()[0];
  var sliders = sliderNode.findAllElements("item").toList();

  for (var s in sliders) {
    /*Post title*/
    var postTitleEle = s.findAllElements("post_title").elementAt(0);
    var postTitle = postTitleEle.text;

    /*Module name*/
    var moduleNameEle = s.findAllElements("module_name").elementAt(0);
    var moduleName = moduleNameEle.text;

    /*Image*/
    var imgEle = s.findAllElements("img").elementAt(0);
    var img = imgEle.text;

    /*link*/
    var linkEle = s.findAllElements("link").elementAt(0);
    var link = linkEle.text;

    /*Type*/
    var typeEle = s.findAllElements("type").elementAt(0);
    var type = typeEle.text;

    /*Post msg*/
    var postMsgEle = s.findAllElements("post_msg").elementAt(0);
    var postMsg = postMsgEle.text;

    /*Item Id*/
    var itemIdEle = s.findAllElements("mod_item_id").elementAt(0);
    var itemId = itemIdEle.text;

    Slider slider = Slider();
    slider.title = postTitle;
    slider.moduleName = moduleName;
    slider.imgURL = img;
    slider.url = link;
    slider.type = type;
    slider.msg = postMsg;
    slider.modItemId = itemId;
    sliderList.add(slider);
  }

  return sliderList;
}

//parse Leaderboard score
List parseScore(String res) {
  List scoreList = List();

  var document = xml.parse(res);
  var nodes = document.findAllElements("r").toList();

  scoreList.add(parseSliders(res));

  for (var element in nodes) {
    Score scoreObj = Score();

    /*Member name*/
    var memberNameEle = element.findAllElements("n").elementAt(0);
    var memberName = memberNameEle.text;

    /*Member name*/
    var qualiEle = element.findAllElements("quali").elementAt(0);
    var quali = qualiEle.text;

    /*Score*/
    var scoreEle = element.findAllElements("pt").elementAt(0);
    var score = scoreEle.text;

    /*Score*/
    var combinedInfoEle = element.findAllElements("i").elementAt(0);
    var combinedInfo = combinedInfoEle.text;

    scoreObj.name = memberName;
    scoreObj.profession = quali;
    scoreObj.score = score;

    /*combined info*/
    var postInfoArr = combinedInfo.split("#");
    scoreObj.memberId = postInfoArr[3];

    scoreList.add(scoreObj);
  }

  return scoreList;
}

 List parseScore1(String res){

   List scoreList = List();

   var document = xml.parse(res);
   var nodes = document.findAllElements("r").toList();

   scoreList.add(parseSliders(res));

   for (var element in nodes) {
     Score scoreObj = Score();

   /*  *//*Member name*//*
     var memberNameEle = element.findAllElements("n").elementAt(0);
     var memberName = memberNameEle.text;

     *//*Member name*//*
     var qualiEle = element.findAllElements("quali").elementAt(0);
     var quali = qualiEle.text;*/

     /*Score*/
     var scoreEle = element.findAllElements("pt").elementAt(0);
     var score = scoreEle.text;

     /*Score*/
     var combinedInfoEle = element.findAllElements("i").elementAt(0);
     var combinedInfo = combinedInfoEle.text;

     scoreObj.score = score;

     /*combined info*/
     var postInfoArr = combinedInfo.split("#");
     scoreObj.memberId = postInfoArr[3];

     scoreList.add(scoreObj);
   }

   return scoreList;
}


/*Parse messages*/
List<Message> parseMessagesOfAConversation(String response) {
  List<Message> messages = List();

  var document = xml.parse(response);
  var nodes = document.findAllElements("r").toList();

  for (var element in nodes) {
    /*Sender id*/
    var memberIdEle = element.findAllElements("m").elementAt(0);
    var memberId = memberIdEle.text;

    /*Sender name*/
    var nameEle = element.findAllElements("n").elementAt(0);
    var name = nameEle.text;

    /*Message date*/
    var dateEle = element.findAllElements("d").elementAt(0);
    var date = dateEle.text;

    /*User image*/
    var avatarEle = element.findAllElements("y").elementAt(0);
    var avatar = avatarEle.text;

    /*Body*/
    var bodyEle = element.findAllElements("z").elementAt(0);
    var body = bodyEle.text;

    /*Conversation id*/
    var conversationIdEle = element.findAllElements("w").elementAt(0);
    var conversationId = conversationIdEle.text;

    Message message = Message();
    message.SenderId = memberId;
    message.SenderName = name;
    message.MsgTime = date;
    message.PMBody = body;
    message.PMId = conversationId;
    if (avatar != null)
      message.AvatarUrl = CCI_BASE_URL_PROFILE_IMAGES + "my_avatars/" + avatar;

    messages.add(message);
  }

  return messages;
}

/*Parse conversation*/
List<Conversation> parseConversation(String response) {
  List<Conversation> conversations = List();

  var document = xml.parse(response);
  var nodes = document.findAllElements("r").toList();

  for (var element in nodes) {
    /*Sender name*/
    var nameEle = element.findAllElements("n").elementAt(0);
    var name = nameEle.text;

    /*Body*/
    var lastMsgEle = element.findAllElements("z").elementAt(0);
    var lastMsg = lastMsgEle.text;

    /*Body*/
    var pidEle = element.findAllElements("i").elementAt(0);
    var pid = pidEle.text;
    var arrSplittedInfo = pid.split("#");
    String lastMsgTime = arrSplittedInfo[0];
    String pmId = arrSplittedInfo[1];
    String senderId = arrSplittedInfo[2];
    String pmStatus = arrSplittedInfo[3];
    String showSenderImage = arrSplittedInfo[4];

    Conversation conversation = Conversation();
    conversation.SenderName = name;
    conversation.LastMsgBody = lastMsg;
    conversation.LastMsgTime = lastMsgTime;
    conversation.PMId = pmId;
    conversation.SenderId = senderId;
    conversation.PMStatus = pmStatus;
    if (showSenderImage != null && showSenderImage == "1")
      conversation.SenderImage = CCI_BASE_URL_PROFILE_IMAGES +
          "my_avatars/" +
          conversation.SenderId +
          ".jpg";
    conversations.add(conversation);

  }

  return conversations;
}



