import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';


class Feed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text("Feed"),
        backgroundColor: Hexcolor('#008B8B'),
      ),
      body: Center(
          child: Container(
              child: Text("No Data Found",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey),),
          ),
      ),
    );
  }
}
