import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:crypto/crypto.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/main.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/profileupdate/profile_update.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'SignUp.dart';

class LoginEmail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Form Validation Demo';

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: LoginEmailForm(),
    );
  }
}

// Create a Form widget.
class LoginEmailForm extends StatefulWidget {
  @override
  LoginEmailFormState createState() {
    return LoginEmailFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class LoginEmailFormState extends State<LoginEmailForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

//  var prefs = SharedPreferences.getInstance();

  SharedPreferences prefs;

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  // your list of DropDownMenuItem
  List<DropdownMenuItem<String>> menuItems = List();

  // loop in the map and getting all the keys

  final KEY_OTHER_COUNTRY = "7";
  final KEY_OTHER_QUALI = "16";

  String dropdownValueCountry = '1';
  String dropdownValueQuali = '1';
  String mobileNumber = "";
  String otherCountry = "";
  String otherQualification = "";
  String fcmToken = "";

  String memberId = "";

  bool showSpecifyCountry = false;
  bool showSpecifyQualification = false;
  bool agreeTOS = false;
  bool showLoader = false;

  var controllerEmail = new TextEditingController();
  var controllerPass = new TextEditingController();
  var controllerOtherCountry = new TextEditingController();
  var controllerOtherQuali = new TextEditingController();

  @override
  void initState() {
    super.initState();

//    getSharedPrefs();
    configureFCM();
  }

  @override
  Widget build(BuildContext context) {
    final mq = MediaQueryData.fromWindow(window);

    return Container(
      color: Colors.black,
      child: SingleChildScrollView(
          child: /*ConstrainedBox(
            constraints: BoxConstraints.tightFor(
              height: mq.size.height-60,
            ),
            child:*/ Container(
            height:  MediaQuery.of(context).size.height-AppBar().preferredSize.height,
            alignment: Alignment.center,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Login with Email",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 40),
                    ),
                    TextFormField(
                      style: TextStyle(color: Colors.white),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xff2A2A2A),
                        hintText: 'Enter email',
                        hintStyle: TextStyle(color: Colors.white70),
                        focusedBorder: OutlineInputBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(10)),
                          borderSide:
                          BorderSide(color: Colors.grey, width: 0.2),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                            width: 0.2,
                          ),
                        ),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your email';
                        }
                        return null;
                      },
                      controller: controllerEmail,
                    ),


                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    TextFormField(
                      obscureText: true,
                      style: TextStyle(color: Colors.white),
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: Color(0xff2A2A2A),
                        hintText: 'Enter password',
                        hintStyle: TextStyle(color: Colors.white70),
                        focusedBorder: OutlineInputBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(10)),
                          borderSide:
                          BorderSide(color: Colors.grey, width: 0.2),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            color: Colors.grey,
                            width: 0.2,
                          ),
                        ),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter your password';
                        }
                        return null;
                      },
                      controller: controllerPass,
                    ),
                    /* Padding(
                            padding: EdgeInsets.only(top: 20),
                          ),
                          Row(
                            children: <Widget>[
                              Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor: Colors.grey),
                                  child: Checkbox(value: agreeTOS,
                                    activeColor: Colors.grey,
                                    onChanged: (v) => tosChange(v),
                                  )),
                              Flexible(child: Text("I agree to the Terms of Service and Privacy Policy", style: TextStyle(color:Colors.white),),)

                            ],

                          ),*/
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    (!showLoader)
                        ? GestureDetector(
                        onTap: () => attemptLogin(),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(40),
                            child: Container(
                                height: 50,
                                width: double.infinity,
                                color: Colors.white,
                                child: new Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    new Container(
                                        padding: EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        child: new Text(
                                          "Login",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.black,
                                          ),
                                        )),
                                  ],
                                ))))
                        : Center(
                        child: CircularProgressIndicator(
                          valueColor:
                          new AlwaysStoppedAnimation(Colors.white),
                        )),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                                padding: EdgeInsets.all(5),
                                child: GestureDetector(
                                  onTap: () {
                                    _launchURL(FORGOT_PASSWORD);
                                  },
                                  child: Text(
                                    "Forgot password?",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ))),
                        Expanded(
                            child: Padding(
                                padding: EdgeInsets.all(5),
                                child: GestureDetector(
                                  onTap: () => {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => SignUp()))
                                  },
                                  child: Text(
                                    "New user? Register",
                                    style: TextStyle(color: Colors.white),
                                    textAlign: TextAlign.right,
                                  ),
                                )))
                      ],
                    )
                  ],
                ),
              ),
            )
//        )
            //)
            ,
          )),
    );
  }

  void configureFCM() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.getToken().then((token) {
      //print("========================="+token);

      fcmToken = token;
    });
  }



  attemptLogin() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        showLoader = true;
      });

      HashMap<String, String> userInfo = new HashMap<String, String>();
      userInfo[USERNAME_STR] = controllerEmail.text;
      userInfo[PASS_STR] =
          md5.convert(utf8.encode(controllerPass.text)).toString();

      try {
        String response = await network.genericNetworkRequestBeforeLogin(
            userInfo, LOGIN_USER_NEW);

        List<String> resArr = response.split("#");

        if (resArr[0].toLowerCase() == "true") {
          final prefs = await SharedPreferences.getInstance();

          prefs.setBool(IS_LOGGED_IN.toString(), true);
          prefs.setString(MEMBER_ID_STR.toString(), resArr[1]);
          prefs.setString(NAME_STR, resArr[2]);
          prefs.setString(EMAIL_STR.toString(), resArr[3]);
          prefs.setString(AVATAR_URL, resArr[4]);
          prefs.setBool(IS_PROFILE_INCOMPLETE, resArr[6] == 1);
          prefs.setString(CITY_ID, resArr[7]);
          prefs.setString(QUALIFICATION_ID, resArr[8]);
          prefs.setString(MOBILE, resArr[9]);
          prefs.setString(DOB, resArr[10]);
          prefs.setString(COUNTRY_ID, resArr[12]);

          setState(() {
            showLoader = false;
          });

          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfileUpdate()));
        } else {

          var snackBar = SnackBar(content: Text(resArr[1]));

          /*check if this snackbar needs to be sticky*/
          if(resArr.length>=3)
          {
            snackBar = SnackBar(content: Text(resArr[1]),
              duration: Duration(minutes: 1),
              action: SnackBarAction(
                textColor: Colors.white,
                label: 'Resend',
                onPressed: () {
                  // Some code to undo the change.
                  _launchURL(resArr[2]);
                },
              ),);
          }

          Scaffold.of(context).showSnackBar(snackBar);

          setState(() {
            showLoader = false;
          });
        }
      } on TimeoutException catch (_) {
        final snackBar = SnackBar(
            content: Text("Failed to reach the server; Please try again"));
        Scaffold.of(context).showSnackBar(snackBar);

        setState(() {
          showLoader = false;
        });
      } on SocketException catch (_) {
        final snackBar = SnackBar(
            content: Text("Could not connect to the server; Please try again"));
        Scaffold.of(context).showSnackBar(snackBar);

        setState(() {
          showLoader = false;
        });
      } on Exception catch (_) {
        final snackBar =
        SnackBar(content: Text("Some error occurred; Please try again"));
        Scaffold.of(context).showSnackBar(snackBar);

        setState(() {
          showLoader = false;
        });
      }
    }
  }

  tosChange(bool value) {
    setState(() {
      agreeTOS = value;
    });
  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }
}
