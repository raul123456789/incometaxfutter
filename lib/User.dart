class User {

  String _memberId;
  String _name="";
  String _avatarUrl;
  String _occupation;
  String _thankscount;
  String _viewsCount;
  String _totalpoints;
  String _datejoined;
  String _mobile;
  String _bio;
  String _city;
  int _status;
  int _isBeingFollowedByMe;
  String _followCount="0";
  String _followingCount;
  String _followingRecordId;
  String _countryId;
  String _otherCountry;
  String _otherQualification;
  String _qualiId;


  String get qualiId => _qualiId;

  set qualiId(String value) {
    _qualiId = value;
  }

  String get countryId => _countryId;

  set countryId(String value) {
    _countryId = value;
  }

  String get memberId => _memberId;

  set memberId(String value) {
    _memberId = value;
  }

  String get name => _name;
  set name(String value){
     _name = value;
  }

  String get occupation => _occupation;

  set occupation(String value) {
    _occupation = value;
  }


  String get thankscount => _thankscount;

  set thankscount(String value) {
    _thankscount = value;
  }

  String get totalpoints => _totalpoints;

  set totalpoints(String value) {
    _totalpoints = value;
  }

  /*int get totalpoints => _totalpoints;

  set totalpoints(int value){
    _totalpoints = value;
  }*/


  String get avatarUrl => _avatarUrl;

  set avatarUrl(String value) {
    _avatarUrl = value;
  }

  String get datejoined => _datejoined;

  set datejoined(String value) {
    _datejoined = value;
  }

  String get bio => _bio;

  set bio(String value) {
    _bio = value;
  }

  String get city => _city;
  set city(String value) {
    _city = value;
  }

  int get status => _status;
  set status(int value){
    _status = value;
  }

  int get isBeingFollowedByMe => _isBeingFollowedByMe;

  set isBeingFollowedByMe(int value) {
    _isBeingFollowedByMe = value;
  }

  String get followingCount => _followingCount;

  set followingCount(String value) {
    _followingCount = value;
  }

  String get followCount => _followCount;

  set followCount(String value) {
    _followCount = value;
  }

  String get followRecordId => _followingRecordId;

  set followRecordId(String value) {
    _followingRecordId = value;
  }

  String get mobile => _mobile;

  set mobile(String value) {
    _mobile = value;
  }

  String get otherCountry => _otherCountry;

  set otherCountry(String value) {
    _otherCountry = value;
  }

  String get otherQualification => _otherQualification;

  set otherQualification(String value) {
    _otherQualification = value;
  }

  String get viewsCount => _viewsCount;

  set viewsCount(String value) {
    _viewsCount = value;
  }

}