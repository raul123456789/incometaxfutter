class Score
{
  String _name;
  String _profession;
  String _score;
  String _memberId;

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  String get profession => _profession;

  String get score => _score;

  set score(String value) {
    _score = value;
  }

  set profession(String value) {
    _profession = value;
  }

  String get memberId => _memberId;

  set memberId(String value) {
    _memberId = value;
  }


}