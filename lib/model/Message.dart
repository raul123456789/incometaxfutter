class Message  {

  String _SenderId;
  String _SenderName;
  String _AvatarUrl;
  String _PMBody;
  String _PMId;
  String _MsgTime;

  String get SenderId => _SenderId;

  set SenderId(String value) {
    _SenderId = value;
  }

  String get SenderName => _SenderName;

  String get MsgTime => _MsgTime;

  set MsgTime(String value) {
    _MsgTime = value;
  }

  String get PMId => _PMId;

  set PMId(String value) {
    _PMId = value;
  }

  String get PMBody => _PMBody;

  set PMBody(String value) {
    _PMBody = value;
  }

  String get AvatarUrl => _AvatarUrl;

  set AvatarUrl(String value) {
    _AvatarUrl = value;
  }

  set SenderName(String value) {
    _SenderName = value;
  }


}