class ITNotification {
  String type;
  String title;
  String body;
  String url;
  String moduleName;
  String modItemId;
  String img;

  ITNotification(this.type, this.title, this.body, this.url, this.moduleName,
      this.modItemId, this.img);

  toMap() {
    return {
      'type': type,
      'title': title,
      'body': body,
      'url': url,
      'module_name': moduleName,
      'mod_item_id': modItemId,
      'img': img
    };
  }
}