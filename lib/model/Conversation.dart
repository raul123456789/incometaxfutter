
class Conversation
{
  String _SenderId;
  String _SenderName;
  String _SenderImage;
  String _LastMsgTime;
  String _LastMsgBody;
  String _PMId;
  String _PMStatus;

  String get SenderId => _SenderId;

  set SenderId(String value) {
    _SenderId = value;
  }

  String get SenderName => _SenderName;

  String get PMStatus => _PMStatus;

  set PMStatus(String value) {
    _PMStatus = value;
  }

  String get PMId => _PMId;

  set PMId(String value) {
    _PMId = value;
  }

  String get LastMsgBody => _LastMsgBody;

  set LastMsgBody(String value) {
    _LastMsgBody = value;
  }

  String get LastMsgTime => _LastMsgTime;

  set LastMsgTime(String value) {
    _LastMsgTime = value;
  }

  String get SenderImage => _SenderImage;

  set SenderImage(String value) {
    _SenderImage = value;
  }

  set SenderName(String value) {
    _SenderName = value;
  }

  @override
  String toString() {
    return 'Conversation{_SenderId: $_SenderId, _SenderName: $_SenderName, _SenderImage: $_SenderImage, _LastMsgTime: $_LastMsgTime, _LastMsgBody: $_LastMsgBody, _PMId: $_PMId, _PMStatus: $_PMStatus}';
  }


}
