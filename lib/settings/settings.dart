import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/User.dart';
import 'package:listview/Utility.dart';
import 'package:listview/constants/web.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/profile/profile_edit.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/login.dart';
import 'package:listview/settings/notification_settings.dart';
import 'package:listview/personalise.dart';
import 'package:listview/profileupdate/profile_update.dart';
import 'package:listview/Inbox/Inbox.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:package_info/package_info.dart';

class Settings extends StatefulWidget {
  User user;


  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  User user;


  @override
  Widget build(BuildContext context) {
    var myNavChildren = [
      getNavItem(Icons.notifications, "Notification", "notification", context),
      getNavItem(Icons.person, "Personalize App", "personalize", context),
      getNavItem(Icons.edit, "Edit Profile", "edit_profile", context),
      getNavItem(Icons.contacts, "Contact Us", "contact", context),
      getNavItem(Icons.featured_play_list, "Other Apps", "otherApps", context),
      getNavItem(Icons.rate_review, "Rate Us", "rate", context),
      getNavItem(Icons.open_in_new, "Open Source", "openSorce", context),
      getNavItem(Icons.inbox, "Inbox", "inbox", context),
      getNavItem(Icons.new_releases, "Beta Program", "beta", context),
      getNavItem(Icons.security, "Privacy policy", "privacy", context),
      getNavItem(Icons.details, "Terms of Use", "terms", context),
      getNavItem(Icons.arrow_back, "Logout", "logout", context),
      //getNavItem(Icons.note, "App Version", "version", context)
     /* Center(
          child: Text(FutureBuilder(
            builder: (BuildContext context, AsyncSnapshot snapshot)=> Text("") ,)),
      )*/
    /* ListTile(
       enabled: false,
       title: Text("App Version"),
       trailing: FutureBuilder(
          future: getVersionNumber(),
          builder: (BuildContext context,
              AsyncSnapshot<String> snapshot)=>
              Center(
                child: Text(snapshot.hasData?snapshot.data:"Loading...",
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
              ),
       ),
     )*/
    ];

    return Scaffold(
      appBar:
          AppBar(title: Text("Settings"), backgroundColor: Hexcolor('#008B8B')),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: myNavChildren,
          ),
        ), //padding
      ), //container
    ); //scaffold
  }

  ListTile getNavItem(
      var icon, String s, String routeName, BuildContext context) {
    return new ListTile(
      leading:
          (icon != null) ? new Icon(icon) : IconButton(icon: Image.asset(icon)),
      title: new Text(s),
      onTap: () {
        switch (routeName) {
          case "notification":
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => NotificationSettings()));
            break;
          case "personalize":
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Personalize()));
            break;
          case "edit_profile":
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ProfileEdit(user)));
            break;
          case "contact":
            contact();
            break;
          case "otherApps":
            launchURL();
            break;
          case "rate":
            rateTheApp1();
            break;
          case "openSorce":
            open();
            break;
          case "inbox":
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Inbox()));
            break;
          case "beta":
            betaTest();
            break;
          case "privacy":
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        Web(PRIVACY_POLICY, "Privacy Policy")));
            break;
          case "terms":
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Web(TERMS_OF_USE, "Terms of Use")));
            break;
          case "logout":
            logout(context);
            break;
          /*case "version":
             getVersionNumber();
             break;*/
        }
      },
    );
  }

  Future logout(BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => login()),
        ModalRoute.withName('/login'));
  }

  launchURL() {
    const url = 'market://search?q=pub:Offline Apps (No Internet required)';
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not open $url';
    }
  }

  open() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                Web(OPEN_SOURCE_CREDITS, "Open Source Libraries")));

  }

  versionNo() async {

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionName = packageInfo.version;
    String versionCode = packageInfo.buildNumber;

    return versionCode+" "+versionName;

  }





  Future<String> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionCode = packageInfo.buildNumber;
    String versionName = packageInfo.version;
    return versionName;
    //return packageInfo.version;
    // Other data you can get:
    // 	String appName = packageInfo.appName;
    // 	String packageName = packageInfo.packageName;
    //	String buildNumber = packageInfo.buildNumber;
  }


}

  contact() {
    const url = CONTACT_US + "?update_via=13";
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not open $url';
    }
  }

  void rateTheApp1() {
    const url = "market://details?id=" + "com.offlineappsindia.incometaxacts";
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not open $url';
    }
  }

  void betaTest() {
    const url =
        "https://play.google.com/apps/testing/com.offlineappsindia.incometaxacts";
    if (canLaunch(url) != null) {
      launch(url);
    } else {
      throw 'Could not open $url';
    }
}
