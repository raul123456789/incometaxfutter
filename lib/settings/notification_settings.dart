import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/settings/settings.dart';

class NotificationSettings extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NotificationSettingsState();
  }
}

class _NotificationSettingsState extends State<NotificationSettings> {

  Future<SharedPreferences> prefs;
  bool switchValue = true;


  @override
  Widget build(BuildContext context) {

    prefs=getSharedPref();

    var myNavChildren = [
      getNavItem("Notification" , "notification",context),
    ];


    return Scaffold(
      appBar: AppBar(
        title: Text("Notification Settings"),
          backgroundColor: Hexcolor('#008B8B')
      ),
      body: Container(
        child: Padding(padding: EdgeInsets.all(15),
          child: ListView(children: myNavChildren,),
        ),//padding
      ),//container
    );//scaffold
  }

  Future<SharedPreferences> getSharedPref() async {

     SharedPreferences prefs = await SharedPreferences.getInstance();

     setState(() {
          switchValue=(prefs.getBool("show_notification")==null)?false:prefs.getBool("show_notification");
     });

    return prefs;

  }

  ListTile getNavItem(String s, String routeName, BuildContext context) {

    Widget trailing;
    if(routeName=="notification") {
            trailing=Switch(value: switchValue, onChanged: (val)=>toggleNotification(val));
    }
    else {
      trailing=Container(width: 0,height: 0,);
    }

    return new ListTile(
      trailing: trailing,
//      leading: new Icon(icon),
      title: new Text(s),
      onTap: () {

        // pop closes the drawer
//            Navigator.of(context).pop();
        // navigate to the route
//            Navigator.of(context).pushNamed(routeName);
        /*switch(routeName)
        {
          *//*case "notification":
            break;*//*
        }*/
      },
    );
  }

  toggleNotification(bool val) async {

    switchValue = val;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("show_notification", val);

    setState(() {
          switchValue = val;
    });

  }
}
