import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart'as http;
import 'package:listview/Chapters/ChapterList.dart';
import 'package:listview/CommonModule/CommonModulePage.dart';
import 'package:listview/ITAct.dart';
import 'package:listview/MyAnswers.dart';
import 'package:listview/MyQueries.dart';
import 'package:listview/OnllineLearning.dart';
import 'package:listview/Post.dart';
import 'package:listview/Section.dart';
import 'package:listview/Utility.dart';
import 'package:listview/act/SectionList.dart';
import 'package:listview/act/SectionListPage.dart';
import 'package:listview/askquery/AskQuery.dart';
import 'package:listview/db/local_db.dart';
import 'package:listview/post_details/PostDetailsStatefull.dart';
import 'package:listview/settings/settings.dart';
import 'package:share/share.dart';
import 'dart:convert' show json;
import 'package:listview/Leaderboard/Leaderboard.dart';
import 'dart:async';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:listview/login.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/CommonModule/CommonModule.dart';
import 'package:listview/constants/const_module.dart';
import 'package:listview/Bookmark/bookmarkpage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'CommonModule/SearchResults.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/Bookmark/bookmarkpage.dart';
import 'constants/constants.dart';
import 'package:listview/profile/profile.dart';
import 'package:listview/fcm/fcm.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  final prefs = await SharedPreferences.getInstance();
  bool loggin=(prefs.getBool(IS_LOGGED_IN) == null)
      ? false
      : prefs.getBool(IS_LOGGED_IN);
  String memberId = prefs.getString(MEMBER_ID_STR);
 /* bool gcmRegIdExists = (prefs.getString(GCM_REG_ID_STR) == null)
      ? false
      : true;*/ //  We are checking the value IS_PROFILE_INCOMPLETE (incomplete)
  bool logginStatus = loggin;
  if(logginStatus==true && logginStatus!=null) {
    runApp(
      Phoenix(
        child:  MaterialApp(
          debugShowCheckedModeBanner: false,
          home:Homepage(memberId),
        ),
      ),
    );
  } else {
    runApp(
      Phoenix(
        child:  MaterialApp(
          debugShowCheckedModeBanner: false,
          home:login(),
        ),
      ),
    );
  }
  /*Fluttertoast.showToast (
      msg: loggin.toString(),
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 3,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 14.0
  );
*/
}

class Homepage extends StatefulWidget {

  GoogleSignIn _googleSignIn;
  FirebaseUser _user;
  String memberId;
  bool loggedIn=false;

  Homepage(this.memberId);

  @override
  _HomepageState createState() => _HomepageState(memberId);

}

class _HomepageState extends State<Homepage> with TickerProviderStateMixin {

  GoogleSignIn _googleSignIn;
  FirebaseUser _user;
  GoogleSignInAccount googleAccount;
  bool loggedIn=false;

  _HomepageState(this.memberId);

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  FCMHelper fcmHelper;
  Section section;
  bool isSearching=false;

  String id="";
  int _currentIndex = 0;
  TabController controller;
  Map data;
  List userData;
  String memberId;
  Widget appBarTitle = new Text(APP_NAME,style: TextStyle(fontSize: 14.5,fontWeight: FontWeight.bold),);
  Icon actionIcon = new Icon(Icons.search);
  Icon actionShIcon = new Icon(Icons.share);
  Icon actionOverFlow = new Icon(Icons.more_vert);

  void signOutGoogle() async {

    await googleSignIn.signOut().whenComplete(() {

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) {
            return login();
          },
        ),
      );
    }
    ) ;

    print("User Sign Out");
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    //initUniLinks();
    fcmHelper = FCMHelper(context);

    fcmHelper.configureLocalNotification();
    fcmHelper.configureFCM();

    //controller = new TabController(length: 3, vsync: this);
    controller = new TabController(length: 3, vsync: this,initialIndex: 0,)..addListener((){
      setState(() {

      });
    });

  }

  /*Widget getTabBar(){

      return BottomBarWidget(controllerTab);

    }*/

  /* TabBarView getTabBarView(var tabs){

      return new TabBarView(

        children: tabs,
        controller: controller,

      );

    }*/

  Drawer getNavDrawer(BuildContext context) {

    var headerChild = new GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Profile(memberId)));
          },
          child: MediaQuery.removePadding(
            context: context,
            // DrawerHeader consumes top MediaQuery padding
            removeTop: true,
            child:
            Container (
                color: Hexcolor('#008B8B'),
                height: 95,
                padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                child:
                new Row(
                  children: <Widget>[
                    ClipOval(
                      child: Image.network(
                        CCI_BASE_URL_PROFILE_IMAGES +
                            "my_avatars/" +
                            memberId+
                            ".jpg",
                        height: 75,
                        width: 70,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: Column(
                        children: <Widget>[
                          FutureBuilder<String>(
                            future: getName(),
                            builder: (context, snapshot) {
                              return Text(
                                snapshot.data,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              );
                            },
                          ),
                          FutureBuilder<String>(
                            future: getEmail(),
                            builder: (context, snapshot) {
                              return Text(
                                snapshot.data,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 11.5,
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
            ),
          ));

    ListTile getNavItem(String s, String routeName,{var icon, var iconBtn}) {
      return new ListTile(
        leading: (icon != null)
            ? new Icon(icon)
            : IconButton(icon: Image.asset(iconBtn)),
        title: new Text(s),
        onTap: () {
          setState(() {
            // pop closes the drawer
            Navigator.of(context).pop();
            // navigate to the route
//            Navigator.of(context).pushNamed(routeName);
            switch (routeName) {
              case "news":
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CommonModulePage(
                            "News",
                            network.fetchPost("news",
                                moduleId: moduleInfo["3"]["id"]),
                            moduleInfo["3"]["id"]
                        )));
                break;

              case "files":
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CommonModulePage(
                            "Resources",
                            network.fetchPost(VALUE_MODULE_SHARE_FILES,
                                moduleId: moduleInfo["4"]["id"]),
                            moduleInfo["4"]["id"])));
                break;

              case "IT Act":
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ITAct(getSections(id))));
                break;

              /*case "e_learn":
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OnlineLearning()));
                break;*/
              case "bookmarks":
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => BookmarkPage()));
                break;

              case "icds":
              /* Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LeaderBoard()));*/
                break;
              case "leaderboard":
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Leaderboard()));
                break;
             /* case "remove":
              *//* Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LeaderBoard()));*//*
                break;*/
              case "settings":
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Settings()));
                break;
              case "shareapp":
                Share.share(
                    "Download Income Tax Act Android App"+
                        "\n http://play.google.com/store/apps/details?id="
                                      +"com.offlineappsindia.incometaxacts"
                    );
                break;
              case "otherapps":
                  launchURL();
                break;
              case "taxationlaws":
              /*Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BookmarkPage()));*/
                break;
            }
          });
        },
      );
    }

    var myNavChildren = [
      headerChild,
      getNavItem(
        "News",
        "news",
        icon: Icons.person,
      ),
      getNavItem(
        "Files",
        "files",
        icon: Icons.attach_file,
      ),
      getNavItem(
        "IT Act 2018",
        "IT Act",
        icon: Icons.book,
      ),
      /*getNavItem("Online Learning",
        "e_learn",
        icon: Icons.live_tv,
      ),*/
      getNavItem(
        "Bookmarks/Notes",
        "bookmarks",
        icon: Icons.star,

      ),
      /*getNavItem(
          "ICDS",
          "icds",
          icon:Icons.featured_play_list
      ),*/
      getNavItem(
        "Leaderboard",
        "leaderboard",
        icon: Icons.insert_chart,
      ),
     /* getNavItem(
        "Remove Ads",
        "remove",
        icon: Icons.attach_money,
      ),*/
      getNavItem(
        "Settings",
        "settings",
        icon: Icons.settings,
      ),
      getNavItem(
          "Share App",
          "shareapp",
          icon: Icons.share
      ),
      getNavItem(
          "Other Apps",
          "otherapps",
          icon: Icons.chrome_reader_mode
      ),
      getNavItem(
          "Taxation laws (Ordinance)",
          "taxationlaws",
          icon: Icons.subject
      ),
    ];
    ListView listView = new ListView(children: myNavChildren);
    return new Drawer(
      child: Padding(
        padding: EdgeInsets.all(2),
        child: listView,
      ),
    );
  }

  TabBarView getTabBarView(var tabs) {
    return new TabBarView(
      // Add tabs as widgets
      children: tabs,
      // set the controller
      controller: controller,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: !isSearching?appBarTitle:
            TextField(
              cursorColor: Colors.white70,
              decoration: new InputDecoration(
                  prefixIcon: Icon(
                                Icons.search,
                                color: Colors.white70,),
                  //prefixIcon: new Icon(Icons.search, color: Colors.white),
                  hintText: controller.index==0?"Search sections":controller.index==1?"Search Articles":"Search forums",
                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white70)),
                  hintStyle: new TextStyle(color: Colors.white)),
              onSubmitted: (text) => performSearch(text),
             /* onChanged: (text) {
                  performSearch(text);
                },*/
            ),
            bottom: new TabBar(
              indicatorColor: Colors.white,
              controller: controller,
              labelColor: Colors.white,
              tabs: <Widget>[
                new Tab(text: "SECTIONS",),
                new Tab(text: "ARTICLES",),
                new Tab(text: "QUERY",),
              ],
            ),
            actions: buildactions(),
           /* <Widget>[
                new IconButton(
                  icon: actionIcon,
                  onPressed: (){
                    setState(() {
                      if(this.actionIcon.icon == Icons.search) {
                        this.actionIcon = new Icon(Icons.close);
                        this.appBarTitle = new TextField(
                          textInputAction: TextInputAction.search,
                          style: new TextStyle(
                            color: Colors.white,
                          ),
                          decoration: new InputDecoration(
                              prefixIcon: new Icon(Icons.search, color: Colors.white),
                              hintText: "Search...",

                              hintStyle: new TextStyle(color: Colors.white)),
                          onSubmitted: (text) => performSearch(text),
                        );
                      }
                      else {
                          this.actionIcon = new Icon(Icons.search);
                          this.appBarTitle = new Text(APP_NAME,style: TextStyle(fontSize: 14.5,fontWeight: FontWeight.bold),);
                      }
                    });
                  }
              ),
              popUpMenuButton(),
            ], //widget*/
            backgroundColor: Hexcolor('#008B8B'),
          ),// appbar
          drawer: getNavDrawer(context),

          floatingActionButton:controller.index==2?
          FloatingActionButton(
            onPressed: ()=> {
              Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context)=> AsKQuery())
              ),
            },
            backgroundColor: Hexcolor('#008B8B'),
            child: Icon(Icons.edit),
            tooltip: 'Ask Query',
          ):
          FlatButton(

          ),

          body: getTabBarView(<Widget>[
            new SectionListPage(getSections(id)),
            //new ChapterList(getChapters(id)),
            new CommonModule(
                "Articles",
                network.fetchPost("articles", moduleId: moduleInfo["2"]["id"]),
                moduleInfo["2"]["id"]),
            new CommonModule(
                "Forum",
                network.fetchPost("forum", moduleId: moduleInfo["1"]["id"]),
                moduleInfo["1"]["id"]),
          ]),
        ));//Default Tab Controller
  } //build

  Future<String> getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return (prefs.getString(NAME_STR) == null)
        ? "User"
        : prefs.getString(NAME_STR);
  }
  Future<String> getEmail() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return (prefs.getString(EMAIL_STR) == null)
        ? "EMAIL"
        : prefs.getString(EMAIL_STR);

  }

  performSearch(String text) {
    HashMap<String, String> params = HashMap();
    params["q"] = text;

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchResultsPage(
                "Search", text, network.fetchPost("search", jsonMap: params))));
  }

  launchURL() {

    const url = 'market://search?q=pub:Offline Apps (No Internet required)';
    if(canLaunch(url) != null){
      launch(url);
    }else {
      throw 'Could not open $url';
    }
  }

  void startSearch() {

    setState(() {
        isSearching=true;
    });

  }

  List<Widget> buildactions() {
    if(isSearching) {
      return [
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
              setState(() {
                  isSearching=false;
              });
            //_clearsearchQuery();
          },
        )
      ];
    }
    return [
      IconButton(
        color: Colors.white,
        icon: const Icon(Icons.search),
        onPressed: (){
          startSearch();
        },
      ),
      popUpMenuButton()

    ];

  }

  void showSnackBar(BuildContext context) {

    var snackbar = SnackBar(
      content: Text("Restart app to reflect the changes"),
      action: SnackBarAction(
        label: "RESTART",
        onPressed: (){

        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackbar);
  }

   Widget popUpMenuButton() {

    return PopupMenuButton<String>(
      itemBuilder: (BuildContext context)=><PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'MoreApps',
          child: Text('More Apps'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'queries_val',
          child: Text('My Queries'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'answers_val',
          child: Text('My Answers'),
        ),//PopupMenuItem
      ],
      onSelected: (retVal) {
        if(retVal == 'MoreApps') {
            launchURL();
        } else if(retVal == 'queries_val') {
          Navigator.push(context,
              MaterialPageRoute(builder: (context)=>MyQueries()));
        }else {
          Navigator.push(context,
              MaterialPageRoute(builder: (context)=>MyAnswers()));
        }
      },
    );
  }


}


