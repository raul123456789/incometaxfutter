import 'dart:collection';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:listview/contribution.dart';
import 'package:listview/Post.dart';
import 'package:listview/network/network.dart' as network;
import 'package:listview/profile/profile.dart';
import 'package:mime/mime.dart';
import 'package:package_info/package_info.dart';
import 'package:path/path.dart' as Path;
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:listview/Utility.dart';
import 'package:listview/CommonModule/SearchResults.dart';


class PostDetailsStatefull extends StatefulWidget {

  Post post;

  PostDetailsStatefull(this.post);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PostDetailsState(post);
  }

}

class PostDetailsState extends State<PostDetailsStatefull> {

  String url="";
  double progress;

  WebViewController _webViewController;
  TextEditingController commentTextController;
  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new Text(APP_NAME);

  Post post;

  PostDetailsState(this.post);

  Future<String> urlWithData;

  bool showPostingCommentLoader=false;
  bool showCommentBar = true;
  bool isSearching=false;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    commentTextController = TextEditingController();
    urlWithData = getUrlWithData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      key: _scaffoldKey,
      appBar: AppBar(
        title: !isSearching?Text(getActivitytitle(post.moduleName)):
        TextField(
          decoration: new InputDecoration(
              prefixIcon: Icon(Icons.search),
              //prefixIcon: new Icon(Icons.search, color: Colors.white),
              hintText: "Search "+post.moduleName,
              hintStyle: new TextStyle(color: Colors.white)),
          onSubmitted: (text) => performSearch(text),
          /* onChanged: (text) {
            setState(() => searchStr = text);
          },*/
          onChanged: (text) {
            //setState(()=> searchStr=text);
          },
        ),
        backgroundColor: Hexcolor('#008B8B'),
        actions: _buildactions(),
       /* <Widget>[

            new IconButton(
                icon: actionIcon,
                onPressed: (){

                  setState(() {
                      if(this.actionIcon.icon == Icons.search){

                        this.actionIcon = new Icon(Icons.close);
                        this.appBarTitle = new TextField(
                          textInputAction: TextInputAction.search,
                          style: new TextStyle(
                            color: Colors.white,
                          ),
                          decoration: new InputDecoration(
                              prefixIcon: new Icon(Icons.search, color: Colors.white),
                              hintText: "Search Article/Query",
                              hintStyle: new TextStyle(color: Colors.white)),
                          onSubmitted: (text) => performSearch(text),

                        );
                      }
                      else{
                          this.actionIcon = new Icon(Icons.search);
                          this.appBarTitle = new Text(APP_NAME);
                      }

                  });

                }
            ),
            new IconButton(
              icon: Icon(Icons.share),
              tooltip: 'Share',
              onPressed: () {
                var url=post.getShareUrl();
                    Share.share(post.title+"\n"+url);
              },
            ),
        ],*/
      ),//App Bar

      body: Stack(

        children: <Widget>[

          FutureBuilder<String> (
              future: urlWithData,
              // ignore: missing_return
              builder: (BuildContext context1, AsyncSnapshot<String> snapshot){

                if(snapshot.hasData) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: (showCommentBar)?50:0),
                    child: WebView (

                        initialUrl: snapshot.data,
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (WebViewController controller) => {

                          if(snapshot.data!=null && snapshot.data.isNotEmpty) {
                            controller.loadUrl(post.getPostUrl()),
                            _webViewController = controller,
                            if (Theme
                                .of(context1)
                                .brightness ==
                                Brightness.dark) {
                              _webViewController.evaluateJavascript(
                                  "document.body.style.setProperty(\"color\", \"#E0E0E0\")"),
                              _webViewController.evaluateJavascript(
                                  "document.body.style.setProperty(\"background-color\", \"#424242\")"),
                              _webViewController.evaluateJavascript(
                                  "document.getElementsByTagName(\"h1\")[0].style.setProperty(\"color\", \"#E0E0E0\");")
                            }
                          }
                        },
                        onPageFinished: (url) {
                          //DynamicTheme.of(context).setBrightness(Theme.of(context).brightness == Brightness.dark? Brightness.light: Brightness.dark);
                          if(Theme.of(context).brightness == Brightness.dark) {
                            _webViewController.evaluateJavascript(
                                "document.body.style.setProperty(\"color\", \"#E0E0E0\")");
                            _webViewController.evaluateJavascript(
                                "document.body.style.setProperty(\"background-color\", \"#424242\")");
                            _webViewController.evaluateJavascript(
                                "document.getElementsByTagName(\"h1\")[0].style.setProperty(\"color\", \"#E0E0E0\");");
                          }
                        },
                      navigationDelegate: (NavigationRequest request) {

                      if(isDownloadableFile(request.url)) {
                            _launchURL(request.url);
                        return NavigationDecision.prevent;

                      }
                      else if(request.url.contains("www.caclubindia.com/webapp/profile.asp")) {

                        var uri = Uri.parse(request.url);
                        print(request.url);
                        var userId;
                        uri.queryParameters.forEach((k, v) {
                          print('key: $k - value: $v');

                          if (k == "member_id") {
                            userId = v;
                          }
                        });

                        Navigator.push(
                            context1,
                            MaterialPageRoute(
                                builder: (context) => Profile(userId)));

                        return NavigationDecision.prevent;

                      }
                      else if(request.url.contains("https://www.caclubindia.com/api/common/master_feed.asp")) {

                        var uri=Uri.parse(request.url);
                        var userId;
                        uri.queryParameters.forEach((k,v){

                          print('key:$k - value:$v');
                          if(k =='member_id'){
                            userId=v;
                          }

                        });

                        return NavigationDecision.prevent;

                      }
                      else if(request.url.contains("https://www.caclubindia.com")) {

                          _launchURL(request.url);
                          return NavigationDecision.prevent;

                      }
                      if(shouldEnableCommentBar(request.url)) {
                        setState(() {
                          showCommentBar=true;
                        });
                      }
                      else {
                        setState(() {
                            showCommentBar=false;
                        });
                      }

                      return NavigationDecision.navigate;

                      }

                      )

                  );
                }else {
                  return new CircularProgressIndicator();
                }
              }
            ),
          Visibility(
              visible: showCommentBar,
              child:Align(
                  alignment: Alignment.bottomLeft,
                  child: Theme(
                    // Create a unique theme with "ThemeData"
                    data: ThemeData(),
                    child: Container(
                        height: 50,
                        decoration: new BoxDecoration(
                            color: Theme.of(context).cardColor,
                            border: new Border(
                                top: BorderSide(
                                  color: Colors.grey,
                                  width: 0.2,
                                ))),

                        child: Row(
                          children: <Widget>[
                            Flexible(
                                child: TextField(
                                  style:
                                  TextStyle(color: Theme.of(context).hintColor),
                                  controller: commentTextController,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.all(5),
                                      border: InputBorder.none,
                                      hintStyle: TextStyle(
                                          color: Theme.of(context).hintColor),
                                      hintText: 'Post your comment/opinion'
                                  ),
                                )),
                            (showPostingCommentLoader)
                                ? Padding(
                                padding: EdgeInsets.all(10),
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  child: CircularProgressIndicator(),
                                ))
                                : IconButton(
                              icon: const Icon(Icons.send),
                              color: Colors.blue,
                              tooltip: 'Submit',
                              onPressed: () {
                                submitComment();
                              },
                            ),
                          ],
                        )),
                  )))
        ],//Widget

      ),//Stack
    );
  }

  performSearch(String text) {
    HashMap<String, String> params = HashMap();
    params["q"] = text;

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchResultsPage(
                "Search", text, network.fetchPost("search", jsonMap: params))));
  }

  List<Widget> _buildactions() {
    if(isSearching) {
      return [
        IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
              setState(() {
                isSearching=false;
              });
            //_clearsearchQuery();
          },
        )
      ];
    }
    return [
      IconButton(
        color: Colors.white,
        icon: const Icon(Icons.search),
        onPressed: (){
          startSearch();
        },
      ),
      IconButton(
        icon: Icon(Icons.share),
        tooltip: "Menu",
        onPressed: () {
            Share.share(post.getPostUrl());
        },
      ),

    ];
  }

  Future<String> getUrlWithData() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String queryString = "url=" +
        Uri.encodeComponent(post.getPostUrl()) +
        "&member_id=" +
        Uri.encodeComponent(prefs.getString(MEMBER_ID_STR))+
        "&ver=" +packageInfo.version+
        "&name=" +
        Uri.encodeComponent(prefs.getString(NAME_STR)) +
        "&username=" +
        Uri.encodeComponent(prefs.getString(EMAIL_STR)) +
        "&id="+
            "836"+
        /*"&city=" +
        prefs.getString(CITY_ID) +
        "&qualification=" +
        prefs.getString(QUALIFICATION_ID) +*/
        /* "&id=" +
        "836" +*/
        "&os=android" +
        "&update_via=14" +
        "&nativecom=1";

    String url = CCI_API_REDIRECT_POST + "?" + queryString;

   /* Fluttertoast.showToast(
        msg: url,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 3,
        textColor: Colors.white,
        fontSize: 14.0
    );*/
    print(url);
    return url;

  }

  void submitComment() async {

    setState(() {
          showPostingCommentLoader=true;
    });

    HashMap<String, String> params = new HashMap<String, String>();
    params[KEY_MODULE_NAME] = post.moduleName;
    params[KEY_API_ITEM_ID] = post.modeItemId;
    params[MEMBER_ID_STR] = post.memberId;
    params[KEY_API_MSG_BODY] = commentTextController.text;

    String res =
    await network.genericNetworkRequest(params, POST_COMMENT_COMMON);


    var splittedRes=res.split("#");

    if(splittedRes[0].toLowerCase()=="true") {

      setState(() {
        showPostingCommentLoader = false;

        getUrlWithData().then((url) {
          _webViewController.reload();
        });
        /*Fluttertoast.showToast (
            msg: splittedRes[1].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 3,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 14.0
        );*/
        final snackBar = SnackBar(
          content: Text(splittedRes[1]),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      });

    }
    else {
      setState(() {
          showPostingCommentLoader = false;
      });

      final snackBar = SnackBar(
        content: Text(splittedRes[1]),
      );

      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    /*For editing a comment pass the comment_id*/
    /*if(mCurrentComment!=null)
    {
      parameters.put("comment_id", mCurrentComment.getmCommentID());
    }*/

    /*check if replying to a comment or a comment on the post*/
    /*if (mReplyingTo == null) {
      */ /*a direct comment to the post*/ /*
    } else {
      */ /*its a reply to a comment*/ /*
      parameters.put(KEY_API_GROUP_BY, mReplyingTo.getmGroupBy());
    }*/
  }

  bool isDownloadableFile(String url) {

    bool isDownloadable = false;

    String ext  = Path.extension(url);
    String mimeType = lookupMimeType(url);
    if (mimeType != null) {
      isDownloadable = mimeType.toLowerCase().contains("video") ||
          ext.toLowerCase().contains("mov") ||
          ext.toLowerCase().contains("mp3") ||
          ext.toLowerCase().contains("pdf") ||
          ext.toLowerCase().contains("doc") ||
          ext.toLowerCase().contains("txt") ||
          ext.toLowerCase().contains("jpg") ||
          ext.toLowerCase().contains("xlsx")||
          ext.toLowerCase().contains("zip") ||
          ext.toLowerCase().contains("rar") ||
          ext.toLowerCase().contains("ppt") ||
          ext.toLowerCase().contains("pps") ||
          ext.toLowerCase().contains("xls") ||
          ext.toLowerCase().contains("rtf") ||
          ext.toLowerCase().contains("pptx")||
          ext.toLowerCase().contains("mdbx")||
          ext.toLowerCase().contains("docs")||
          ext.toLowerCase().contains("gif") ||
          ext.toLowerCase().contains("bmp") ||
          ext.toLowerCase().contains("png") ||
          ext.toLowerCase().contains("psd") ||
          ext.toLowerCase().contains("pcx") ||
          ext.toLowerCase().contains("tif") ||
          ext.toLowerCase().contains("wbmp")||
          ext.toLowerCase().contains("xml") ||
          ext.toLowerCase().contains("jpeg")||
          ext.toLowerCase().contains("xlt") ||
          ext.toLowerCase().contains("ttf") ||
          ext.toLowerCase().contains("xla") ||
          ext.toLowerCase().contains("dot") ||
          ext.toLowerCase().contains("odt");
    }
    return isDownloadable;
  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }

  bool shouldEnableCommentBar(String url) {}

  void startSearch() {

    setState(() {
      isSearching=true;
    });

  }

  Widget popUpMenuButton() {

    return PopupMenuButton<String>(
      itemBuilder: (BuildContext context)=><PopupMenuEntry<String>>[
        PopupMenuItem<String>(
          value: 'MoreApps',
          child: Text('More Apps'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'queries_val',
          child: Text('My Queries'),
        ),//PopupMenuItem
        PopupMenuItem<String>(
          value: 'answers_val',
          child: Text('My Answers'),
        ),//PopupMenuItem
      ],
      onSelected: (retVal) {
        if(retVal == 'MoreApps') {
          launchURL();
        } else if(retVal == 'queries_val') {
          /*Navigator.push(context,
              MaterialPageRoute(builder: (context)=>MyQueries()));*/
        }else {
          /*Navigator.push(context,
              MaterialPageRoute(builder: (context)=>MyAnswers()));*/
        }
      },
    );
  }

  launchURL() {

    const url = 'market://search?q=pub:Offline Apps (No Internet required)';
    if(canLaunch(url) != null){
      launch(url);
    }else {
      throw 'Could not open $url';
    }
  }




}
