import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:listview/constants/const_api.dart';
import 'package:listview/constants/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:listview/main.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:listview/constants/constants_dropdown.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:listview/network/network.dart' as network;


class ProfileUpdate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Form Validation Demo';

    return Scaffold(
      body: ProfileUpdateForm(),
    );
  }
}

// Create a Form widget.
class ProfileUpdateForm extends StatefulWidget {
  @override
  ProfileUpdateFormState createState() {
    return ProfileUpdateFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class ProfileUpdateFormState extends State<ProfileUpdateForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

//  var prefs = SharedPreferences.getInstance();

  SharedPreferences prefs;

  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  // your list of DropDownMenuItem
  List<DropdownMenuItem<String>> menuItems = List();

  // loop in the map and getting all the keys

  final KEY_OTHER_COUNTRY = "7";
  final KEY_OTHER_QUALI = "16";
  final KEY_OTHER_CITY = "31";

  String dropdownValueCountry = null;
  String dropdownValueQuali = null;
  String dropDownValueCity=null;
  String mobileNumber = "";
  String otherCountry = "";
  String otherQualification = "";
  String otherCity="";
  String fcmToken = "";

  String memberId = "";

  bool showSpecifyCountry = false;
  bool showSpecifyCity=false;
  bool showSpecifyQualification = false;
  bool agreeTOS = false;
  bool showLoader = false;

  var controllerMobile = new TextEditingController();
  var controllerOtherCity = new TextEditingController();
  var controllerOtherQuali = new TextEditingController();

  TapGestureRecognizer _recognizer1;
  TapGestureRecognizer _recognizer2;

  Future<Null> getSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
    var city = prefs.getString(CITY_ID);
    var country;
    var quali = prefs.getString(QUALIFICATION_ID);
    var mobile = prefs.getString(MOBILE);
    var othercity = prefs.getString(KEY_OTHER_CITY);
    var otherQuali = prefs.getString(KEY_OTHER_QUALI);
    memberId = prefs.getString(MEMBER_ID_STR);
    /* Scaffold.of(context)
        .showSnackBar(SnackBar(content: Text('Processing Data:'+mobile)));*/
    setState(() {

      //this.dropdownValueCountry = (country == null || country == "") ? null : country;
      this.dropDownValueCity= (city == null || city == "") ? null : city;
      this.dropdownValueQuali = (quali == null || quali == "") ? null : quali;
      this.mobileNumber = mobile;
      this.otherCountry = otherCountry;
      this.otherCity=othercity;
      this.otherQualification = otherQuali;


      controllerMobile.text = mobileNumber;
      controllerOtherCity.text = othercity;
      controllerOtherQuali.text = otherQuali;

    });

  }

  @override
  void initState() {
    super.initState();

    getSharedPrefs();
    configureFCM();

    _recognizer1 = TapGestureRecognizer()
      ..onTap = () {
        _launchURL(TERMS_OF_USE);
      };
    _recognizer2 = TapGestureRecognizer()
      ..onTap = () {
        _launchURL(PRIVACY_POLICY);
      };
  }

  @override
  Widget build(BuildContext context) {
    final mq = MediaQueryData.fromWindow(window);
    var underlineStyle = TextStyle(decoration: TextDecoration.underline, color: Colors.white, fontSize: 16);

    return Container(
      color: Colors.lightBlueAccent,
      child: SingleChildScrollView(
        child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(
              height: mq.size.height,
            ),
            child:

            Container(
                height: double.infinity,
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: Form(
                    key: _formKey,
                    child:
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 50),
                          child: Text(
                            "Please fill up the following details",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        /*Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text("Qualification",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),*/
                        Row(
                          children: <Widget>[
                            Text("Qualificatin : ",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                            Padding(
                              padding: const EdgeInsets.only(left: 1),
                              child: Container(
                                decoration: new BoxDecoration(
                                  border: Border.all(width: 0.2, color: Colors.black),
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.lightBlueAccent,
                                ),
                                width: 195,
                                height: 55,
                                child:
                                DropdownButtonHideUnderline(
                                  child: Theme(
                                      data: Theme.of(context).copyWith(
                                        canvasColor: Colors.black,
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10, right: 10),
                                        child: DropdownButton(
                                          hint: Text(
                                            "Select qualification",
                                            style: TextStyle(color: Colors.black),
                                          ),
                                          value: dropdownValueQuali,
                                          items: qualification.entries
                                              .map<DropdownMenuItem<String>>(
                                                  (MapEntry<String, String> e) =>
                                                      DropdownMenuItem<String>(
                                                        value: e.key,
                                                        child: Text(
                                                          e.value,
                                                          style: TextStyle(
                                                              color: Colors.white),
                                                        ),
                                                      ))
                                              .toList(),
                                          onChanged: (String newKey) {
                                            setState(() {
                                              if (newKey == KEY_OTHER_QUALI)
                                                showSpecifyQualification = true;
                                              else
                                                showSpecifyQualification = false;

                                              dropdownValueQuali = newKey;
                                            });
                                          },
                                        ),
                                      )),
                                ),
                              ),
                            ),
                          ],
                        ),



                        Visibility(
                          visible: showSpecifyQualification,
                          child: Padding(
                            padding: EdgeInsets.only(top: 10),
                          ),
                        ),
                        Visibility(
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: new InputDecoration(
                              filled: true,
                              fillColor: Color(0xff2A2A2A),
                              hintText: 'Specify qualification',
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide:
                                    BorderSide(color: Colors.black, width: 0.2),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  color: Colors.black,
                                  width: 0.2,
                                ),
                              ),
                            ),
                            controller: controllerOtherQuali,
                          ),
                          visible: showSpecifyQualification,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),
                        //city
                        Row(
                          children: <Widget>[
                            Text("City              : ",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                            Container(
                              decoration: new BoxDecoration(
                                border: Border.all(width: 0.2, color: Colors.black),
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                color: Colors.lightBlueAccent,
                              ),
                              width: 195,
                              height: 55,
                              child: DropdownButtonHideUnderline(
                                child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Color(0xff2A2A2A),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 15, right: 15),
                                      child: DropdownButton(
                                        hint: Text(
                                          "Select city",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        value: dropDownValueCity,
                                        items: city.entries
                                            .map<DropdownMenuItem<String>>(
                                                (MapEntry<String, String> e) =>
                                                DropdownMenuItem<String>(
                                                  value: e.key,
                                                  child: Text(
                                                    e.value,
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ))
                                            .toList(),
                                        onChanged: (String newKey) {
                                          setState(() {
                                            if (newKey == KEY_OTHER_CITY)
                                              showSpecifyCity = true;
                                            else
                                              showSpecifyCity = false;

                                            dropDownValueCity = newKey;
                                          });
                                        },
                                      ),
                                    )),
                              ),
                            ),
                          ],
                        ),

                        Visibility(
                          visible: showSpecifyCity,
                          child: Padding(
                            padding: EdgeInsets.only(top: 10),
                          ),
                        ),
                        Visibility(
                          child: TextFormField(
                            style: TextStyle(color: Colors.white),
                            decoration: new InputDecoration(
                              filled: true,
                              fillColor: Color(0xff2A2A2A),
                              hintText: 'Specify City',
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                borderSide:
                                BorderSide(color: Colors.black, width: 0.2),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 0.2,
                                ),
                              ),
                            ),
                            controller: controllerOtherCity,
                          ),
                          visible: showSpecifyCity,
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 30),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),

                        Row(
                          children: <Widget>[
                            Text("Mobile No. : ",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                            Container(
                              width: 195,
                              height: 55,
                              child: TextField(
                                style: TextStyle(color: Colors.white),
                                decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.lightBlueAccent,
                                  hintText: 'Mobile number',
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    borderSide:
                                        BorderSide(color: Colors.black, width: 0.2),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0.2,
                                    ),
                                  ),
                                ),
                                controller: controllerMobile,
                                  /*validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter your mobile number';
                                    }
                                    return null;
                                  }*/
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                        ),
                        Row(
                          children: <Widget>[
                            Theme(
                                data: ThemeData(
                                    unselectedWidgetColor: Colors.white),
                                child: Checkbox(
                                  value: agreeTOS,
                                  activeColor: Colors.grey,
                                  onChanged: (v) => tosChange(v),
                                )),
                            Flexible(
                              child: RichText(
                                text: TextSpan(
                                  text: 'I agree to the ',
                                  style: underlineStyle.copyWith(decoration: TextDecoration.none),
                                  children: <TextSpan>[
                                    TextSpan(text: 'Terms of Service ', style: underlineStyle, recognizer: _recognizer1),
                                    TextSpan(text: 'and '),
                                    TextSpan(text: 'Privacy Policy', style: underlineStyle, recognizer: _recognizer2),
                                    TextSpan(text: '. '),
                                  ],
                                ),
                              ),
                             /* child: Text(
                                "I agree to the Terms of Service and Privacy Policy",
                                style: TextStyle(color: Colors.white),
                              ),*/
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),
                        (!showLoader)
                            ? GestureDetector(
                                onTap: () => updateProfile(context),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(40),
                                    child: Container(
                                        height: 50,
                                        width: double.infinity,
                                        color: Colors.white,
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            new Container(
                                                padding: EdgeInsets.only(
                                                    left: 10.0, right: 10.0),
                                                child: new Text(
                                                  "CONTINUE",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black,
                                                  ),
                                                )),
                                          ],
                                        ))))
                            : Center(
                                child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation(Colors.white),
                              ))
                      ],
                    ),
                  ),
                ))),
      ),
    );
  }

  void configureFCM() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.getToken().then((token) {
      //print("========================="+token);

      fcmToken = token;
    });
  }

  updateProfile(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (_formKey.currentState.validate()) {
      if (agreeTOS) {
        // If the form is valid, display a Snackbar.

        setState(() {
          showLoader = true;
        });

        String error="";

        if(dropDownValueCity==null || dropDownValueCity.isEmpty)
          error+="Please select your city";

        if(dropdownValueQuali==null || dropdownValueQuali.isEmpty)
          error+="Please select your qualification";

        if(error.isNotEmpty)
          {
            Scaffold.of(context).showSnackBar(
                SnackBar(content: Text(error)));
            setState(() {
              showLoader = false;
            });

            return;
          }

        HashMap<String, String> userInfo = new HashMap<String, String>();
        userInfo[MOBILE] = controllerMobile.text;
        //userInfo[OTHER_COUNTRY] = controllerOtherCountry.text;
        userInfo[OTHER_CITY]=controllerOtherCity.text;
        userInfo[OTHER_QUALIFICATION] = controllerOtherQuali.text;
        userInfo[CITY_ID]=dropDownValueCity;
        //userInfo[COUNTRY_ID] = dropdownValueCountry;
        userInfo[QUALIFICATION_ID] = dropdownValueQuali;
        userInfo[FCM_TOKEN] = fcmToken;
        userInfo[AFTER_LOGIN] = "1";
        userInfo[MEMBER_ID_STR] = memberId;


        try {

        String response = await network.uploadProfileInfo(userInfo);

        var resArr = response.split("#");

        if (resArr[0].toLowerCase() == "true") {
          prefs.setString(GCM_REG_ID_STR, resArr[2]);

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Homepage(memberId)),
            );
          Fluttertoast.showToast(
                msg: "Profile Updated Sucessfully",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 3,
                textColor: Colors.white,
                fontSize: 14.0
            );

        } else {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('Failed to update your profile')));
          setState(() {
            showLoader = true;
          });
        }
        } on TimeoutException catch (_) {
          final snackBar = SnackBar(
              content: Text("Failed to reach the server; Please try again"));
          Scaffold.of(context).showSnackBar(snackBar);

          setState(() {
            showLoader = false;
          });
        } on SocketException catch (_) {

          final snackBar = SnackBar(
              content: Text("Could not connect to the server; Please try again"));
          Scaffold.of(context).showSnackBar(snackBar);

          setState(() {
            showLoader = false;
          });
        } on Exception catch (_) {
          final snackBar =
          SnackBar(content: Text("Some error occurred; Please try again"));
          Scaffold.of(context).showSnackBar(snackBar);

          setState(() {
            showLoader = false;
          });
        }
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            content:
                Text('You must agree to our Terms of Service to continue')));
      }
    }
  }

  tosChange(bool value) {
    setState(() {
      agreeTOS = value;
    });
  }

  _launchURL(String url) async {
    if (!url.contains("http")) url = "http://" + url;

    await launch(url);
  }
}
